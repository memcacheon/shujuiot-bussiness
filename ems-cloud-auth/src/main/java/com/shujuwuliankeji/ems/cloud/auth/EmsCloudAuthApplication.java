package com.shujuwuliankeji.ems.cloud.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableRyFeignClients;

/**
 * 认证授权中心
 * 
 * @author ruoyi
 */
@EnableRyFeignClients
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@ComponentScan(basePackages = { "com.shujuwuliankeji.ems.cloud" })
public class EmsCloudAuthApplication {
	public static void main(String[] args) {
		SpringApplication.run(EmsCloudAuthApplication.class, args);
	}
}
