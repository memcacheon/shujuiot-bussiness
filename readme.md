## 沭聚物联综合业务管理平台

#### 沭聚物联综合业务管理平台基于 SpringBoot、Vue、Mybatis、Mysql、Redis 等技术开发,实现了物联网综合业务系统的 SaaS 管理功能，与沭聚物联感知中台做了深度对接。整个框架是在若依 Cloud 微服务开源框架之上做了 SaaS 功能的改造。基于此开源项目，我们可以快速方便的完成物联网综合业务系统的开发，为企业数字化赋能提供一站式服务。 目前，除了基础平台功能，该平台已实现了能源管理平台部分基础功能，包括知智慧用电、智慧用水、电表管理、水表管理、项目管理、企业管理等功能，我们将逐步完善平台，助您快速接入物联网，让万物互联更简单。

##### 欢迎体验沭聚物联感知中台：http://iot.shujuwuliankeji.com

##### **特别鸣谢：Ruoyi-Cloud，element**

### 前端项目结构

```
├── build // 构建相关
├── bin // 执行脚本
├── public // 公共文件
│ ├── favicon.ico // favicon 图标
│ └── index.html // html 模板
├── src // 源代码
│ ├── api // 所有请求
│ ├── assets // 主题 字体等静态资源
│ ├── components // 全局公用组件
│ ├── directive // 全局指令
│ ├── layout // 布局
│ ├── plugins // 通用方法
│ ├── router // 路由
│ ├── store // 全局 store 管理
│ ├── utils // 全局公用方法
│ ├── views // view
│ ├── App.vue // 入口页面
│ ├── main.js // 入口 加载组件 初始化等
│ ├── permission.js // 权限管理
│ └── settings.js // 系统配置
├── .editorconfig // 编码格式
├── .env.development // 开发环境配置
├── .env.production // 生产环境配置
├── .env.staging // 测试环境配置
├── .eslintignore // 忽略语法检查
├── .eslintrc.js // eslint 配置项
├── .gitignore // git 忽略项
├── babel.config.js // babel.config.js
├── package.json // package.json
└── vue.config.js // vue.config.js
```

### 后端项目结构

```
com.shujuwuliankeji.ems.cloud
├── ems-cloud-ui // 前端框架 [80]
├── ems-cloud-gateway // 网关模块 [8080]
├── ems-cloud-auth // 认证中心 [9200]
├── ems-cloud-api // 接口模块
│ └── ems-cloud-api-system // 系统接口
│ └── ems-cloud-api-bussiness // 业务接口
├── ems-cloud-common // 通用模块
│ └── ems-cloud-common-core // 核心模块
│ └── ems-cloud-common-datascope // 权限范围
│ └── ems-cloud-common-datasource // 多数据源
│ └── ems-cloud-common-log // 日志记录
│ └── ems-cloud-common-redis // 缓存服务
│ └── ems-cloud-common-seata // 分布式事务
│ └── ems-cloud-common-security // 安全模块
│ └── ems-cloud-common-swagger // 系统接口
├── ems-cloud-modules // 业务模块
│ └── ems-cloud-system // 系统模块 [9201]
│ └── ems-cloud-gen // 代码生成 [9202]
│ └── ems-cloud-job // 定时任务 [9203]
│ └── ems-cloud-file // 文件服务 [9300]
│ └── ems-cloud-modules-bussiness // 业务服务 [9204]
├── ems-cloud-visual // 图形化管理模块
│ └── ems-cloud-visual-monitor // 监控中心 [9100]
├──pom.xml
```

##### 目前，所有业务服务均在**ems-cloud-modules-bussiness**中，实际开发中请根据业务进行合理的服务划分。
