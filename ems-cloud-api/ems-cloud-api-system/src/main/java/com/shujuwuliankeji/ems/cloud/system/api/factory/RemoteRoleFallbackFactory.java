package com.shujuwuliankeji.ems.cloud.system.api.factory;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import com.shujuwuliankeji.ems.cloud.common.core.domain.R;
import com.shujuwuliankeji.ems.cloud.system.api.RemoteRoleService;
import com.shujuwuliankeji.ems.cloud.system.api.domain.SysRole;

/**
 * 用户服务降级处理
 * 
 * @author ruoyi
 */
@Component
public class RemoteRoleFallbackFactory implements FallbackFactory<RemoteRoleService> {
	private static final Logger log = LoggerFactory.getLogger(RemoteRoleFallbackFactory.class);

	@Override
	public RemoteRoleService create(Throwable throwable) {
		log.error("角色服务调用失败:{}", throwable.getMessage());
		return new RemoteRoleService() {

			@Override
			public R<List<SysRole>> getSaaSRole(String source) {
				return R.fail("获取SaaS角色失败");
			}

			@Override
			public R<String> saveUserRole(Long userId, Long[] roleIds, String source) {
				// TODO Auto-generated method stub
				return R.fail("保存SaaS角色失败");
			}

			@Override
			public R<List<Long>> getSelectedSaasRole(Long userId, String inner) {
				// TODO Auto-generated method stub
				return R.fail("查询已分配SaaS角色失败");
			}

			@Override
			public R<Integer> deleteSaasUserRole(Long[] userIds, String inner) {
				return R.fail("删除SaaS关联角色失败");
			}

		};
	}
}
