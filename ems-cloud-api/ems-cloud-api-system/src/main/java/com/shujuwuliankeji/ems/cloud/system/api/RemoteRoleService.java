package com.shujuwuliankeji.ems.cloud.system.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.shujuwuliankeji.ems.cloud.common.core.constant.SecurityConstants;
import com.shujuwuliankeji.ems.cloud.common.core.constant.ServiceNameConstants;
import com.shujuwuliankeji.ems.cloud.common.core.domain.R;
import com.shujuwuliankeji.ems.cloud.system.api.domain.SysRole;
import com.shujuwuliankeji.ems.cloud.system.api.factory.RemoteRoleFallbackFactory;

/**
 * 用户服务
 * 
 * @author fanzhongjie
 */
@FeignClient(contextId = "remoteRoleService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteRoleFallbackFactory.class)
public interface RemoteRoleService {

	/**
	 * 获取saas role
	 * 
	 * @param source
	 * @return
	 */
	@GetMapping("/role/saas")
	public R<List<SysRole>> getSaaSRole(@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

	/**
	 * 保存用户角色
	 * 
	 * @param userId
	 * @param roleIds
	 * @param inner
	 * @return
	 */
	@PostMapping("/role/saas/user")
	public R<String> saveUserRole(@RequestParam("userId") Long userId, @RequestBody Long[] roleIds,
			@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

	/**
	 * 根据userId查询角色
	 * 
	 * @param userId
	 * @param inner
	 * @return
	 */
	@GetMapping("/role/saas/user/selected")
	public R<List<Long>> getSelectedSaasRole(@RequestParam("userId") Long userId,
			@RequestHeader(SecurityConstants.FROM_SOURCE) String inner);

	@DeleteMapping("/role/saas/user/{userIds}")
	public R<Integer> deleteSaasUserRole(@PathVariable("userIds") Long[] userIds,
			@RequestHeader(SecurityConstants.FROM_SOURCE) String inner);
}
