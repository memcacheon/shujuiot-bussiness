package com.shujuwuliankeji.ems.cloud.bussiness.api.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import com.shujuwuliankeji.ems.cloud.bussiness.api.RemoteCompanyService;
import com.shujuwuliankeji.ems.cloud.bussiness.api.domain.BussCompany;
import com.shujuwuliankeji.ems.cloud.common.core.domain.R;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月6日 下午5:02:21
 * @tips TODO
 */
@Component
public class RemoteCompanyFallbackFactory implements FallbackFactory<RemoteCompanyService> {
	private static final Logger log = LoggerFactory.getLogger(RemoteCompanyFallbackFactory.class);

	@Override
	public RemoteCompanyService create(Throwable throwable) {
		log.error("用户服务调用失败:{}", throwable.getMessage());
		return new RemoteCompanyService() {

			@Override
			public R<BussCompany> getUserCompany(Long userId, String source) {
				return R.fail("获取用户关联企业失败:" + throwable.getMessage());
			}

		};
	}

}
