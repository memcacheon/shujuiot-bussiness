package com.shujuwuliankeji.ems.cloud.bussiness.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.shujuwuliankeji.ems.cloud.bussiness.api.domain.BussCompany;
import com.shujuwuliankeji.ems.cloud.bussiness.api.factory.RemoteCompanyFallbackFactory;
import com.shujuwuliankeji.ems.cloud.common.core.constant.SecurityConstants;
import com.shujuwuliankeji.ems.cloud.common.core.constant.ServiceNameConstants;
import com.shujuwuliankeji.ems.cloud.common.core.domain.R;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月6日 下午5:00:03
 * @tips TODO
 */
@FeignClient(contextId = "remoteCompanyService", value = ServiceNameConstants.BUSSINESS_SERVICE, fallbackFactory = RemoteCompanyFallbackFactory.class)
public interface RemoteCompanyService {

	@GetMapping("/company/user")
	public R<BussCompany> getUserCompany(@RequestParam("userId") Long userId,
			@RequestHeader(SecurityConstants.FROM_SOURCE) String source);

}
