package com.shujuwuliankeji.ems.cloud.bussiness.api.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 企业管理对象 buss_company
 * 
 * @author fanzhongjie
 * @date 2023-01-04
 */
public class BussCompany extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 企业名称 */
	@Excel(name = "企业名称")
	private String companyName;

	/** 企业地址地址 */
	@Excel(name = "企业地址")
	private String address;

	/** 联系人 */
	@Excel(name = "联系人")
	private String linkMain;

	/** 联系电话 */
	@Excel(name = "联系电话")
	private String linkTel;

	/** 企业logo */
	@Excel(name = "企业logo")
	private String logo;

	/** 登录背景图片 */
	@Excel(name = "登录背景图片")
	private String loginBgImg;

	/** 自定义平台名称 */
	@Excel(name = "自定义平台名称")
	private String platformName;

	/** 二级域名 */
	@Excel(name = "二级域名")
	private String domain;

	/**
	 * 绑定userID
	 */
	private Long userId;

	/**
	 * 绑定userName
	 */
	private String userName;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setLinkMain(String linkMain) {
		this.linkMain = linkMain;
	}

	public String getLinkMain() {
		return linkMain;
	}

	public void setLinkTel(String linkTel) {
		this.linkTel = linkTel;
	}

	public String getLinkTel() {
		return linkTel;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getLogo() {
		return logo;
	}

	public void setLoginBgImg(String loginBgImg) {
		this.loginBgImg = loginBgImg;
	}

	public String getLoginBgImg() {
		return loginBgImg;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getDomain() {
		return domain;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("companyName", getCompanyName()).append("address", getAddress())
				.append("linkMain", getLinkMain()).append("linkTel", getLinkTel()).append("logo", getLogo())
				.append("remark", getRemark()).append("loginBgImg", getLoginBgImg())
				.append("platformName", getPlatformName()).append("domain", getDomain()).toString();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
