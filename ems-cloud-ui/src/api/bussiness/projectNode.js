import request from '@/utils/request'

// 查询项目节点列表
export function listProjectNode(query) {
  return request({
    url: '/bussiness/projectNode/list',
    method: 'get',
    params: query
  })
}

// 查询项目节点详细
export function getProjectNode(id) {
  return request({
    url: '/bussiness/projectNode/' + id,
    method: 'get'
  })
}

// 新增项目节点
export function addProjectNode(data) {
  return request({
    url: '/bussiness/projectNode',
    method: 'post',
    data: data
  })
}

// 修改项目节点
export function updateProjectNode(data) {
  return request({
    url: '/bussiness/projectNode',
    method: 'put',
    data: data
  })
}

// 删除项目节点
export function delProjectNode(id) {
  return request({
    url: '/bussiness/projectNode/' + id,
    method: 'delete'
  })
}

// 获取所属项目
export function listProject() { 
    return request({
        url: '/bussiness/projectNode/project/list',
        method:'get'
    });
}