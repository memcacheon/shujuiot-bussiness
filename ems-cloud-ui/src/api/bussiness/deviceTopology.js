import request from '@/utils/request'

// 查询项目
export function listProject() {
  return request({
    url: '/bussiness/device/topology/project/list',
    method: 'get',
  })
}

// 查询项目节点
export function listProjectNode(query) {
  return request({
    url: '/bussiness/device/topology/project/node/tree',
    method: 'get',
    params: query
  })
}

//获取设备信息
export function listDevice(query) { 
  return request({
    url: '/bussiness/device/topology/device/list',
    method: 'get',
    params:query
  })
}

//移除设备拓扑
export function removeDeivce(ids) { 
  return request({
    url: '/bussiness/device/topology/device/remove/' + ids,
    method: 'delete'
  })
}
//查询暂未分配的设备
export function listAllocateDevice(query) { 
   return request({
    url: '/bussiness/device/topology/allocate/device/list',
    method: 'get',
    params:query
  })
}
//添加拓扑设备
export function addDevice(data) { 
    return request({
    url: '/bussiness/device/topology/device/add',
    method: 'post',
    data: data
  })
}