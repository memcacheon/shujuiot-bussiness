import request from '@/utils/request'

// 查询报警信息列表
export function listAlarmAmWarning(query) {
  return request({
    url: '/bussiness/alarmAmWarning/list',
    method: 'get',
    params: query
  })
}

// 查询报警信息详细
export function getAlarmAmWarning(id) {
  return request({
    url: '/bussiness/alarmAmWarning/' + id,
    method: 'get'
  })
}

// 新增报警信息
export function addAlarmAmWarning(data) {
  return request({
    url: '/bussiness/alarmAmWarning',
    method: 'post',
    data: data
  })
}

// 修改报警信息
export function updateAlarmAmWarning(data) {
  return request({
    url: '/bussiness/alarmAmWarning',
    method: 'put',
    data: data
  })
}

// 删除报警信息
export function delAlarmAmWarning(id) {
  return request({
    url: '/bussiness/alarmAmWarning/' + id,
    method: 'delete'
  })
}

//获取项目数据
export function listProject() { 
  return request({
    url: '/bussiness/alarmAmWarning/project/list',
    method:'get'
  })
}