import request from '@/utils/request'
import { method } from 'lodash'

// 查询电报警规则列表
export function listAmAlarmRule(query) {
  return request({
    url: '/bussiness/amAlarmRule/list',
    method: 'get',
    params: query
  })
}

// 查询电报警规则详细
export function getAmAlarmRule(id) {
  return request({
    url: '/bussiness/amAlarmRule/' + id,
    method: 'get'
  })
}

// 新增电报警规则
export function addAmAlarmRule(data) {
  return request({
    url: '/bussiness/amAlarmRule',
    method: 'post',
    data: data
  })
}

// 修改电报警规则
export function updateAmAlarmRule(data) {
  return request({
    url: '/bussiness/amAlarmRule',
    method: 'put',
    data: data
  })
}

// 删除电报警规则
export function delAmAlarmRule(id) {
  return request({
    url: '/bussiness/amAlarmRule/' + id,
    method: 'delete'
  })
}


//获取项目数据
export function listProject() { 
  return request({
    url: '/bussiness/amAlarmRule/project/list',
    method:'get'
  })
}