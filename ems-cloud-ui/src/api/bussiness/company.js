import request from '@/utils/request'

// 查询企业管理列表
export function listCompany(query) {
  return request({
    url: '/bussiness/company/list',
    method: 'get',
    params: query
  })
}

// 查询企业管理详细
export function getCompany(id) {
  return request({
    url: '/bussiness/company/' + id,
    method: 'get'
  })
}

// 新增企业管理
export function addCompany(data) {
  return request({
    url: '/bussiness/company',
    method: 'post',
    data: data
  })
}

// 修改企业管理
export function updateCompany(data) {
  return request({
    url: '/bussiness/company',
    method: 'put',
    data: data
  })
}

// 删除企业管理
export function delCompany(id) {
  return request({
    url: '/bussiness/company/' + id,
    method: 'delete'
  })
}

// 获取所有用户
export function listUser() {
  return request({
    url: '/bussiness/company/user/list',
    method: 'get'
  })
}
