import request from '@/utils/request'

// 查询水表管理列表
export function listDeviceWatermeter(query) {
  return request({
    url: '/bussiness/device/watermeter/list',
    method: 'get',
    params: query
  })
}

// 查询水表管理详细
export function getDeviceWatermeter(id) {
  return request({
    url: '/bussiness/device/watermeter/' + id,
    method: 'get'
  })
}

// 新增水表管理
export function addDeviceWatermeter(data) {
  return request({
    url: '/bussiness/device/watermeter',
    method: 'post',
    data: data
  })
}

// 修改水表管理
export function updateDeviceWatermeter(data) {
  return request({
    url: '/bussiness/device/watermeter',
    method: 'put',
    data: data
  })
}

// 删除水表管理
export function delDeviceWatermeter(id) {
  return request({
    url: '/bussiness/device/watermeter/' + id,
    method: 'delete'
  })
}

export function pollDeviceWatermeter() { 
    return request({
      url: '/bussiness/device/watermeter/poll',
      method: 'get',
  })
}
