import request from '@/utils/request'

// 查询用户信息列表
export function listUser(query) {
  return request({
    url: '/bussiness/company/user/page',
    method: 'get',
    params: query
  })
}

// 查询用户信息详细
export function getUser(userId) {
  return request({
    url: '/bussiness/company/user/' + userId,
    method: 'get'
  })
}

// 新增用户信息
export function addUser(data) {
  return request({
    url: '/bussiness/company/user',
    method: 'post',
    data: data
  })
}

// 修改用户信息
export function updateUser(data) {
  return request({
    url: '/bussiness/company/user',
    method: 'put',
    data: data
  })
}

// 删除用户信息
export function delUser(userId) {
  return request({
    url: '/bussiness/company/user/' + userId,
    method: 'delete'
  })
}

// 用户状态修改
export function changeUserStatus(userId, status) {
  const data = {
    userId,
    status
  }
  return request({
    url: '/bussiness/company/user/changeStatus',
    method: 'put',
    data: data
  })
}

// 查询部门下拉树结构
export function deptTreeSelect() {
  return request({
    url: '/bussiness/company/user/deptTree',
    method: 'get'
  })
}

// 查询部门下拉树结构
export function getSaasRole() {
  return request({
    url: '/bussiness/company/user/role',
    method: 'get'
  })
}

// 用户密码重置
export function resetUserPwd(userId, password) {
  const data = {
    userId,
    password
  }
  return request({
    url: '/bussiness/company/user/resetPwd',
    method: 'put',
    data: data
  })
}