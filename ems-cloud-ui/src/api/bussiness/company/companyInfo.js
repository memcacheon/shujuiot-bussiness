import request from '@/utils/request'

// 查询企业信息详细
export function getCompanyInfo() {
  return request({
    url: '/bussiness/company/info',
    method: 'get'
  })
}

// 修改企业信息
export function updateCompanyInfo(data) {
  return request({
    url: '/bussiness/company/info',
    method: 'put',
    data: data
  })
}
