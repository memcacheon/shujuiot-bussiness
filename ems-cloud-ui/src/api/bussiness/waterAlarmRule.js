import request from '@/utils/request'

// 查询水报警规则列表
export function listWaterAlarmRule(query) {
  return request({
    url: '/bussiness/waterAlarmRule/list',
    method: 'get',
    params: query
  })
}

// 查询水报警规则详细
export function getWaterAlarmRule(id) {
  return request({
    url: '/bussiness/waterAlarmRule/' + id,
    method: 'get'
  })
}

// 新增水报警规则
export function addWaterAlarmRule(data) {
  return request({
    url: '/bussiness/waterAlarmRule',
    method: 'post',
    data: data
  })
}

// 修改水报警规则
export function updateWaterAlarmRule(data) {
  return request({
    url: '/bussiness/waterAlarmRule',
    method: 'put',
    data: data
  })
}

// 删除水报警规则
export function delWaterAlarmRule(id) {
  return request({
    url: '/bussiness/waterAlarmRule/' + id,
    method: 'delete'
  })
}
