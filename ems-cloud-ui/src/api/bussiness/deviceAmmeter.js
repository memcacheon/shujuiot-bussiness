import request from '@/utils/request'

// 查询设备管理列表
export function listDevice(query) {
  return request({
    url: '/bussiness/device/ammeter/list',
    method: 'get',
    params: query
  })
}

// 查询设备管理详细
export function getDevice(id) {
  return request({
    url: '/bussiness/device/ammeter/' + id,
    method: 'get'
  })
}

// 新增设备管理
export function addDevice(data) {
  return request({
    url: '/bussiness/device/ammeter',
    method: 'post',
    data: data
  })
}

// 修改设备管理
export function updateDevice(data) {
  return request({
    url: '/bussiness/device/ammeter',
    method: 'put',
    data: data
  })
}

// 删除设备管理
export function delDevice(id) {
  return request({
    url: '/bussiness/device/ammeter/' + id,
    method: 'delete'
  })
}
//同步电表设备
export function pollDeviceAmmeter() { 
    return request({
      url: '/bussiness/device/ammeter/poll',
      method: 'get',
  })
}
