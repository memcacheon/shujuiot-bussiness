import request from '@/utils/request'

// 查询策略列表
export function listTactics(query) {
  return request({
    url: '/bussiness/tactics/list',
    method: 'get',
    params: query
  })
}

// 查询策略详细
export function getTactics(id) {
  return request({
    url: '/bussiness/tactics/' + id,
    method: 'get'
  })
}

// 新增策略
export function addTactics(data) {
  return request({
    url: '/bussiness/tactics',
    method: 'post',
    data: data
  })
}

// 修改策略
export function updateTactics(data) {
  return request({
    url: '/bussiness/tactics',
    method: 'put',
    data: data
  })
}

// 删除策略
export function delTactics(id) {
  return request({
    url: '/bussiness/tactics/' + id,
    method: 'delete'
  })
}
