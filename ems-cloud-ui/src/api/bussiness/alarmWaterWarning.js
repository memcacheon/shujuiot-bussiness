import request from '@/utils/request'

// 查询报警信息列表
export function listAlarmWaterWarning(query) {
  return request({
    url: '/bussiness/alarmWaterWarning/list',
    method: 'get',
    params: query
  })
}

// 查询报警信息详细
export function getAlarmWaterWarning(id) {
  return request({
    url: '/bussiness/alarmWaterWarning/' + id,
    method: 'get'
  })
}

// 新增报警信息
export function addAlarmWaterWarning(data) {
  return request({
    url: '/bussiness/alarmWaterWarning',
    method: 'post',
    data: data
  })
}

// 修改报警信息
export function updateAlarmWaterWarning(data) {
  return request({
    url: '/bussiness/alarmWaterWarning',
    method: 'put',
    data: data
  })
}

// 删除报警信息
export function delAlarmWaterWarning(id) {
  return request({
    url: '/bussiness/alarmWaterWarning/' + id,
    method: 'delete'
  })
}
