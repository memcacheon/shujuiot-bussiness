import request from '@/utils/request'

//获取项目列表
export function listProject() {
  return request({
    url: '/bussiness/device/water/monitor/project/list',
    method: 'get',
  })
}

//获取项目列表
export function listDevice(query) {
  return request({
    url: '/bussiness/device/water/monitor/list',
    method: 'get',
    params:query
  })
}

//获取项目节点
export function listProjectNode(query) { 
  return request({
    url: '/bussiness/device/water/monitor/project/node/list',
    method: 'get',
    params:query
  })
}
//获取项目设备概况数据
export function getProjectSummary(query) { 
  return request({
    url: '/bussiness/device/water/monitor/project/summary',
    method: 'get',
    params:query
  });
}
//获取设备最新一条数据消息
export function getDeviceLastData(id) { 
  return request({
    url: '/bussiness/device/water/monitor/' + id,
    method:'get'
  });
}
//获取历史曲线数据
export function getDeviceStatistic(query) { 
  return request({
    url: '/bussiness/device/water/monitor/statistic/' + query.id,
    method: 'get',
    params: {
      date:query.date
    }
  }) 
}