import request from '@/utils/request'

// 查询项目列表
export function listProjectOverview(query) {
  return request({
    url: '/bussiness/project/overview/list',
    method: 'get',
    params: query
  })
}

// 查询项目详细
export function getProjectOverview(id) {
  return request({
    url: '/bussiness/project/overview/' + id,
    method: 'get'
  })
}

// 查询所有企业
export function listCompany() {
  return request({
    url: '/bussiness/project/overview/company/list',
    method: 'get'
  })
}
