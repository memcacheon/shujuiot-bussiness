package com.shujuwuliankeji.ems.cloud.common.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月7日 上午9:08:06
 * @tips Saas注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
public @interface SaaS {
	String[] value() default { "companyId" };
}
