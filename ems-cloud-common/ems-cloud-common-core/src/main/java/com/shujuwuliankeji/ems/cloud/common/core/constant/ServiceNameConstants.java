package com.shujuwuliankeji.ems.cloud.common.core.constant;

/**
 * 服务名称
 * 
 * @author ruoyi
 */
public class ServiceNameConstants {
	/**
	 * 认证服务的serviceid
	 */
	public static final String AUTH_SERVICE = "ems-cloud-auth";

	/**
	 * 系统模块的serviceid
	 */
	public static final String SYSTEM_SERVICE = "ems-cloud-system";

	/**
	 * 文件服务的serviceid
	 */
	public static final String FILE_SERVICE = "ems-cloud-file";

	/**
	 * buss业务serviceId
	 */
	public static final String BUSSINESS_SERVICE = "ems-cloud-bussiness";
}
