package com.shujuwuliankeji.ems.cloud.module.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import com.shujuwuliankeji.ems.cloud.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 文件服务
 * 
 * @author ruoyi
 */
@EnableCustomSwagger2
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class }, scanBasePackages = {
		"com.shujuwuliankeji.ems.cloud" })
public class EmsCloudFileApplication {
	public static void main(String[] args) {
		SpringApplication.run(EmsCloudFileApplication.class, args);
	}
}
