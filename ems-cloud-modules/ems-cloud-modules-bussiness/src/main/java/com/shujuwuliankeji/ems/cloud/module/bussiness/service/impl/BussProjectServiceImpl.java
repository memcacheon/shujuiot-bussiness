package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shujuwuliankeji.ems.cloud.common.core.exception.GlobalException;
import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyUser;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectUser;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.SysUserVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussCompanyUserMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussProjectMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussProjectUserMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectService;
import com.shujuwuliankeji.ems.cloud.system.api.model.LoginUser;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;

/**
 * 项目Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
@Service
public class BussProjectServiceImpl implements IBussProjectService {
	@Autowired
	private BussProjectMapper bussProjectMapper;

	@Autowired
	private BussCompanyUserMapper companyUserMapper;

	@Autowired
	private BussProjectUserMapper projectUserMapper;

	/**
	 * 查询项目
	 * 
	 * @param id 项目主键
	 * @return 项目
	 */
	@Override
	@SaaS
	public BussProject selectBussProjectById(BussProject bussProject) {
		BussProject project = bussProjectMapper.selectBussProjectById(bussProject);
		if (project != null) {
			List<BussProjectUser> projectUsers = project.getProjectUsers();
			if (CollUtil.isNotEmpty(projectUsers)) {
				project.setUserIds(ArrayUtil.toArray(
						projectUsers.stream().map(BussProjectUser::getUserId).collect(Collectors.toList()),
						Long.class));
			}
		}
		return project;
	}

	/**
	 * 查询项目列表
	 * 
	 * @param bussProject 项目
	 * @return 项目
	 */
	@Override
	@SaaS
	public List<BussProject> selectBussProjectList(BussProject bussProject) {
		return bussProjectMapper.selectBussProjectList(bussProject);
	}

	/**
	 * 新增项目
	 * 
	 * @param bussProject 项目
	 * @return 结果
	 */
	@Override
	@SaaS
	@Transactional
	public int insertBussProject(BussProject bussProject) {
		if (checkProjectNameUnique(bussProject) > 0) {
			throw new GlobalException("保存项目'" + bussProject.getProjectName() + "'失败，项目名称已存在");
		}
		LoginUser user = SecurityUtils.getLoginUser();
		bussProject.setCreateTime(DateUtils.getNowDate());
		bussProject.setCreateBy(user.getUsername());
		int rows = bussProjectMapper.insertBussProject(bussProject);
		if (rows > 0) {
			// 插入project_user
			Long[] userIds = bussProject.getUserIds();
			if (userIds != null && userIds.length > 0) {
				List<BussProjectUser> projectUsers = new ArrayList<>();
				for (Long userId : userIds) {
					BussProjectUser temp = new BussProjectUser();
					temp.setCompanyId(bussProject.getCompanyId());
					temp.setProjectId(bussProject.getId());
					temp.setUserId(userId);
					projectUsers.add(temp);
				}
				projectUserMapper.batchBussProjectUser(projectUsers);
			}
		}
		return rows;
	}

	/**
	 * 修改项目
	 * 
	 * @param bussProject 项目
	 * @return 结果
	 */
	@Override
	@SaaS
	public int updateBussProject(BussProject bussProject) {

		BussProject project = bussProjectMapper.selectBussProjectByName(bussProject);

		if (project != null && !project.getId().equals(bussProject.getId())) {
			throw new GlobalException("保存项目'" + bussProject.getProjectName() + "'失败，项目名称已存在");
		}

		LoginUser user = SecurityUtils.getLoginUser();
		bussProject.setUpdateTime(DateUtils.getNowDate());
		bussProject.setUpdateBy(user.getUsername());
		return bussProjectMapper.updateBussProject(bussProject);
	}

	/**
	 * 批量删除项目
	 * 
	 * @param ids 需要删除的项目主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussProjectByIds(BussProject bussProject) {
		// 检测是否可以删除 不能删除规则：项目底下有设备不可以删除、已绑定管理用户不可以删除
		checkCanDelete(bussProject);
		return bussProjectMapper.deleteBussProjectByIds(bussProject);
	}

	/**
	 * 检查是否可以删除
	 * 
	 * @param bussProject
	 */
	private void checkCanDelete(BussProject bussProject) {
		// FOR:TODO
//		int can = bussProjectMapper.checkCanDeleteForUser(bussProject);
//		if (can > 0) {
//			throw new GlobalException("该项目已绑定管理用户");
//		}
//		can = bussProjectMapper.checkCanDeleteForUser(bussProject);
	}

	/**
	 * 删除项目信息
	 * 
	 * @param id 项目主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussProjectById(BussProject bussProject) {
		return bussProjectMapper.deleteBussProjectById(bussProject);
	}

	@Override
	@SaaS
	public int checkProjectNameUnique(BussProject bussProject) {
		return bussProjectMapper.checkProjectNameUnique(bussProject);
	}

	@Override
	public List<SysUserVO> listUser() {
		BussCompanyUser temp = new BussCompanyUser();
		temp.setCompanyId(SecurityUtils.getCompanyId());
		List<BussCompanyUser> selectBussCompanyUserList = companyUserMapper.selectBussCompanyUserList(temp);
		return BeanUtil.copyToList(selectBussCompanyUserList, SysUserVO.class);
	}

	@Override
	public List<BussProject> selectBussProjectByCompanyId(Long companyId) {
		// TODO Auto-generated method stub
		return bussProjectMapper.selectBussProjectByCompanyId(companyId);
	}

	@Override
	public List<BussProject> selectBussProjectAllByCompanyId(Long companyId) {
		// TODO Auto-generated method stub
		return bussProjectMapper.selectBussProjectAllByCompanyId(companyId);
	}
}
