package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.OneNetData.MsgData;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 中台设备数据对象 one_receive_msg
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
public class OneReceiveMsg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public OneReceiveMsg() {
    }

    public OneReceiveMsg(MsgData msgData){
        this.data= msgData.getData();
        this.version=msgData.getVersion();
        this.deviceId=msgData.getDeviceId();
        this.deviceName=msgData.getDeviceName();
        this.messageType=msgData.getMessageType();
        this.notifyType=msgData.getNotifyType();
        this.productId=msgData.getProductId();

    }
    /** ID */
    private Long id;

    /** 数据 */
    @Excel(name = "数据")
    private String data;

    /** 版本 */
    @Excel(name = "版本")
    private String version;

    /** 设备id */
    @Excel(name = "设备id")
    private Long deviceId;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** 消息类型 */
    @Excel(name = "消息类型")
    private String messageType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String notifyType;

    /** 产品id */
    @Excel(name = "产品id")
    private String productId;

    /** 推送数据id */
    @Excel(name = "推送数据id")
    private Long oneReceiveId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setData(String data) 
    {
        this.data = data;
    }

    public String getData() 
    {
        return data;
    }
    public void setVersion(String version) 
    {
        this.version = version;
    }

    public String getVersion() 
    {
        return version;
    }
    public void setDeviceId(Long deviceId) 
    {
        this.deviceId = deviceId;
    }

    public Long getDeviceId() 
    {
        return deviceId;
    }
    public void setDeviceName(String deviceName) 
    {
        this.deviceName = deviceName;
    }

    public String getDeviceName() 
    {
        return deviceName;
    }
    public void setMessageType(String messageType) 
    {
        this.messageType = messageType;
    }

    public String getMessageType() 
    {
        return messageType;
    }
    public void setNotifyType(String notifyType) 
    {
        this.notifyType = notifyType;
    }

    public String getNotifyType() 
    {
        return notifyType;
    }
    public void setProductId(String productId) 
    {
        this.productId = productId;
    }

    public String getProductId() 
    {
        return productId;
    }
    public void setOneReceiveId(Long oneReceiveId) 
    {
        this.oneReceiveId = oneReceiveId;
    }

    public Long getOneReceiveId() 
    {
        return oneReceiveId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("data", getData())
            .append("version", getVersion())
            .append("deviceId", getDeviceId())
            .append("deviceName", getDeviceName())
            .append("messageType", getMessageType())
            .append("notifyType", getNotifyType())
            .append("productId", getProductId())
            .append("oneReceiveId", getOneReceiveId())
            .toString();
    }
}
