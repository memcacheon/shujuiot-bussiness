package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.OneReceive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.OneReceiveMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IOneReceiveService;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;

/**
 * 中台推送数据Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
@Service
public class OneReceiveServiceImpl implements IOneReceiveService 
{
    @Autowired
    private OneReceiveMapper oneReceiveMapper;

	/**
     * 查询中台推送数据
     * 
     * @param id 中台推送数据主键
     * @return 中台推送数据
     */
    @Override
    //@SaaS
    public OneReceive selectOneReceiveById(OneReceive oneReceive)
    {
        return oneReceiveMapper.selectOneReceiveById(oneReceive);
    }

    /**
     * 查询中台推送数据列表
     * 
     * @param oneReceive 中台推送数据
     * @return 中台推送数据
     */
    @Override
    @SaaS
    public List<OneReceive> selectOneReceiveList(OneReceive oneReceive)
    {
        return oneReceiveMapper.selectOneReceiveList(oneReceive);
    }

    /**
     * 新增中台推送数据
     * 
     * @param oneReceive 中台推送数据
     * @return 结果
     */
    @Override
    @SaaS
    public int insertOneReceive(OneReceive oneReceive)
    {
        oneReceive.setCreateTime(DateUtils.getNowDate());
        return oneReceiveMapper.insertOneReceive(oneReceive);
    }

    /**
     * 修改中台推送数据
     * 
     * @param oneReceive 中台推送数据
     * @return 结果
     */
	@SaaS
    @Override
    public int updateOneReceive(OneReceive oneReceive)
    {
        oneReceive.setUpdateTime(DateUtils.getNowDate());
        return oneReceiveMapper.updateOneReceive(oneReceive);
    }

    /**
     * 批量删除中台推送数据
     * 
     * @param ids 需要删除的中台推送数据主键
     * @return 结果
     */
	@SaaS
    @Override
    public int deleteOneReceiveByIds(OneReceive oneReceive)
    {
        return oneReceiveMapper.deleteOneReceiveByIds(oneReceive);
    }

    /**
     * 删除中台推送数据信息
     * 
     * @param id 中台推送数据主键
     * @return 结果
     */
    @Override
    @SaaS
    public int deleteOneReceiveById(OneReceive oneReceive)
    {
        return oneReceiveMapper.deleteOneReceiveById(oneReceive);
    }


    
}
