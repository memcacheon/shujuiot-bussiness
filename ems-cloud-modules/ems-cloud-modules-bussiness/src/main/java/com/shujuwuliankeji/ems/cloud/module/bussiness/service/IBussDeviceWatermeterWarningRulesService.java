package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterWarningRules;

/**
 * 水报警规则Service接口
 * 
 * @author fanzhongjie
 * @date 2023-02-10
 */
public interface IBussDeviceWatermeterWarningRulesService {

	/**
	 * 查询水报警规则
	 * 
	 * @param id 水报警规则主键
	 * @return 水报警规则
	 */
	public BussDeviceWatermeterWarningRules selectBussDeviceWatermeterWarningRulesById(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 查询水报警规则列表
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 水报警规则集合
	 */
	public List<BussDeviceWatermeterWarningRules> selectBussDeviceWatermeterWarningRulesList(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 新增水报警规则
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 结果
	 */
	public int insertBussDeviceWatermeterWarningRules(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 修改水报警规则
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 结果
	 */
	public int updateBussDeviceWatermeterWarningRules(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 批量删除水报警规则
	 * 
	 * @param ids 需要删除的水报警规则主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterWarningRulesByIds(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 删除水报警规则信息
	 * 
	 * @param id 水报警规则主键
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterWarningRulesById(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

}
