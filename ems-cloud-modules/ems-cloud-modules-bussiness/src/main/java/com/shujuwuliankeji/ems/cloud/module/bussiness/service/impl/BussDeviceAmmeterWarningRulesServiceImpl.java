package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterWarningRules;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceAmmeterWarningRulesMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterWarningRulesService;

/**
 * 电报警规则Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
@Service
public class BussDeviceAmmeterWarningRulesServiceImpl implements IBussDeviceAmmeterWarningRulesService {
	@Autowired
	private BussDeviceAmmeterWarningRulesMapper bussDeviceAmmeterWarningRulesMapper;

	/**
	 * 查询电报警规则
	 * 
	 * @param id 电报警规则主键
	 * @return 电报警规则
	 */
	@Override
	@SaaS
	public BussDeviceAmmeterWarningRules selectBussDeviceAmmeterWarningRulesById(
			BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		return bussDeviceAmmeterWarningRulesMapper
				.selectBussDeviceAmmeterWarningRulesById(bussDeviceAmmeterWarningRules);
	}

	/**
	 * 查询电报警规则列表
	 * 
	 * @param bussDeviceAmmeterWarningRules 电报警规则
	 * @return 电报警规则
	 */
	@Override
	@SaaS
	public List<BussDeviceAmmeterWarningRules> selectBussDeviceAmmeterWarningRulesList(
			BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		return bussDeviceAmmeterWarningRulesMapper
				.selectBussDeviceAmmeterWarningRulesList(bussDeviceAmmeterWarningRules);
	}

	/**
	 * 新增电报警规则
	 * 
	 * @param bussDeviceAmmeterWarningRules 电报警规则
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussDeviceAmmeterWarningRules(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		bussDeviceAmmeterWarningRules.setCreateTime(DateUtils.getNowDate());
		return bussDeviceAmmeterWarningRulesMapper.insertBussDeviceAmmeterWarningRules(bussDeviceAmmeterWarningRules);
	}

	/**
	 * 修改电报警规则
	 * 
	 * @param bussDeviceAmmeterWarningRules 电报警规则
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussDeviceAmmeterWarningRules(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		bussDeviceAmmeterWarningRules.setUpdateTime(DateUtils.getNowDate());
		return bussDeviceAmmeterWarningRulesMapper.updateBussDeviceAmmeterWarningRules(bussDeviceAmmeterWarningRules);
	}

	/**
	 * 批量删除电报警规则
	 * 
	 * @param ids 需要删除的电报警规则主键
	 * @return 结果
	 */
	@SaaS
	@Override
	public int deleteBussDeviceAmmeterWarningRulesByIds(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		return bussDeviceAmmeterWarningRulesMapper
				.deleteBussDeviceAmmeterWarningRulesByIds(bussDeviceAmmeterWarningRules);
	}

	/**
	 * 删除电报警规则信息
	 * 
	 * @param id 电报警规则主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussDeviceAmmeterWarningRulesById(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		return bussDeviceAmmeterWarningRulesMapper
				.deleteBussDeviceAmmeterWarningRulesById(bussDeviceAmmeterWarningRules);
	}

}
