package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 电报警对象 buss_device_ammeter_warning
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
public class BussDeviceAmmeterWarning extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 设备ID */
	@Excel(name = "设备ID")
	private Long deviceId;

	/** 企业ID */
	@Excel(name = "企业ID")
	private Long companyId;

	/** 项目ID */
	@Excel(name = "项目ID")
	private Long projectId;

	/** 节点ID */
	@Excel(name = "节点ID")
	private Long nodeId;

	/** 报警事件类型 */
	@Excel(name = "报警事件类型")
	private Integer eventType;

	/** 报警事件信息 */
	@Excel(name = "报警事件信息")
	private String eventInfo;

	/** 是否已读 */
	@Excel(name = "是否已读")
	private Integer isRead;

	/** 报警级别 */
	@Excel(name = "报警级别")
	private Integer eventLevel;

	private String deviceName;

	private String projectName;

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public Long getNodeId() {
		return nodeId;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	public Integer getEventType() {
		return eventType;
	}

	public void setEventInfo(String eventInfo) {
		this.eventInfo = eventInfo;
	}

	public String getEventInfo() {
		return eventInfo;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setEventLevel(Integer eventLevel) {
		this.eventLevel = eventLevel;
	}

	public Integer getEventLevel() {
		return eventLevel;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("deviceId", getDeviceId()).append("companyId", getCompanyId())
				.append("projectId", getProjectId()).append("nodeId", getNodeId()).append("eventType", getEventType())
				.append("eventInfo", getEventInfo()).append("isRead", getIsRead()).append("eventLevel", getEventLevel())
				.toString();
	}
}
