package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 项目对象 buss_project
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
public class BussProject extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 项目名称 */
	@Excel(name = "项目名称")
	private String projectName;

	/** 项目地址 */
	@Excel(name = "项目地址")
	private String address;

	/** 负责人 */
	@Excel(name = "负责人")
	private String linkMan;

	/** 负责人电话 */
	@Excel(name = "负责人电话")
	private String linkTel;

	/** 企业ID */
	@Excel(name = "企业ID")
	private Long companyId;

	/** 项目简介 */
	@Excel(name = "项目简介")
	private String info;

	/** 项目logo */
	@Excel(name = "项目logo")
	private String logo;

	/** 所属公司名称 冗余 */
	@Excel(name = "所属公司名称 冗余")
	private String companyName;

	/**
	 * 项目绑定UserIds
	 */
	private Long[] userIds;

	/**
	 * 沭聚物联项目管理id
	 */
	private String iotProjectId;

	/** 电表项目分组Id */
	private String ammeterGroupId;

	/** 水表项目分组Id */
	private String watermeterGroupId;

	public String getAmmeterGroupId() {
		return ammeterGroupId;
	}

	public void setAmmeterGroupId(String ammeterGroupId) {
		this.ammeterGroupId = ammeterGroupId;
	}

	public String getWatermeterGroupId() {
		return watermeterGroupId;
	}

	public void setWatermeterGroupId(String watermeterGroupId) {
		this.watermeterGroupId = watermeterGroupId;
	}

	public void setProjectUsers(List<BussProjectUser> projectUsers) {
		this.projectUsers = projectUsers;
	}

	@JsonIgnore
	private List<BussProjectUser> projectUsers;

	public List<BussProjectUser> getProjectUsers() {
		return projectUsers;
	}

	public void setUsers(List<BussProjectUser> projectUsers) {
		this.projectUsers = projectUsers;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkTel(String linkTel) {
		this.linkTel = linkTel;
	}

	public String getLinkTel() {
		return linkTel;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getLogo() {
		return logo;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("projectName", getProjectName()).append("address", getAddress()).append("linkMan", getLinkMan())
				.append("linkTel", getLinkTel()).append("companyId", getCompanyId()).append("info", getInfo())
				.append("logo", getLogo()).append("companyName", getCompanyName()).toString();
	}

	public Long[] getUserIds() {
		return userIds;
	}

	public void setUserIds(Long[] userIds) {
		this.userIds = userIds;
	}

	public String getIotProjectId() {
		return iotProjectId;
	}

	public void setIotProjectId(String iotProjectId) {
		this.iotProjectId = iotProjectId;
	}

}
