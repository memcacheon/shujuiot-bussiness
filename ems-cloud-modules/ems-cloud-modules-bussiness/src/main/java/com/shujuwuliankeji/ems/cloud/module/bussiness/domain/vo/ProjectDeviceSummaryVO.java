package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

/**
 * 查询设备概况
 * 
 * @author Fanzhongjie
 * @date 2023年2月4日 上午10:27:06
 * @tips TODO
 */
public class ProjectDeviceSummaryVO {

	/**
	 * 设备总数
	 */
	private int deviceTotal;

	/**
	 * 在线总数
	 */
	private int deviceOnlineTotal;

	/**
	 * 今日能耗 水 用户量 电是用电量
	 * 
	 */

	private double energyConsumption;

	public int getDeviceTotal() {
		return deviceTotal;
	}

	public void setDeviceTotal(int deviceTotal) {
		this.deviceTotal = deviceTotal;
	}

	public int getDeviceOnlineTotal() {
		return deviceOnlineTotal;
	}

	public void setDeviceOnlineTotal(int deviceOnlineTotal) {
		this.deviceOnlineTotal = deviceOnlineTotal;
	}

	public double getEnergyConsumption() {
		return energyConsumption;
	}

	public void setEnergyConsumption(double energyConsumption) {
		this.energyConsumption = energyConsumption;
	}
}
