package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shujuwuliankeji.ems.cloud.common.core.constant.SecurityConstants;
import com.shujuwuliankeji.ems.cloud.common.core.constant.UserConstants;
import com.shujuwuliankeji.ems.cloud.common.core.domain.R;
import com.shujuwuliankeji.ems.cloud.common.core.exception.GlobalException;
import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.core.utils.StringUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyUser;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussCompanyUserMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyUserService;
import com.shujuwuliankeji.ems.cloud.system.api.RemoteRoleService;
import com.shujuwuliankeji.ems.cloud.system.api.model.LoginUser;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;

/**
 * 用户信息Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-07
 */
@Service
public class BussCompanyUserServiceImpl implements IBussCompanyUserService {
	@Autowired
	private BussCompanyUserMapper bussCompanyUserMapper;

	@Autowired
	private RemoteRoleService remoteRoleService;

	/**
	 * 查询用户信息
	 * 
	 * @param userId 用户信息主键
	 * @return 用户信息
	 */
	@Override
	public BussCompanyUser selectBussCompanyUserByUserId(Long userId) {
		return bussCompanyUserMapper.selectBussCompanyUserByUserId(userId);
	}

	/**
	 * 查询用户信息列表
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 用户信息
	 */
	@Override
	public List<BussCompanyUser> selectBussCompanyUserList(BussCompanyUser bussCompanyUser) {
		return bussCompanyUserMapper.selectBussCompanyUserList(bussCompanyUser);
	}

	/**
	 * 新增用户信息
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertBussCompanyUser(BussCompanyUser bussCompanyUser) {
		LoginUser user = SecurityUtils.getLoginUser();
		bussCompanyUser.setCreateTime(DateUtils.getNowDate());
		bussCompanyUser.setCreateBy(user.getUsername());
		int result = bussCompanyUserMapper.insertBussCompanyUser(bussCompanyUser);
		if (result > 0) {
			// 保存权限
			R<String> res = remoteRoleService.saveUserRole(bussCompanyUser.getUserId(), bussCompanyUser.getRoleIds(),
					SecurityConstants.INNER);
			if (res.getCode() != 200) {
				throw new GlobalException("权限保存失败。");
			}
		}
		return result;
	}

	/**
	 * 修改用户信息
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 结果
	 */
	@Override
	public int updateBussCompanyUser(BussCompanyUser bussCompanyUser) {
		bussCompanyUser.setUpdateTime(DateUtils.getNowDate());
		int result = bussCompanyUserMapper.updateBussCompanyUser(bussCompanyUser);
		if (result > 0) {
			// 保存权限
			R<String> res = remoteRoleService.saveUserRole(bussCompanyUser.getUserId(), bussCompanyUser.getRoleIds(),
					SecurityConstants.INNER);
			if (res.getCode() != 200) {
				throw new GlobalException("权限保存失败。");
			}
		}
		return result;
	}

	/**
	 * 批量删除用户信息
	 * 
	 * @param userIds 需要删除的用户信息主键
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteBussCompanyUserByUserIds(Long[] userIds) {
		Long companyId = SecurityUtils.getCompanyId();
		R<Integer> deleteSaasUserRoleRes = remoteRoleService.deleteSaasUserRole(userIds, SecurityConstants.INNER);
		if (deleteSaasUserRoleRes.getCode() != 200) {
			throw new GlobalException("删除SaaS角色关联失败");
		}
		return bussCompanyUserMapper.deleteBussCompanyUserByUserIds(userIds, companyId);
	}

	/**
	 * 删除用户信息信息
	 * 
	 * @param userId 用户信息主键
	 * @return 结果
	 */
	@Override
	public int deleteBussCompanyUserByUserId(Long userId) {
		return bussCompanyUserMapper.deleteBussCompanyUserByUserId(userId);
	}

	@Override
	public int updateUserStatus(BussCompanyUser user) {
		return bussCompanyUserMapper.updateBussCompanyUser(user);
	}

	@Override
	@SaaS
	public BussCompanyUser selectBussCompanyUserOne(BussCompanyUser user) {
		BussCompanyUser one = bussCompanyUserMapper.selectBussCompanyUserOne(user);
//		// 查询用户角色
		R<List<Long>> selectedRoleRes = remoteRoleService.getSelectedSaasRole(user.getUserId(),
				SecurityConstants.INNER);
		if (selectedRoleRes.getCode() == 200) {
			if (CollUtil.isNotEmpty(selectedRoleRes.getData())) {
				one.setRoleIds(ArrayUtil.toArray(selectedRoleRes.getData(), Long.class));
			}
		}
		return one;
	}

	/**
	 * 校验用户名称是否唯一
	 * 
	 * @param user 用户信息
	 * @return 结果
	 */
	@Override
	public String checkUserNameUnique(BussCompanyUser user) {
		Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
		BussCompanyUser info = bussCompanyUserMapper.checkUserNameUnique(user.getUserName());
		if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 校验手机号码是否唯一
	 *
	 * @param user 用户信息
	 * @return
	 */
	@Override
	public String checkPhoneUnique(BussCompanyUser user) {
		Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
		BussCompanyUser info = bussCompanyUserMapper.checkPhoneUnique(user.getPhonenumber());
		if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 校验email是否唯一
	 *
	 * @param user 用户信息
	 * @return
	 */
	@Override
	public String checkEmailUnique(BussCompanyUser user) {
		Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
		BussCompanyUser info = bussCompanyUserMapper.checkEmailUnique(user.getEmail());
		if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	@Override
	@SaaS
	public int resetPwd(BussCompanyUser user) {
		return bussCompanyUserMapper.updateBussCompanyUser(user);
	}
}
