package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeter;

/**
 * 水表管理Service接口
 * 
 * @author fanzhongjie
 * @date 2023-01-30
 */
public interface IBussDeviceWatermeterService {

	/**
	 * 查询水表管理
	 * 
	 * @param id 水表管理主键
	 * @return 水表管理
	 */
	public BussDeviceWatermeter selectBussDeviceWatermeterById(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 查询水表管理列表
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 水表管理集合
	 */
	public List<BussDeviceWatermeter> selectBussDeviceWatermeterList(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 新增水表管理
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 结果
	 */
	public int insertBussDeviceWatermeter(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 修改水表管理
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 结果
	 */
	public int updateBussDeviceWatermeter(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 批量删除水表管理
	 * 
	 * @param ids 需要删除的水表管理主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterByIds(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 删除水表管理信息
	 * 
	 * @param id 水表管理主键
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterById(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 同步水表数据
	 * 
	 * @param bussDevice
	 */
	public void pollData(BussDevice bussDevice);

}
