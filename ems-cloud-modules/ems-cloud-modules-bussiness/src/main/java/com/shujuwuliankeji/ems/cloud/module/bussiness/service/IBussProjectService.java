package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.SysUserVO;

/**
 * 项目Service接口
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
public interface IBussProjectService {
	/**
	 * 查询项目
	 * 
	 * @param id 项目主键
	 * @return 项目
	 */
	public BussProject selectBussProjectById(BussProject bussProject);

	/**
	 * 查询项目列表
	 * 
	 * @param bussProject 项目
	 * @return 项目集合
	 */
	public List<BussProject> selectBussProjectList(BussProject bussProject);

	/**
	 * 新增项目
	 * 
	 * @param bussProject 项目
	 * @return 结果
	 */
	public int insertBussProject(BussProject bussProject);

	/**
	 * 修改项目
	 * 
	 * @param bussProject 项目
	 * @return 结果
	 */
	public int updateBussProject(BussProject bussProject);

	/**
	 * 批量删除项目
	 * 
	 * @param ids 需要删除的项目主键集合
	 * @return 结果
	 */
	public int deleteBussProjectByIds(BussProject bussProject);

	/**
	 * 删除项目信息
	 * 
	 * @param id 项目主键
	 * @return 结果
	 */
	public int deleteBussProjectById(BussProject bussProject);

	/**
	 * 监测项目名称是否存在
	 * 
	 * @param bussProject
	 * @return
	 */
	public int checkProjectNameUnique(BussProject bussProject);

	/**
	 * 获取当前企业下所有的管理用户
	 * 
	 * @return
	 */
	public List<SysUserVO> listUser();

	/**
	 * 根据company_id 获取project
	 * 
	 * @param companyId
	 * @return
	 */
	public List<BussProject> selectBussProjectByCompanyId(Long companyId);

	public List<BussProject> selectBussProjectAllByCompanyId(Long companyId);
}
