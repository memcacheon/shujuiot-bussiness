package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DeviceWatermeterDetailStatisticVO {

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createDate;

	private BigDecimal waterConsumption;
	private Integer waterMeterState;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getWaterConsumption() {
		return waterConsumption;
	}

	public void setWaterConsumption(BigDecimal waterConsumption) {
		this.waterConsumption = waterConsumption;
	}

	public Integer getWaterMeterState() {
		return waterMeterState;
	}

	public void setWaterMeterState(Integer waterMeterState) {
		this.waterMeterState = waterMeterState;
	}

}
