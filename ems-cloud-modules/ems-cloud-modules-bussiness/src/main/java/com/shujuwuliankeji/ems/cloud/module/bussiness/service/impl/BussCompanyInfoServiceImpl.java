package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyInfo;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussCompanyInfoMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyInfoService;

/**
 * 企业信息Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
@Service
public class BussCompanyInfoServiceImpl implements IBussCompanyInfoService {
	@Autowired
	private BussCompanyInfoMapper bussCompanyInfoMapper;

	/**
	 * 查询企业信息
	 * 
	 * @param id 企业信息主键
	 * @return 企业信息
	 */
	@Override
	@SaaS
	public BussCompanyInfo selectBussCompanyInfoById(BussCompanyInfo bussCompanyInfo) {
		return bussCompanyInfoMapper.selectBussCompanyInfoById(bussCompanyInfo);
	}

	/**
	 * 修改企业信息
	 * 
	 * @param bussCompanyInfo 企业信息
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussCompanyInfo(BussCompanyInfo bussCompanyInfo) {
		bussCompanyInfo.setUpdateBy(SecurityUtils.getUsername());
		bussCompanyInfo.setUpdateTime(DateUtils.getNowDate());
		return bussCompanyInfoMapper.updateBussCompanyInfo(bussCompanyInfo);
	}

}
