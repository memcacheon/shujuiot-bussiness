package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.constant.SecurityConstants;
import com.shujuwuliankeji.ems.cloud.common.core.domain.R;
import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.InnerAuth;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompany;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.SysUserVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyService;
import com.shujuwuliankeji.ems.cloud.system.api.RemoteUserService;
import com.shujuwuliankeji.ems.cloud.system.api.domain.SysUser;
import com.shujuwuliankeji.ems.cloud.system.api.model.LoginUser;

import cn.hutool.core.bean.BeanUtil;

/**
 * 企业管理Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-05
 */
@RestController
@RequestMapping("/company")
public class BussCompanyController extends BaseController {
	@Autowired
	private IBussCompanyService bussCompanyService;

	@Autowired
	private RemoteUserService remoteUserService;

	/**
	 * 查询企业管理列表
	 */
	@RequiresPermissions("bussiness:company:list")
	@GetMapping("/list")
	public TableDataInfo list(BussCompany bussCompany) {
		startPage();
		List<BussCompany> list = bussCompanyService.selectBussCompanyList(bussCompany);
		return getDataTable(list);
	}

	/**
	 * 导出企业管理列表
	 */
	@RequiresPermissions("bussiness:company:export")
	@Log(title = "企业管理", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussCompany bussCompany) {
		List<BussCompany> list = bussCompanyService.selectBussCompanyList(bussCompany);
		ExcelUtil<BussCompany> util = new ExcelUtil<BussCompany>(BussCompany.class);
		util.exportExcel(response, list, "企业管理数据");
	}

	/**
	 * 获取企业管理详细信息
	 */
	@RequiresPermissions("bussiness:company:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		return success(bussCompanyService.selectBussCompanyById(id));
	}

	/**
	 * 新增企业管理
	 */
	@RequiresPermissions("bussiness:company:add")
	@Log(title = "企业管理", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussCompany bussCompany) {
		LoginUser user = SecurityUtils.getLoginUser();
		bussCompany.setCreateBy(user.getUsername());
		bussCompany.setCreateTime(DateUtils.getNowDate());
		return toAjax(bussCompanyService.insertBussCompany(bussCompany));
	}

	/**
	 * 修改企业管理
	 */
	@RequiresPermissions("bussiness:company:edit")
	@Log(title = "企业管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussCompany bussCompany) {
		LoginUser user = SecurityUtils.getLoginUser();
		bussCompany.setUpdateBy(user.getUsername());
		bussCompany.setUpdateTime(DateUtils.getNowDate());
		return toAjax(bussCompanyService.updateBussCompany(bussCompany));
	}

	/**
	 * 删除企业管理
	 */
	@RequiresPermissions("bussiness:company:remove")
	@Log(title = "企业管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		return toAjax(bussCompanyService.deleteBussCompanyByIds(ids));
	}

	@RequiresPermissions("bussiness:company:add")
	@GetMapping("user/list")
	public R<List<SysUserVO>> queryAllUser() {
		R<List<SysUser>> userAllList = remoteUserService.userAllList(SecurityConstants.INNER);
		int code = userAllList.getCode();
		if (code == 200) {
			List<SysUser> data = userAllList.getData();
			if (data != null && data.size() > 0) {
				return R.ok(BeanUtil.copyToList(data, SysUserVO.class));
			}
		}
		return R.fail(userAllList.getMsg());
	}

	@InnerAuth
	@GetMapping("user")
	public R<BussCompany> queryCompanyByUserId(@RequestParam Long userId) {
		BussCompany company = bussCompanyService.getCompanyByUserId(userId);
		if (company == null) {
			return R.fail("该账号未绑定企业");
		}
		return R.ok(company);
	}

}
