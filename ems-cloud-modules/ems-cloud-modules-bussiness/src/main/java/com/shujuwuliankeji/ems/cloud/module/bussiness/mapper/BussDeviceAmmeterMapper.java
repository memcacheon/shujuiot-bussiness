package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;

/**
 * 设备管理Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-28
 */
public interface BussDeviceAmmeterMapper {

	/**
	 * 查询设备管理
	 * 
	 * @param id 设备管理主键
	 * @return 设备管理
	 */
	public BussDeviceAmmeter selectBussDeviceAmmeterById(BussDeviceAmmeter bussDevice);

	/**
	 * 查询设备管理列表
	 * 
	 * @param bussDevice 设备管理
	 * @return 设备管理集合
	 */
	public List<BussDeviceAmmeter> selectBussDeviceAmmeterList(BussDeviceAmmeter bussDevice);

	/**
	 * 新增设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int insertBussDeviceAmmeter(BussDeviceAmmeter bussDevice);

	/**
	 * 修改设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int updateBussDeviceAmmeter(BussDeviceAmmeter bussDevice);

	/**
	 * 删除设备管理
	 * 
	 * @param id 设备管理主键
	 * @return 结果
	 */
	public int deleteBussDeviceAmmeterById(BussDeviceAmmeter bussDevice);

	/**
	 * 批量删除设备管理
	 * 
	 * @param ids 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceAmmeterByIds(@Param("entity") BussDeviceAmmeter bussDevice);

}
