package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterData;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.DeviceWatermeterDetailStatisticVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.StatisticVO;

/**
 * 水属性数据Service接口
 * 
 * @author fanzhongjie
 * @date 2023-02-04
 */
public interface IBussDeviceWatermeterDataService {

	/**
	 * 查询水属性数据
	 * 
	 * @param id 水属性数据主键
	 * @return 水属性数据
	 */
	public BussDeviceWatermeterData selectBussDeviceWatermeterDataById(Long id);

	/**
	 * 查询水属性数据列表
	 * 
	 * @param bussDeviceWatermeterData 水属性数据
	 * @return 水属性数据集合
	 */
	public List<BussDeviceWatermeterData> selectBussDeviceWatermeterDataList(
			BussDeviceWatermeterData bussDeviceAmmeterData);

	/**
	 * 查询最新的一条数据信息
	 * 
	 * @param bussDevice
	 * @return
	 */
	public DeviceWatermeterDetailStatisticVO lastDevieData(BussDeviceWatermeter bussDevice);

	/**
	 * 设备详情请求数据 x y
	 * 
	 * @param bussDevice
	 * @return
	 */
	public StatisticVO dataStatistic(BussDeviceWatermeter bussDevice);

}
