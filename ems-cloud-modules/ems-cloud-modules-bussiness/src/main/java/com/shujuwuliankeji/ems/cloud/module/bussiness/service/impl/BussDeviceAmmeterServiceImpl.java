package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.constant.DeviceTypeEnum;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceAmmeterMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceService;

/**
 * 设备管理Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-28
 */
@Service
public class BussDeviceAmmeterServiceImpl implements IBussDeviceAmmeterService {
	@Autowired
	private BussDeviceAmmeterMapper bussDeviceMapper;

	@Autowired
	private IBussDeviceService deviceService;

	/**
	 * 查询设备管理
	 * 
	 * @param id 设备管理主键
	 * @return 设备管理
	 */
	@Override
	@SaaS
	public BussDeviceAmmeter selectBussDeviceById(BussDeviceAmmeter bussDevice) {
		return bussDeviceMapper.selectBussDeviceAmmeterById(bussDevice);
	}

	/**
	 * 查询设备管理列表
	 * 
	 * @param bussDevice 设备管理
	 * @return 设备管理
	 */
	@Override
	@SaaS
	public List<BussDeviceAmmeter> selectBussDeviceList(BussDeviceAmmeter bussDevice) {
		return bussDeviceMapper.selectBussDeviceAmmeterList(bussDevice);
	}

	/**
	 * 新增设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussDevice(BussDeviceAmmeter bussDevice) {
		bussDevice.setCreateTime(DateUtils.getNowDate());
		return bussDeviceMapper.insertBussDeviceAmmeter(bussDevice);
	}

	/**
	 * 修改设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussDevice(BussDeviceAmmeter bussDevice) {
		bussDevice.setUpdateTime(DateUtils.getNowDate());
		return bussDeviceMapper.updateBussDeviceAmmeter(bussDevice);
	}

	/**
	 * 批量删除设备管理
	 * 
	 * @param ids 需要删除的设备管理主键
	 * @return 结果
	 */
	@SaaS
	@Override
	public int deleteBussDeviceByIds(BussDeviceAmmeter bussDevice) {
		return bussDeviceMapper.deleteBussDeviceAmmeterByIds(bussDevice);
	}

	/**
	 * 删除设备管理信息
	 * 
	 * @param id 设备管理主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussDeviceById(BussDeviceAmmeter bussDevice) {
		return bussDeviceMapper.deleteBussDeviceAmmeterById(bussDevice);
	}

	/**
	 * 有关同步数据说明 1、平台只提供在项目里面查询设备列表，非项目中没有设备列表请求接口。
	 * 2、平台规则引擎中，只针对所有产品设备/指定设备进行事件推送，目前没办法根据分组来进行规则引擎推送。
	 * 3、电表、水表是根据项目中分组来的，所以平台必须严格按照设备分组来添加相应的设备。 4、同步设备，通过查询分组内的设备数据来进行同步。
	 * 5、规则引擎中的数据推送由于没有针对分组，所以，先判断数据库中有无该设备，如果没有则通过列表接口查询该设备是否在分组内，如果是则添加设备，如果不是则忽略。
	 * 6、可以根据具体情况进行定时同步。
	 */
	@Override
	@SaaS
	@Async
	public void pollData(BussDevice bussDevice) {
		Long companyId = bussDevice.getCompanyId();
		deviceService.poll(companyId, DeviceTypeEnum.AMMETER);
	}

}
