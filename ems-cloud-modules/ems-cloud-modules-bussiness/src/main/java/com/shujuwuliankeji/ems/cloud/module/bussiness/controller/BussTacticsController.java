package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussTactics;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussTacticsService;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;

/**
 * 策略Controller
 * 
 * @author fanzhongjie
 * @date 2023-03-07
 */
@RestController
@RequestMapping("/tactics")
public class BussTacticsController extends BaseController
{
    @Autowired
    private IBussTacticsService bussTacticsService;


    @GetMapping("/test")
    public void getInfos(){
        System.out.println("hello");
    }
    @PostMapping("/test")
    public void getInfo(){
        System.out.println("hello");
    }
    /**
     * 查询策略列表
     */
    @RequiresPermissions("bussiness:tactics:list")
    @GetMapping("/list")
    public TableDataInfo list(BussTactics bussTactics)
    {
        startPage();
        List<BussTactics> list = bussTacticsService.selectBussTacticsList(bussTactics);
        return getDataTable(list);
    }

    /**
     * 导出策略列表
     */
    @RequiresPermissions("bussiness:tactics:export")
    @Log(title = "策略", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BussTactics bussTactics)
    {
        List<BussTactics> list = bussTacticsService.selectBussTacticsList(bussTactics);
        ExcelUtil<BussTactics> util = new ExcelUtil<BussTactics>(BussTactics.class);
        util.exportExcel(response, list, "策略数据");
    }

  /**
     * 获取策略详细信息
     */
    @RequiresPermissions("bussiness:tactics:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
    	BussTactics bussTactics = new BussTactics() ;
    	bussTactics.setId(id) ;
        return success(bussTacticsService.selectBussTacticsById(bussTactics));
    }
  

    /**
     * 新增策略
     */
    @RequiresPermissions("bussiness:tactics:add")
    @Log(title = "策略", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BussTactics bussTactics)
    {
        return toAjax(bussTacticsService.insertBussTactics(bussTactics));
    }

    /**
     * 修改策略
     */
    @RequiresPermissions("bussiness:tactics:edit")
    @Log(title = "策略", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BussTactics bussTactics)
    {
        return toAjax(bussTacticsService.updateBussTactics(bussTactics));
    }
 /**
     * 删除策略
     */
    @RequiresPermissions("bussiness:tactics:remove")
    @Log(title = "策略", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
    	BussTactics bussTactics = new BussTactics() ;
    	bussTactics.setIds(ids) ;
        return toAjax(bussTacticsService.deleteBussTacticsByIds(bussTactics));
    }
   
}
