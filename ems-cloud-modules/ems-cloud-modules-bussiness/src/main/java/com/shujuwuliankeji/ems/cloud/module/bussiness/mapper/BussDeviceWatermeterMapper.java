package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeter;

/**
 * 水表管理Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-30
 */
public interface BussDeviceWatermeterMapper {

	/**
	 * 查询水表管理
	 * 
	 * @param id 水表管理主键
	 * @return 水表管理
	 */
	public BussDeviceWatermeter selectBussDeviceWatermeterById(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 查询水表管理列表
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 水表管理集合
	 */
	public List<BussDeviceWatermeter> selectBussDeviceWatermeterList(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 新增水表管理
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 结果
	 */
	public int insertBussDeviceWatermeter(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 修改水表管理
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 结果
	 */
	public int updateBussDeviceWatermeter(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 删除水表管理
	 * 
	 * @param id 水表管理主键
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterById(BussDeviceWatermeter bussDeviceWatermeter);

	/**
	 * 批量删除水表管理
	 * 
	 * @param ids 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterByIds(@Param("entity") BussDeviceWatermeter bussDeviceWatermeter);

}
