package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 项目节点对象 buss_project_node
 * 
 * @author fanzhongjie
 * @date 2023-01-12
 */
public class BussProjectNode extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 父菜单ID */
	private Long parentId;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getAncestors() {
		return ancestors;
	}

	public void setAncestors(String ancestors) {
		this.ancestors = ancestors;
	}

	public List<BussProjectNode> getChildren() {
		return children;
	}

	public void setChildren(List<BussProjectNode> children) {
		this.children = children;
	}

	/** 显示顺序 */
	private Integer orderNum;

	/** 祖级列表 */
	private String ancestors;
	/** 子部门 */
	private List<BussProjectNode> children = new ArrayList<>();

	/** 用户ID */
	private Long id;

	/** 项目ID */
	private Long projectId;

	/** 节点名称 */
	@Excel(name = "节点名称")
	private String nodeName;

	/** 节点信息 */
	private String info;

	/** 企业ID */
	private Long companyId;

	/**
	 * 项目名称
	 */
	private String projectName;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("projectId", getProjectId()).append("nodeName", getNodeName()).append("parentId", getParentId())
				.append("orderNum", getOrderNum()).append("info", getInfo()).append("companyId", getCompanyId())
				.toString();
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
