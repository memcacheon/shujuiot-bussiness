package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.OneNetData.HttpParams;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 中台推送数据对象 one_receive
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
public class OneReceive extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public OneReceive() {
    }

    public OneReceive(HttpParams body){
        this.httpId=body.getId();
        this.msg=body.getMsg();
        this.nonce=body.getNonce();
        this.signature=body.getSignature();
        this.time=String.valueOf(body.getTime());
    }
    /** ID */
    private Long id;

    /** 推送内容 */
    @Excel(name = "推送内容")
    private String msg;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String signature;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String time;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String httpId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String nonce;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMsg(String msg) 
    {
        this.msg = msg;
    }

    public String getMsg() 
    {
        return msg;
    }
    public void setSignature(String signature) 
    {
        this.signature = signature;
    }

    public String getSignature() 
    {
        return signature;
    }
    public void setTime(String time) 
    {
        this.time = time;
    }

    public String getTime() 
    {
        return time;
    }
    public void setHttpId(String httpId) 
    {
        this.httpId = httpId;
    }

    public String getHttpId() 
    {
        return httpId;
    }
    public void setNonce(String nonce) 
    {
        this.nonce = nonce;
    }

    public String getNonce() 
    {
        return nonce;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("msg", getMsg())
            .append("signature", getSignature())
            .append("time", getTime())
            .append("httpId", getHttpId())
            .append("nonce", getNonce())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
