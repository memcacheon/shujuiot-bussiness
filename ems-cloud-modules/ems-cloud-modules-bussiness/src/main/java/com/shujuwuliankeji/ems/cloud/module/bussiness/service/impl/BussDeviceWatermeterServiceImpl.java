package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.constant.DeviceTypeEnum;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceWatermeterMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceWatermeterService;

/**
 * 水表管理Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-30
 */
@Service
public class BussDeviceWatermeterServiceImpl implements IBussDeviceWatermeterService {
	@Autowired
	private BussDeviceWatermeterMapper bussDeviceWatermeterMapper;

	@Autowired
	private IBussDeviceService deviceService;

	/**
	 * 查询水表管理
	 * 
	 * @param id 水表管理主键
	 * @return 水表管理
	 */
	@Override
	@SaaS
	public BussDeviceWatermeter selectBussDeviceWatermeterById(BussDeviceWatermeter bussDeviceWatermeter) {
		return bussDeviceWatermeterMapper.selectBussDeviceWatermeterById(bussDeviceWatermeter);
	}

	/**
	 * 查询水表管理列表
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 水表管理
	 */
	@Override
	@SaaS
	public List<BussDeviceWatermeter> selectBussDeviceWatermeterList(BussDeviceWatermeter bussDeviceWatermeter) {
		return bussDeviceWatermeterMapper.selectBussDeviceWatermeterList(bussDeviceWatermeter);
	}

	/**
	 * 新增水表管理
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussDeviceWatermeter(BussDeviceWatermeter bussDeviceWatermeter) {
		bussDeviceWatermeter.setCreateTime(DateUtils.getNowDate());
		return bussDeviceWatermeterMapper.insertBussDeviceWatermeter(bussDeviceWatermeter);
	}

	/**
	 * 修改水表管理
	 * 
	 * @param bussDeviceWatermeter 水表管理
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussDeviceWatermeter(BussDeviceWatermeter bussDeviceWatermeter) {
		bussDeviceWatermeter.setUpdateTime(DateUtils.getNowDate());
		return bussDeviceWatermeterMapper.updateBussDeviceWatermeter(bussDeviceWatermeter);
	}

	/**
	 * 批量删除水表管理
	 * 
	 * @param ids 需要删除的水表管理主键
	 * @return 结果
	 */
	@SaaS
	@Override
	public int deleteBussDeviceWatermeterByIds(BussDeviceWatermeter bussDeviceWatermeter) {
		return bussDeviceWatermeterMapper.deleteBussDeviceWatermeterByIds(bussDeviceWatermeter);
	}

	/**
	 * 删除水表管理信息
	 * 
	 * @param id 水表管理主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussDeviceWatermeterById(BussDeviceWatermeter bussDeviceWatermeter) {
		return bussDeviceWatermeterMapper.deleteBussDeviceWatermeterById(bussDeviceWatermeter);
	}

	@Override
	@SaaS
	@Async
	public void pollData(BussDevice bussDevice) {
		deviceService.poll(bussDevice.getCompanyId(), DeviceTypeEnum.WATERMETER);
	}

}
