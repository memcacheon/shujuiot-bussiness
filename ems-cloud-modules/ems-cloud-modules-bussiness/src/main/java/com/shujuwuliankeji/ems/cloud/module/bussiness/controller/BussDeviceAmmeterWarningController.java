package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterWarning;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.ProjectVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterWarningService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectService;

import cn.hutool.core.bean.BeanUtil;

/**
 * 报警信息Controller
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
@RestController
@RequestMapping("/alarmAmWarning")
public class BussDeviceAmmeterWarningController extends BaseController {
	@Autowired
	private IBussDeviceAmmeterWarningService bussDeviceAmmeterWarningService;
	@Autowired
	private IBussProjectService projectService;

	/**
	 * 查询报警信息列表
	 */
	@RequiresPermissions("bussiness:alarmAmWarning:list")
	@GetMapping("/list")
	public TableDataInfo list(BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		startPage();
		List<BussDeviceAmmeterWarning> list = bussDeviceAmmeterWarningService
				.selectBussDeviceAmmeterWarningList(bussDeviceAmmeterWarning);
		return getDataTable(list);
	}

	/**
	 * 导出报警信息列表
	 */
	@RequiresPermissions("bussiness:alarmAmWarning:export")
	@Log(title = "报警信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		List<BussDeviceAmmeterWarning> list = bussDeviceAmmeterWarningService
				.selectBussDeviceAmmeterWarningList(bussDeviceAmmeterWarning);
		ExcelUtil<BussDeviceAmmeterWarning> util = new ExcelUtil<BussDeviceAmmeterWarning>(
				BussDeviceAmmeterWarning.class);
		util.exportExcel(response, list, "报警信息数据");
	}

	/**
	 * 获取报警信息详细信息
	 */
	@RequiresPermissions("bussiness:alarmAmWarning:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussDeviceAmmeterWarning bussDeviceAmmeterWarning = new BussDeviceAmmeterWarning();
		bussDeviceAmmeterWarning.setId(id);
		return success(bussDeviceAmmeterWarningService.selectBussDeviceAmmeterWarningById(bussDeviceAmmeterWarning));
	}

	/**
	 * 新增报警信息
	 */
	@RequiresPermissions("bussiness:alarmAmWarning:add")
	@Log(title = "报警信息", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		return toAjax(bussDeviceAmmeterWarningService.insertBussDeviceAmmeterWarning(bussDeviceAmmeterWarning));
	}

	/**
	 * 修改报警信息
	 */
	@RequiresPermissions("bussiness:alarmAmWarning:edit")
	@Log(title = "报警信息", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		return toAjax(bussDeviceAmmeterWarningService.updateBussDeviceAmmeterWarning(bussDeviceAmmeterWarning));
	}

	/**
	 * 删除报警信息
	 */
	@RequiresPermissions("bussiness:alarmAmWarning:remove")
	@Log(title = "报警信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussDeviceAmmeterWarning bussDeviceAmmeterWarning = new BussDeviceAmmeterWarning();
		bussDeviceAmmeterWarning.setIds(ids);
		return toAjax(bussDeviceAmmeterWarningService.deleteBussDeviceAmmeterWarningByIds(bussDeviceAmmeterWarning));
	}

	@RequiresPermissions("bussiness:alarmAmWarning:list")
	@GetMapping("project/list")
	public AjaxResult listProject() {
		BussProject temp = new BussProject();
		List<BussProject> projects = projectService.selectBussProjectList(temp);
		return success(BeanUtil.copyToList(projects, ProjectVO.class));
	}

}
