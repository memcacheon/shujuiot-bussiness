package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.OneReceiveMsg;
import org.apache.ibatis.annotations.Param;

/**
 * 中台设备数据Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
public interface OneReceiveMsgMapper 
{

 /**
     * 查询中台设备数据
     * 
     * @param id 中台设备数据主键
     * @return 中台设备数据
     */
    public OneReceiveMsg selectOneReceiveMsgById(OneReceiveMsg oneReceiveMsg);

    /**
     * 查询中台设备数据列表
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 中台设备数据集合
     */
    public List<OneReceiveMsg> selectOneReceiveMsgList(OneReceiveMsg oneReceiveMsg);

    /**
     * 新增中台设备数据
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 结果
     */
    public int insertOneReceiveMsg(OneReceiveMsg oneReceiveMsg);

    /**
     * 修改中台设备数据
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 结果
     */
    public int updateOneReceiveMsg(OneReceiveMsg oneReceiveMsg);

    /**
     * 删除中台设备数据
     * 
     * @param id 中台设备数据主键
     * @return 结果
     */
    public int deleteOneReceiveMsgById(OneReceiveMsg oneReceiveMsg);

    /**
     * 批量删除中台设备数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOneReceiveMsgByIds(@Param("entity") OneReceiveMsg oneReceiveMsg);
}
