package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterWarning;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceAmmeterWarningMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterWarningService;

/**
 * 电报警Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
@Service
public class BussDeviceAmmeterWarningServiceImpl implements IBussDeviceAmmeterWarningService {
	@Autowired
	private BussDeviceAmmeterWarningMapper bussDeviceAmmeterWarningMapper;

	/**
	 * 查询电报警
	 * 
	 * @param id 电报警主键
	 * @return 电报警
	 */
	@Override
	@SaaS
	public BussDeviceAmmeterWarning selectBussDeviceAmmeterWarningById(
			BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		return bussDeviceAmmeterWarningMapper.selectBussDeviceAmmeterWarningById(bussDeviceAmmeterWarning);
	}

	/**
	 * 查询电报警列表
	 * 
	 * @param bussDeviceAmmeterWarning 电报警
	 * @return 电报警
	 */
	@Override
	@SaaS
	public List<BussDeviceAmmeterWarning> selectBussDeviceAmmeterWarningList(
			BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		return bussDeviceAmmeterWarningMapper.selectBussDeviceAmmeterWarningList(bussDeviceAmmeterWarning);
	}

	/**
	 * 新增电报警
	 * 
	 * @param bussDeviceAmmeterWarning 电报警
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussDeviceAmmeterWarning(BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		bussDeviceAmmeterWarning.setCreateTime(DateUtils.getNowDate());
		return bussDeviceAmmeterWarningMapper.insertBussDeviceAmmeterWarning(bussDeviceAmmeterWarning);
	}

	/**
	 * 修改电报警
	 * 
	 * @param bussDeviceAmmeterWarning 电报警
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussDeviceAmmeterWarning(BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		bussDeviceAmmeterWarning.setUpdateTime(DateUtils.getNowDate());
		return bussDeviceAmmeterWarningMapper.updateBussDeviceAmmeterWarning(bussDeviceAmmeterWarning);
	}

	/**
	 * 批量删除电报警
	 * 
	 * @param ids 需要删除的电报警主键
	 * @return 结果
	 */
	@SaaS
	@Override
	public int deleteBussDeviceAmmeterWarningByIds(BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		return bussDeviceAmmeterWarningMapper.deleteBussDeviceAmmeterWarningByIds(bussDeviceAmmeterWarning);
	}

	/**
	 * 删除电报警信息
	 * 
	 * @param id 电报警主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussDeviceAmmeterWarningById(BussDeviceAmmeterWarning bussDeviceAmmeterWarning) {
		return bussDeviceAmmeterWarningMapper.deleteBussDeviceAmmeterWarningById(bussDeviceAmmeterWarning);
	}

}
