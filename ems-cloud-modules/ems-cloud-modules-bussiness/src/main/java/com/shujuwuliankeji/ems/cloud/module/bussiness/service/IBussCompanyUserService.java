package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyUser;

/**
 * 用户信息Service接口
 * 
 * @author fanzhongjie
 * @date 2023-01-07
 */
public interface IBussCompanyUserService {
	/**
	 * 查询用户信息
	 * 
	 * @param userId 用户信息主键
	 * @return 用户信息
	 */
	public BussCompanyUser selectBussCompanyUserByUserId(Long userId);

	/**
	 * 查询单个用户信息
	 * 
	 * @param user
	 * @return
	 */

	public BussCompanyUser selectBussCompanyUserOne(BussCompanyUser user);

	/**
	 * 查询用户信息列表
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 用户信息集合
	 */
	public List<BussCompanyUser> selectBussCompanyUserList(BussCompanyUser bussCompanyUser);

	/**
	 * 新增用户信息
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 结果
	 */
	public int insertBussCompanyUser(BussCompanyUser bussCompanyUser);

	/**
	 * 修改用户信息
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 结果
	 */
	public int updateBussCompanyUser(BussCompanyUser bussCompanyUser);

	/**
	 * 批量删除用户信息
	 * 
	 * @param userIds 需要删除的用户信息主键集合
	 * @return 结果
	 */
	public int deleteBussCompanyUserByUserIds(Long[] userIds);

	/**
	 * 删除用户信息信息
	 * 
	 * @param userId 用户信息主键
	 * @return 结果
	 */
	public int deleteBussCompanyUserByUserId(Long userId);

	/**
	 * 修改用户状态
	 * 
	 * @param user 用户信息
	 * @return 结果
	 */
	public int updateUserStatus(BussCompanyUser user);

	/**
	 * 校验用户名称是否唯一
	 * 
	 * @param user 用户信息
	 * @return 结果
	 */
	public String checkUserNameUnique(BussCompanyUser user);

	/**
	 * 校验手机号码是否唯一
	 *
	 * @param user 用户信息
	 * @return 结果
	 */
	public String checkPhoneUnique(BussCompanyUser user);

	/**
	 * 校验email是否唯一
	 *
	 * @param user 用户信息
	 * @return 结果
	 */
	public String checkEmailUnique(BussCompanyUser user);

	/**
	 * 重置密码
	 * 
	 * @param user
	 * @return
	 */
	public int resetPwd(BussCompanyUser user);

}
