package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterWarning;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceWatermeterWarningService;

/**
 * 报警信息Controller
 * 
 * @author fanzhongjie
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/alarmWaterWarning")
public class BussDeviceWatermeterWarningController extends BaseController {
	@Autowired
	private IBussDeviceWatermeterWarningService bussDeviceWatermeterWarningService;

	/**
	 * 查询报警信息列表
	 */
	@RequiresPermissions("bussiness:alarmWaterWarning:list")
	@GetMapping("/list")
	public TableDataInfo list(BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		startPage();
		List<BussDeviceWatermeterWarning> list = bussDeviceWatermeterWarningService
				.selectBussDeviceWatermeterWarningList(bussDeviceWatermeterWarning);
		return getDataTable(list);
	}

	/**
	 * 导出报警信息列表
	 */
	@RequiresPermissions("bussiness:alarmWaterWarning:export")
	@Log(title = "报警信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		List<BussDeviceWatermeterWarning> list = bussDeviceWatermeterWarningService
				.selectBussDeviceWatermeterWarningList(bussDeviceWatermeterWarning);
		ExcelUtil<BussDeviceWatermeterWarning> util = new ExcelUtil<BussDeviceWatermeterWarning>(
				BussDeviceWatermeterWarning.class);
		util.exportExcel(response, list, "报警信息数据");
	}

	/**
	 * 获取报警信息详细信息
	 */
	@RequiresPermissions("bussiness:alarmWaterWarning:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussDeviceWatermeterWarning bussDeviceWatermeterWarning = new BussDeviceWatermeterWarning();
		bussDeviceWatermeterWarning.setId(id);
		return success(
				bussDeviceWatermeterWarningService.selectBussDeviceWatermeterWarningById(bussDeviceWatermeterWarning));
	}

	/**
	 * 新增报警信息
	 */
	@RequiresPermissions("bussiness:alarmWaterWarning:add")
	@Log(title = "报警信息", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		return toAjax(
				bussDeviceWatermeterWarningService.insertBussDeviceWatermeterWarning(bussDeviceWatermeterWarning));
	}

	/**
	 * 修改报警信息
	 */
	@RequiresPermissions("bussiness:alarmWaterWarning:edit")
	@Log(title = "报警信息", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		return toAjax(
				bussDeviceWatermeterWarningService.updateBussDeviceWatermeterWarning(bussDeviceWatermeterWarning));
	}

	/**
	 * 删除报警信息
	 */
	@RequiresPermissions("bussiness:alarmWaterWarning:remove")
	@Log(title = "报警信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussDeviceWatermeterWarning bussDeviceWatermeterWarning = new BussDeviceWatermeterWarning();
		bussDeviceWatermeterWarning.setIds(ids);
		return toAjax(
				bussDeviceWatermeterWarningService.deleteBussDeviceWatermeterWarningByIds(bussDeviceWatermeterWarning));
	}

}
