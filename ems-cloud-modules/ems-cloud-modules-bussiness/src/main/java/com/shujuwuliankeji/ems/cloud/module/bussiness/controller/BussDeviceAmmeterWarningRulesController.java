package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterWarningRules;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.ProjectVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterWarningRulesService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectService;

import cn.hutool.core.bean.BeanUtil;

/**
 * 电表报警规则Controller
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
@RestController
@RequestMapping("/amAlarmRule")
public class BussDeviceAmmeterWarningRulesController extends BaseController {
	@Autowired
	private IBussDeviceAmmeterWarningRulesService bussDeviceAmmeterWarningRulesService;

	@Autowired
	private IBussProjectService projectService;

	/**
	 * 查询电表报警规则列表
	 */
	@RequiresPermissions("bussiness:amAlarmRule:list")
	@GetMapping("/list")
	public TableDataInfo list(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		startPage();
		List<BussDeviceAmmeterWarningRules> list = bussDeviceAmmeterWarningRulesService
				.selectBussDeviceAmmeterWarningRulesList(bussDeviceAmmeterWarningRules);
		return getDataTable(list);
	}

	/**
	 * 导出电表报警规则列表
	 */
	@RequiresPermissions("bussiness:amAlarmRule:export")
	@Log(title = "电表报警规则", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		List<BussDeviceAmmeterWarningRules> list = bussDeviceAmmeterWarningRulesService
				.selectBussDeviceAmmeterWarningRulesList(bussDeviceAmmeterWarningRules);
		ExcelUtil<BussDeviceAmmeterWarningRules> util = new ExcelUtil<BussDeviceAmmeterWarningRules>(
				BussDeviceAmmeterWarningRules.class);
		util.exportExcel(response, list, "电表报警规则数据");
	}

	/**
	 * 获取电表报警规则详细信息
	 */
	@RequiresPermissions("bussiness:amAlarmRule:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules = new BussDeviceAmmeterWarningRules();
		bussDeviceAmmeterWarningRules.setId(id);
		return success(bussDeviceAmmeterWarningRulesService
				.selectBussDeviceAmmeterWarningRulesById(bussDeviceAmmeterWarningRules));
	}

	/**
	 * 新增电表报警规则
	 */
	@RequiresPermissions("bussiness:amAlarmRule:add")
	@Log(title = "电表报警规则", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		return toAjax(bussDeviceAmmeterWarningRulesService
				.insertBussDeviceAmmeterWarningRules(bussDeviceAmmeterWarningRules));
	}

	/**
	 * 修改电表报警规则
	 */
	@RequiresPermissions("bussiness:amAlarmRule:edit")
	@Log(title = "电表报警规则", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules) {
		return toAjax(bussDeviceAmmeterWarningRulesService
				.updateBussDeviceAmmeterWarningRules(bussDeviceAmmeterWarningRules));
	}

	/**
	 * 删除电表报警规则
	 */
	@RequiresPermissions("bussiness:amAlarmRule:remove")
	@Log(title = "电表报警规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules = new BussDeviceAmmeterWarningRules();
		bussDeviceAmmeterWarningRules.setIds(ids);
		return toAjax(bussDeviceAmmeterWarningRulesService
				.deleteBussDeviceAmmeterWarningRulesByIds(bussDeviceAmmeterWarningRules));
	}

	@RequiresPermissions("bussiness:amAlarmRule:list")
	@GetMapping("project/list")
	public AjaxResult listProject() {
		BussProject temp = new BussProject();
		List<BussProject> projects = projectService.selectBussProjectList(temp);
		return success(BeanUtil.copyToList(projects, ProjectVO.class));
	}

}
