package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shujuwuliankeji.ems.cloud.common.core.exception.GlobalException;
import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompany;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyDept;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussCompanyDeptMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussCompanyMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyService;
import com.shujuwuliankeji.ems.cloud.system.api.model.LoginUser;

/**
 * 项目Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2022-12-30
 */
@Service
public class BussCompanyServiceImpl implements IBussCompanyService {
	@Autowired
	private BussCompanyMapper bussCompanyMapper;

	@Autowired
	private BussCompanyDeptMapper deptMapper;

	/**
	 * 查询项目
	 * 
	 * @param id 项目主键
	 * @return 项目
	 */
	@Override
	public BussCompany selectBussCompanyById(Long id) {
		return bussCompanyMapper.selectBussCompanyById(id);
	}

	/**
	 * 查询项目列表
	 * 
	 * @param bussCompany 项目
	 * @return 项目
	 */
	@Override
	public List<BussCompany> selectBussCompanyList(BussCompany bussCompany) {
		return bussCompanyMapper.selectBussCompanyList(bussCompany);
	}

	/**
	 * 新增项目
	 * 
	 * @param bussCompany 项目
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertBussCompany(BussCompany bussCompany) {

		LoginUser currentUser = SecurityUtils.getLoginUser();
		bussCompany.setCreateBy(currentUser.getUsername());
		bussCompany.setCreateTime(DateUtils.getNowDate());
		// 查询企业是否已经存在
		int companyNameCount = bussCompanyMapper.checkCompanyNameUnique(bussCompany.getCompanyName());
		if (companyNameCount > 0) {
			throw new GlobalException("企业名称已存在");
		}
		// 查询二级域名是否已经存在
		int domainCount = bussCompanyMapper.checkDomainUnique(bussCompany.getDomain());
		if (domainCount > 0) {
			throw new GlobalException("二级域名已存在");
		}
		int result = bussCompanyMapper.insertBussCompany(bussCompany);
		// 新建企业插入企业部门数据
		if (result > 0) {
			BussCompanyDept dept = new BussCompanyDept();
			dept.setDeptName(bussCompany.getCompanyName());
			dept.setCompanyId(bussCompany.getId());
			dept.setAncestors("0");
			dept.setParentId(0L);
			dept.setCreateBy(currentUser.getUsername());
			dept.setCreateTime(DateUtils.getNowDate());
			dept.setDelFlag("0");
			dept.setLeader(bussCompany.getLinkMain());
			dept.setOrderNum(0);
			dept.setPhone(bussCompany.getLinkTel());
			dept.setStatus("0");
			deptMapper.insertDept(dept);
		}
		return result;
	}

	/**
	 * 修改项目
	 * 
	 * @param bussCompany 项目
	 * @return 结果
	 */
	@Override
	public int updateBussCompany(BussCompany bussCompany) {
		bussCompany.setUpdateTime(DateUtils.getNowDate());
		return bussCompanyMapper.updateBussCompany(bussCompany);
	}

	/**
	 * 批量删除项目
	 * 
	 * @param ids 需要删除的项目主键
	 * @return 结果
	 */
	@Override
	public int deleteBussCompanyByIds(Long[] ids) {
		return bussCompanyMapper.deleteBussCompanyByIds(ids);
	}

	/**
	 * 删除项目信息
	 * 
	 * @param id 项目主键
	 * @return 结果
	 */
	@Override
	public int deleteBussCompanyById(Long id) {
		return bussCompanyMapper.deleteBussCompanyById(id);
	}

	@Override
	public List<BussCompany> listAllCompany() {
		return bussCompanyMapper.listAllCompany();
	}

	@Override
	public BussCompany getCompanyByUserId(Long userId) {
		return bussCompanyMapper.getCompanyByUserId(userId);
	}
}
