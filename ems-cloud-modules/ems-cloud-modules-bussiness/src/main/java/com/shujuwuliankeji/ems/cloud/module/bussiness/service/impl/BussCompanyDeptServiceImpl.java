package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.constant.UserConstants;
import com.shujuwuliankeji.ems.cloud.common.core.exception.ServiceException;
import com.shujuwuliankeji.ems.cloud.common.core.text.Convert;
import com.shujuwuliankeji.ems.cloud.common.core.utils.SpringUtils;
import com.shujuwuliankeji.ems.cloud.common.core.utils.StringUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyDept;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.TreeSelect;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussCompanyDeptMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyDeptService;
import com.shujuwuliankeji.ems.cloud.system.api.domain.SysUser;

/**
 * 部门管理 服务实现
 * 
 * @author ruoyi
 */
@Service
public class BussCompanyDeptServiceImpl implements IBussCompanyDeptService {
	@Autowired
	private BussCompanyDeptMapper deptMapper;

	/**
	 * 查询部门管理数据
	 * 
	 * @param dept 部门信息
	 * @return 部门信息集合
	 */
	@Override
	@SaaS
	public List<BussCompanyDept> selectDeptList(BussCompanyDept dept) {
		return deptMapper.selectDeptList(dept);
	}

	/**
	 * 查询部门树结构信息
	 * 
	 * @param dept 部门信息
	 * @return 部门树信息集合
	 */
	@Override
	public List<TreeSelect> selectDeptTreeList(BussCompanyDept dept) {
		List<BussCompanyDept> depts = SpringUtils.getAopProxy(this).selectDeptList(dept);
		return buildDeptTreeSelect(depts);
	}

	/**
	 * 构建前端所需要树结构
	 * 
	 * @param depts 部门列表
	 * @return 树结构列表
	 */
	@Override
	public List<BussCompanyDept> buildDeptTree(List<BussCompanyDept> depts) {
		List<BussCompanyDept> returnList = new ArrayList<BussCompanyDept>();
		List<Long> tempList = depts.stream().map(BussCompanyDept::getDeptId).collect(Collectors.toList());
		for (BussCompanyDept dept : depts) {
			// 如果是顶级节点, 遍历该父节点的所有子节点
			if (!tempList.contains(dept.getParentId())) {
				recursionFn(depts, dept);
				returnList.add(dept);
			}
		}
		if (returnList.isEmpty()) {
			returnList = depts;
		}
		return returnList;
	}

	/**
	 * 构建前端所需要下拉树结构
	 * 
	 * @param depts 部门列表
	 * @return 下拉树结构列表
	 */
	@Override
	public List<TreeSelect> buildDeptTreeSelect(List<BussCompanyDept> depts) {
		List<BussCompanyDept> deptTrees = buildDeptTree(depts);
		return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
	}

	/**
	 * 根据部门ID查询信息
	 * 
	 * @param deptId 部门ID
	 * @return 部门信息
	 */
	@Override
	@SaaS
	public BussCompanyDept selectDeptById(BussCompanyDept dept) {
		return deptMapper.selectDeptById(dept);
	}

	/**
	 * 根据ID查询所有子部门（正常状态）
	 * 
	 * @param deptId 部门ID
	 * @return 子部门数
	 */
	@Override
	public int selectNormalChildrenDeptById(Long deptId) {
		return deptMapper.selectNormalChildrenDeptById(deptId);
	}

	/**
	 * 是否存在子节点
	 * 
	 * @param deptId 部门ID
	 * @return 结果
	 */
	@Override
	public boolean hasChildByDeptId(Long deptId) {
		int result = deptMapper.hasChildByDeptId(deptId);
		return result > 0;
	}

	/**
	 * 查询部门是否存在用户
	 * 
	 * @param deptId 部门ID
	 * @return 结果 true 存在 false 不存在
	 */
	@Override
	public boolean checkDeptExistUser(Long deptId) {
		int result = deptMapper.checkDeptExistUser(deptId);
		return result > 0;
	}

	/**
	 * 校验部门名称是否唯一
	 * 
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public String checkDeptNameUnique(BussCompanyDept dept) {
		Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
		BussCompanyDept info = deptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
		if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 校验部门是否有数据权限
	 * 
	 * @param deptId 部门id
	 */
	@Override
	public void checkDeptDataScope(Long deptId) {
		if (!SysUser.isAdmin(SecurityUtils.getUserId())) {
			BussCompanyDept dept = new BussCompanyDept();
			dept.setDeptId(deptId);
			List<BussCompanyDept> depts = SpringUtils.getAopProxy(this).selectDeptList(dept);
			if (StringUtils.isEmpty(depts)) {
				throw new ServiceException("没有权限访问部门数据！");
			}
		}
	}

	/**
	 * 新增保存部门信息
	 * 
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public int insertDept(BussCompanyDept dept) {
		BussCompanyDept temp = new BussCompanyDept();
		temp.setDeptId(dept.getParentId());
		temp.setCompanyId(dept.getCompanyId());
		BussCompanyDept info = deptMapper.selectDeptById(temp);
		// 如果父节点不为正常状态,则不允许新增子节点
		if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
			throw new ServiceException("部门停用，不允许新增");
		}
		dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
		return deptMapper.insertDept(dept);
	}

	/**
	 * 修改保存部门信息
	 * 
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public int updateDept(BussCompanyDept dept) {
		BussCompanyDept temp = new BussCompanyDept();
		temp.setCompanyId(dept.getCompanyId());
		temp.setDeptId(dept.getParentId());
		BussCompanyDept newParentDept = deptMapper.selectDeptById(temp);
		temp.setDeptId(dept.getDeptId());
		BussCompanyDept oldDept = deptMapper.selectDeptById(temp);
		if (StringUtils.isNotNull(newParentDept) && StringUtils.isNotNull(oldDept)) {
			String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getDeptId();
			String oldAncestors = oldDept.getAncestors();
			dept.setAncestors(newAncestors);
			updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors);
		}
		int result = deptMapper.updateDept(dept);
		if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()) && StringUtils.isNotEmpty(dept.getAncestors())
				&& !StringUtils.equals("0", dept.getAncestors())) {
			// 如果该部门是启用状态，则启用该部门的所有上级部门
			updateParentDeptStatusNormal(dept);
		}
		return result;
	}

	/**
	 * 修改该部门的父级部门状态
	 * 
	 * @param dept 当前部门
	 */
	private void updateParentDeptStatusNormal(BussCompanyDept dept) {
		String ancestors = dept.getAncestors();
		Long[] deptIds = Convert.toLongArray(ancestors);
		deptMapper.updateDeptStatusNormal(deptIds);
	}

	/**
	 * 修改子元素关系
	 * 
	 * @param deptId       被修改的部门ID
	 * @param newAncestors 新的父ID集合
	 * @param oldAncestors 旧的父ID集合
	 */
	public void updateDeptChildren(Long deptId, String newAncestors, String oldAncestors) {
		List<BussCompanyDept> children = deptMapper.selectChildrenDeptById(deptId);
		for (BussCompanyDept child : children) {
			child.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
		}
		if (children.size() > 0) {
			deptMapper.updateDeptChildren(children);
		}
	}

	/**
	 * 删除部门管理信息
	 * 
	 * @param dept
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteDeptById(BussCompanyDept dept) {
		return deptMapper.deleteDeptById(dept);
	}

	/**
	 * 递归列表
	 */
	private void recursionFn(List<BussCompanyDept> list, BussCompanyDept t) {
		// 得到子节点列表
		List<BussCompanyDept> childList = getChildList(list, t);
		t.setChildren(childList);
		for (BussCompanyDept tChild : childList) {
			if (hasChild(list, tChild)) {
				recursionFn(list, tChild);
			}
		}
	}

	/**
	 * 得到子节点列表
	 */
	private List<BussCompanyDept> getChildList(List<BussCompanyDept> list, BussCompanyDept t) {
		List<BussCompanyDept> tlist = new ArrayList<BussCompanyDept>();
		Iterator<BussCompanyDept> it = list.iterator();
		while (it.hasNext()) {
			BussCompanyDept n = (BussCompanyDept) it.next();
			if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getDeptId().longValue()) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	/**
	 * 判断是否有子节点
	 */
	private boolean hasChild(List<BussCompanyDept> list, BussCompanyDept t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}
}
