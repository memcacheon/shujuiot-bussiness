package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年2月4日 下午2:36:02
 * @tips TODO
 */
public class DeviceAmmeterDetailStatisticVO {

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createDate;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 电压
	 */
	private BigDecimal voltage;

	/**
	 * 电流
	 */
	private BigDecimal electricCurrent;

	public BigDecimal getVoltage() {
		return voltage;
	}

	public void setVoltage(BigDecimal voltage) {
		this.voltage = voltage;
	}

	public BigDecimal getElectricCurrent() {
		return electricCurrent;
	}

	public void setElectricCurrent(BigDecimal electricCurrent) {
		this.electricCurrent = electricCurrent;
	}

	public BigDecimal getTotalActivePower() {
		return totalActivePower;
	}

	public void setTotalActivePower(BigDecimal totalActivePower) {
		this.totalActivePower = totalActivePower;
	}

	/**
	 * 功率
	 */
	private BigDecimal totalActivePower;
}
