package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussTactics;
import org.apache.ibatis.annotations.Param;

/**
 * 策略Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-03-07
 */
public interface BussTacticsMapper 
{

 /**
     * 查询策略
     * 
     * @param id 策略主键
     * @return 策略
     */
    public BussTactics selectBussTacticsById(BussTactics bussTactics);

    /**
     * 查询策略列表
     * 
     * @param bussTactics 策略
     * @return 策略集合
     */
    public List<BussTactics> selectBussTacticsList(BussTactics bussTactics);

    /**
     * 新增策略
     * 
     * @param bussTactics 策略
     * @return 结果
     */
    public int insertBussTactics(BussTactics bussTactics);

    /**
     * 修改策略
     * 
     * @param bussTactics 策略
     * @return 结果
     */
    public int updateBussTactics(BussTactics bussTactics);

    /**
     * 删除策略
     * 
     * @param id 策略主键
     * @return 结果
     */
    public int deleteBussTacticsById(BussTactics bussTactics);

    /**
     * 批量删除策略
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBussTacticsByIds(@Param("entity") BussTactics bussTactics);
}
