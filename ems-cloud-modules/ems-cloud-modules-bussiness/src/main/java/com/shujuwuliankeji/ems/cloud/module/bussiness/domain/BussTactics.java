package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 策略对象 buss_tactics
 *
 * @author fanzhongjie
 * @date 2023-03-08
 */
public class BussTactics extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 策略名称 */
    @Excel(name = "策略名称")
    private String tacticsName;

    /** 策略类型 1 照明 2 路灯 3 集中器 */
    @Excel(name = "策略类型 1 照明 2 路灯 3 集中器")
    private String tacticsType;

    /** 开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date beginTime;

    /** 结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 企业id
     */
    @Excel(name = "企业id")
    private Long companyId;

    /** 项目id */
    @Excel(name = "项目id")
    private Long projectId;

    /** 计划类型  1 定时计划 2 周期计划 */
    @Excel(name = "计划类型  1 定时计划 2 周期计划")
    private String planType;

    /** 周期开启日 */
    @Excel(name = "周期开启日")
    private String begin;

    /** 定时计划执行日 */
    @Excel(name = "定时计划执行日")
    private String operateDate;

    /** 周期间隔时长(小时) */
    @Excel(name = "周期间隔时长(小时)")
    private String period;

    //策略详细
    private List<BussTacticsDetail> detailList;


    public List<BussTacticsDetail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<BussTacticsDetail> detailList) {
        this.detailList = detailList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setTacticsName(String tacticsName)
    {
        this.tacticsName = tacticsName;
    }

    public String getTacticsName()
    {
        return tacticsName;
    }
    public void setTacticsType(String tacticsType)
    {
        this.tacticsType = tacticsType;
    }

    public String getTacticsType()
    {
        return tacticsType;
    }
    public void setBeginTime(Date beginTime)
    {
        this.beginTime = beginTime;
    }

    public Date getBeginTime()
    {
        return beginTime;
    }
    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    public Date getEndTime()
    {
        return endTime;
    }
    public void setCompanyId(Long companyId)
    {
        this.companyId = companyId;
    }

    public Long getCompanyId()
    {
        return companyId;
    }
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    public Long getProjectId()
    {
        return projectId;
    }
    public void setPlanType(String planType)
    {
        this.planType = planType;
    }

    public String getPlanType()
    {
        return planType;
    }
    public void setBegin(String begin)
    {
        this.begin = begin;
    }

    public String getBegin()
    {
        return begin;
    }
    public void setOperateDate(String operateDate)
    {
        this.operateDate = operateDate;
    }

    public String getOperateDate()
    {
        return operateDate;
    }
    public void setPeriod(String period)
    {
        this.period = period;
    }

    public String getPeriod()
    {
        return period;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("tacticsName", getTacticsName())
                .append("tacticsType", getTacticsType())
                .append("beginTime", getBeginTime())
                .append("endTime", getEndTime())
                .append("companyId", getCompanyId())
                .append("projectId", getProjectId())
                .append("planType", getPlanType())
                .append("begin", getBegin())
                .append("operateDate", getOperateDate())
                .append("period", getPeriod())
                .toString();
    }
}
