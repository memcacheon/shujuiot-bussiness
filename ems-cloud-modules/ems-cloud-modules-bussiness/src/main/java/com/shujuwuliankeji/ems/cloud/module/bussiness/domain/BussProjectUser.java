package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 项目用户关联对象 buss_project_user
 * 
 * @author fanzhongjie
 * @date 2023-01-06
 */
public class BussProjectUser extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** 用户ID */
	private Long userId;

	/** 项目ID */
	private Long projectId;

	/** 企业ID */
	@Excel(name = "企业ID")
	private Long companyId;

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("userId", getUserId())
				.append("projectId", getProjectId()).append("companyId", getCompanyId()).toString();
	}
}
