package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.constant.DeviceTypeEnum;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectNode;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.NodeTreeSelect;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterDataService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectNodeService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectService;

/**
 * 电表Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-28
 */
@RestController
@RequestMapping("/device/am/monitor")
public class BussDeviceAmMonitorController extends BaseController {
	@Autowired
	private IBussDeviceService deviceService;

	@Autowired
	private IBussDeviceAmmeterDataService ammeterDataService;

	@Autowired
	private IBussProjectService projectService;

	@Autowired
	private IBussProjectNodeService nodeService;

	/**
	 * 查询项目节点信息
	 */
	@RequiresPermissions("bussiness:monitor:am:list")
	@GetMapping("/project/list")
	public AjaxResult projectList() {
		List<BussProject> list = projectService.selectBussProjectList(new BussProject());
		return success(list);
	}

	/**
	 * 查询项目节点信息
	 */
	@RequiresPermissions("bussiness:monitor:am:list")
	@GetMapping("/project/node/list")
	public AjaxResult treeNode(BussProjectNode projectNode) {
		List<NodeTreeSelect> selectNodeTreeList = nodeService.selectNodeTreeList(projectNode);
		return success(selectNodeTreeList);
	}

	/**
	 * 查询电表管理列表
	 */
	@RequiresPermissions("bussiness:monitor:am:list")
	@GetMapping("/list")
	public TableDataInfo list(BussDevice bussDevice) {
		startPage();
		bussDevice.setDeviceType(DeviceTypeEnum.AMMETER.getValue());
		List<BussDevice> list = deviceService.selectBussDeviceList(bussDevice);
		return getDataTable(list);
	}

	/**
	 * 获取电表管理详细信息
	 */
	@RequiresPermissions("bussiness:monitor:am:list")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussDeviceAmmeter bussDevice = new BussDeviceAmmeter();
		bussDevice.setId(id);
		return success(ammeterDataService.lastDevieData(bussDevice));
	}

	/**
	 * 获取电表当天统计数据
	 */
	@RequiresPermissions("bussiness:monitor:am:list")
	@GetMapping(value = "statistic/{id}")
	public AjaxResult dataStatistic(@PathVariable("id") Long id, @RequestParam String field,
			@RequestParam(required = false) String date) {
		BussDeviceAmmeter bussDevice = new BussDeviceAmmeter();
		bussDevice.setId(id);
		bussDevice.setDate(date);
		bussDevice.setField(field);
		return success(ammeterDataService.dataStatistic(bussDevice));
	}

	/**
	 * 统计设备信息
	 * 
	 * @param device
	 * @return
	 */
	@RequiresPermissions("bussiness:monitor:am:list")
	@GetMapping("project/summary")
	public AjaxResult projectSummary(BussDevice device) {
		device.setDeviceType(DeviceTypeEnum.AMMETER.getValue());
		return success(deviceService.projectSummary(device));
	}
}
