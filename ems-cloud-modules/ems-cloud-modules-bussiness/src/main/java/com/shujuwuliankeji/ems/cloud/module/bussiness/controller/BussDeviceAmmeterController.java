package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterService;

/**
 * 电表管理Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-28
 */
@RestController
@RequestMapping("/device/ammeter")
public class BussDeviceAmmeterController extends BaseController {
	@Autowired
	private IBussDeviceAmmeterService bussDeviceService;

	/**
	 * 查询电表管理列表
	 */
	@RequiresPermissions("bussiness:device:ammeter:list")
	@GetMapping("/list")
	public TableDataInfo list(BussDeviceAmmeter bussDevice) {
		startPage();
		List<BussDeviceAmmeter> list = bussDeviceService.selectBussDeviceList(bussDevice);
		return getDataTable(list);
	}

	/**
	 * 导出电表管理列表
	 */
	@RequiresPermissions("bussiness:device:ammeter:export")
	@Log(title = "电表管理", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussDeviceAmmeter bussDevice) {
		List<BussDeviceAmmeter> list = bussDeviceService.selectBussDeviceList(bussDevice);
		ExcelUtil<BussDeviceAmmeter> util = new ExcelUtil<BussDeviceAmmeter>(BussDeviceAmmeter.class);
		util.exportExcel(response, list, "电表管理数据");
	}

	/**
	 * 获取电表管理详细信息
	 */
	@RequiresPermissions("bussiness:device:ammeter:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussDeviceAmmeter bussDevice = new BussDeviceAmmeter();
		bussDevice.setId(id);
		return success(bussDeviceService.selectBussDeviceById(bussDevice));
	}

	/**
	 * 新增电表管理
	 */
	@RequiresPermissions("bussiness:device:ammeter:add")
	@Log(title = "电表管理", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussDeviceAmmeter bussDevice) {
		return toAjax(bussDeviceService.insertBussDevice(bussDevice));
	}

	/**
	 * 修改电表管理
	 */
	@RequiresPermissions("bussiness:device:ammeter:edit")
	@Log(title = "电表管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussDeviceAmmeter bussDevice) {
		return toAjax(bussDeviceService.updateBussDevice(bussDevice));
	}

	/**
	 * 删除电表管理
	 */
	@RequiresPermissions("bussiness:device:ammeter:remove")
	@Log(title = "电表管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussDeviceAmmeter bussDevice = new BussDeviceAmmeter();
		bussDevice.setIds(ids);
		return toAjax(bussDeviceService.deleteBussDeviceByIds(bussDevice));
	}

	@RequiresPermissions("bussiness:device:ammeter:poll")
	@Log(title = "电表管理", businessType = BusinessType.OTHER)
	@GetMapping("poll")
	public AjaxResult pollData() {
		BussDevice bussDevice = new BussDevice();
		bussDeviceService.pollData(bussDevice);
		return success();
	}

}
