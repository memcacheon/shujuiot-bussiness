package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 电属性数据对象 buss_device_ammeter_data
 * 
 * @author fanzhongjie
 * @date 2023-02-04
 */
public class BussDeviceAmmeterData extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 设备ID */
	@Excel(name = "设备ID")
	private Long deviceId;

	/** A相电流 安培 / A */
	@Excel(name = "A相电流 安培 / A")
	private BigDecimal ia;

	/** B相电流 安培 / A */
	@Excel(name = "B相电流 安培 / A")
	private BigDecimal ib;

	/** C相电流 安培 / A */
	@Excel(name = "C相电流 安培 / A")
	private BigDecimal ic;

	/** A相电压 伏特 / V */
	@Excel(name = "A相电压 伏特 / V")
	private BigDecimal ua;

	/** B相电压 伏特 / V */
	@Excel(name = "B相电压 伏特 / V")
	private BigDecimal ub;

	/** C相电压 伏特 / V */
	@Excel(name = "C相电压 伏特 / V")
	private BigDecimal uc;

	/** AB线电压 伏特 / V */
	@Excel(name = "AB线电压 伏特 / V")
	private BigDecimal uab;

	/** AC线电压 伏特 / V */
	@Excel(name = "AC线电压 伏特 / V")
	private BigDecimal uac;

	/** BC线电压 伏特 / V */
	@Excel(name = "BC线电压 伏特 / V")
	private BigDecimal ubc;

	/** 总有功功率 千瓦 / kW */
	@Excel(name = "总有功功率 千瓦 / kW")
	private BigDecimal p;

	/** A相有功功率 千瓦 / kW */
	@Excel(name = "A相有功功率 千瓦 / kW")
	private BigDecimal pa;

	/** B相有功功率 千瓦 / kW */
	@Excel(name = "B相有功功率 千瓦 / kW")
	private BigDecimal pb;

	/** C相有功功率 千瓦 / kW */
	@Excel(name = "C相有功功率 千瓦 / kW")
	private BigDecimal pc;

	/** 总无功功率 千瓦 / kW */
	@Excel(name = "总无功功率 千瓦 / kW")
	private BigDecimal q;

	/** A相无功功率 千瓦 / kW */
	@Excel(name = "A相无功功率 千瓦 / kW")
	private BigDecimal qa;

	/** B相无功功率 千瓦 / kW */
	@Excel(name = "B相无功功率 千瓦 / kW")
	private BigDecimal qb;

	/** C相无功功率 千瓦 / kW */
	@Excel(name = "C相无功功率 千瓦 / kW")
	private BigDecimal qc;

	/** 有功需量 千瓦 / kW */
	@Excel(name = "有功需量 千瓦 / kW")
	private BigDecimal pdmd;

	/** 总视在功率 千瓦 / kW */
	@Excel(name = "总视在功率 千瓦 / kW")
	private BigDecimal s;

	/** A相视在功率 千瓦 / kW */
	@Excel(name = "A相视在功率  千瓦 / kW")
	private BigDecimal sa;

	/** B相视在功率 千瓦 / kW */
	@Excel(name = "B相视在功率  千瓦 / kW")
	private BigDecimal sb;

	/** C相视在功率 千瓦 / kW */
	@Excel(name = "C相视在功率  千瓦 / kW")
	private BigDecimal sc;

	/** 总功率因数 */
	@Excel(name = "总功率因数")
	private BigDecimal cosq;

	/** A相功率因数 */
	@Excel(name = "A相功率因数")
	private BigDecimal cosa;

	/** B相功率因数 */
	@Excel(name = "B相功率因数")
	private BigDecimal cosb;

	/** C相功率因数 */
	@Excel(name = "C相功率因数")
	private BigDecimal cosc;

	/** 频率 赫兹 / Hz */
	@Excel(name = "频率 赫兹 / Hz")
	private BigDecimal f;

	/** 总有功电能 千瓦·时(度) / kW·h */
	@Excel(name = "总有功电能  千瓦·时(度) / kW·h")
	private BigDecimal ep;

	/** 总有功尖电能 千瓦·时(度) / kW·h */
	@Excel(name = "总有功尖电能 千瓦·时(度) / kW·h")
	private BigDecimal ep1;

	/** 总有功峰电能 千瓦·时(度) / kW·h */
	@Excel(name = "总有功峰电能 千瓦·时(度) / kW·h")
	private BigDecimal ep2;

	/** 总有功平电能 千瓦·时(度) / kW·h */
	@Excel(name = "总有功平电能 千瓦·时(度) / kW·h")
	private BigDecimal ep3;

	/** 总有功谷电能 千瓦·时(度) / kW·h */
	@Excel(name = "总有功谷电能 千瓦·时(度) / kW·h")
	private BigDecimal ep4;

	/** 总无功电能 千瓦·时(度) / kW·h */
	@Excel(name = "总无功电能 千瓦·时(度) / kW·h ")
	private BigDecimal eq;

	/** 回路出线侧A相温度 摄氏度 / ℃ */
	@Excel(name = "回路出线侧A相温度 摄氏度 / ℃")
	private BigDecimal tOutA;

	/** 回路出线侧B相温度 摄氏度 / ℃ */
	@Excel(name = "回路出线侧B相温度 摄氏度 / ℃")
	private BigDecimal tOutB;

	/** 回路出线侧C相温度 摄氏度 / ℃ */
	@Excel(name = "回路出线侧C相温度 摄氏度 / ℃")
	private BigDecimal tOutC;

	/** A相电流总谐波 百分比 / % */
	@Excel(name = "A相电流总谐波 百分比 / %")
	private BigDecimal iAThd;

	/** B相电流总谐波 百分比 / % */
	@Excel(name = "B相电流总谐波 百分比 / %")
	private BigDecimal iBThd;

	/** C相电流总谐波 百分比 / % */
	@Excel(name = "C相电流总谐波 百分比 / %")
	private BigDecimal iCThd;

	/** A相电压总谐波 百分比 / % */
	@Excel(name = "A相电压总谐波  百分比 / %")
	private BigDecimal uAThd;

	/** B相电压总谐波 百分比 / % */
	@Excel(name = "B相电压总谐波  百分比 / %")
	private BigDecimal uBThd;

	/** C相电压总谐波 百分比 / % */
	@Excel(name = "C相电压总谐波  百分比 / %")
	private BigDecimal uCThd;

	/** 主回路开关合闸状态 1 闭合 0断开 */
	@Excel(name = "主回路开关合闸状态 1 闭合 0断开")
	private Integer cSwitch;

	/** 剩余电流 毫安 / mA */
	@Excel(name = "剩余电流 毫安 / mA")
	private BigDecimal rc;

	/** 表计通讯状态 1正常 0异常 */
	@Excel(name = "表计通讯状态 1正常 0异常")
	private Integer mStatus;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setIa(BigDecimal ia) {
		this.ia = ia;
	}

	public BigDecimal getIa() {
		return ia;
	}

	public void setIb(BigDecimal ib) {
		this.ib = ib;
	}

	public BigDecimal getIb() {
		return ib;
	}

	public void setIc(BigDecimal ic) {
		this.ic = ic;
	}

	public BigDecimal getIc() {
		return ic;
	}

	public void setUa(BigDecimal ua) {
		this.ua = ua;
	}

	public BigDecimal getUa() {
		return ua;
	}

	public void setUb(BigDecimal ub) {
		this.ub = ub;
	}

	public BigDecimal getUb() {
		return ub;
	}

	public void setUc(BigDecimal uc) {
		this.uc = uc;
	}

	public BigDecimal getUc() {
		return uc;
	}

	public void setUab(BigDecimal uab) {
		this.uab = uab;
	}

	public BigDecimal getUab() {
		return uab;
	}

	public void setUac(BigDecimal uac) {
		this.uac = uac;
	}

	public BigDecimal getUac() {
		return uac;
	}

	public void setUbc(BigDecimal ubc) {
		this.ubc = ubc;
	}

	public BigDecimal getUbc() {
		return ubc;
	}

	public void setP(BigDecimal p) {
		this.p = p;
	}

	public BigDecimal getP() {
		return p;
	}

	public void setPa(BigDecimal pa) {
		this.pa = pa;
	}

	public BigDecimal getPa() {
		return pa;
	}

	public void setPb(BigDecimal pb) {
		this.pb = pb;
	}

	public BigDecimal getPb() {
		return pb;
	}

	public void setPc(BigDecimal pc) {
		this.pc = pc;
	}

	public BigDecimal getPc() {
		return pc;
	}

	public void setQ(BigDecimal q) {
		this.q = q;
	}

	public BigDecimal getQ() {
		return q;
	}

	public void setQa(BigDecimal qa) {
		this.qa = qa;
	}

	public BigDecimal getQa() {
		return qa;
	}

	public void setQb(BigDecimal qb) {
		this.qb = qb;
	}

	public BigDecimal getQb() {
		return qb;
	}

	public void setQc(BigDecimal qc) {
		this.qc = qc;
	}

	public BigDecimal getQc() {
		return qc;
	}

	public void setPdmd(BigDecimal pdmd) {
		this.pdmd = pdmd;
	}

	public BigDecimal getPdmd() {
		return pdmd;
	}

	public void setS(BigDecimal s) {
		this.s = s;
	}

	public BigDecimal getS() {
		return s;
	}

	public void setSa(BigDecimal sa) {
		this.sa = sa;
	}

	public BigDecimal getSa() {
		return sa;
	}

	public void setSb(BigDecimal sb) {
		this.sb = sb;
	}

	public BigDecimal getSb() {
		return sb;
	}

	public void setSc(BigDecimal sc) {
		this.sc = sc;
	}

	public BigDecimal getSc() {
		return sc;
	}

	public void setCosq(BigDecimal cosq) {
		this.cosq = cosq;
	}

	public BigDecimal getCosq() {
		return cosq;
	}

	public void setCosa(BigDecimal cosa) {
		this.cosa = cosa;
	}

	public BigDecimal getCosa() {
		return cosa;
	}

	public void setCosb(BigDecimal cosb) {
		this.cosb = cosb;
	}

	public BigDecimal getCosb() {
		return cosb;
	}

	public void setCosc(BigDecimal cosc) {
		this.cosc = cosc;
	}

	public BigDecimal getCosc() {
		return cosc;
	}

	public void setF(BigDecimal f) {
		this.f = f;
	}

	public BigDecimal getF() {
		return f;
	}

	public void setEp(BigDecimal ep) {
		this.ep = ep;
	}

	public BigDecimal getEp() {
		return ep;
	}

	public void setEp1(BigDecimal ep1) {
		this.ep1 = ep1;
	}

	public BigDecimal getEp1() {
		return ep1;
	}

	public void setEp2(BigDecimal ep2) {
		this.ep2 = ep2;
	}

	public BigDecimal getEp2() {
		return ep2;
	}

	public void setEp3(BigDecimal ep3) {
		this.ep3 = ep3;
	}

	public BigDecimal getEp3() {
		return ep3;
	}

	public void setEp4(BigDecimal ep4) {
		this.ep4 = ep4;
	}

	public BigDecimal getEp4() {
		return ep4;
	}

	public void setEq(BigDecimal eq) {
		this.eq = eq;
	}

	public BigDecimal getEq() {
		return eq;
	}

	public void settOutA(BigDecimal tOutA) {
		this.tOutA = tOutA;
	}

	public BigDecimal gettOutA() {
		return tOutA;
	}

	public void settOutB(BigDecimal tOutB) {
		this.tOutB = tOutB;
	}

	public BigDecimal gettOutB() {
		return tOutB;
	}

	public void settOutC(BigDecimal tOutC) {
		this.tOutC = tOutC;
	}

	public BigDecimal gettOutC() {
		return tOutC;
	}

	public void setiAThd(BigDecimal iAThd) {
		this.iAThd = iAThd;
	}

	public BigDecimal getiAThd() {
		return iAThd;
	}

	public void setiBThd(BigDecimal iBThd) {
		this.iBThd = iBThd;
	}

	public BigDecimal getiBThd() {
		return iBThd;
	}

	public void setiCThd(BigDecimal iCThd) {
		this.iCThd = iCThd;
	}

	public BigDecimal getiCThd() {
		return iCThd;
	}

	public void setuAThd(BigDecimal uAThd) {
		this.uAThd = uAThd;
	}

	public BigDecimal getuAThd() {
		return uAThd;
	}

	public void setuBThd(BigDecimal uBThd) {
		this.uBThd = uBThd;
	}

	public BigDecimal getuBThd() {
		return uBThd;
	}

	public void setuCThd(BigDecimal uCThd) {
		this.uCThd = uCThd;
	}

	public BigDecimal getuCThd() {
		return uCThd;
	}

	public void setcSwitch(Integer cSwitch) {
		this.cSwitch = cSwitch;
	}

	public Integer getcSwitch() {
		return cSwitch;
	}

	public void setRc(BigDecimal rc) {
		this.rc = rc;
	}

	public BigDecimal getRc() {
		return rc;
	}

	public void setmStatus(Integer mStatus) {
		this.mStatus = mStatus;
	}

	public Integer getmStatus() {
		return mStatus;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("deviceId", getDeviceId()).append("ia", getIa()).append("ib", getIb()).append("ic", getIc())
				.append("ua", getUa()).append("ub", getUb()).append("uc", getUc()).append("uab", getUab())
				.append("uac", getUac()).append("ubc", getUbc()).append("p", getP()).append("pa", getPa())
				.append("pb", getPb()).append("pc", getPc()).append("q", getQ()).append("qa", getQa())
				.append("qb", getQb()).append("qc", getQc()).append("pdmd", getPdmd()).append("s", getS())
				.append("sa", getSa()).append("sb", getSb()).append("sc", getSc()).append("cosq", getCosq())
				.append("cosa", getCosa()).append("cosb", getCosb()).append("cosc", getCosc()).append("f", getF())
				.append("ep", getEp()).append("ep1", getEp1()).append("ep2", getEp2()).append("ep3", getEp3())
				.append("ep4", getEp4()).append("eq", getEq()).append("tOutA", gettOutA()).append("tOutB", gettOutB())
				.append("tOutC", gettOutC()).append("iAThd", getiAThd()).append("iBThd", getiBThd())
				.append("iCThd", getiCThd()).append("uAThd", getuAThd()).append("uBThd", getuBThd())
				.append("uCThd", getuCThd()).append("cSwitch", getcSwitch()).append("rc", getRc())
				.append("mStatus", getmStatus()).toString();
	}
}
