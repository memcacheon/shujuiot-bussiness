package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 水属性数据对象 buss_device_watermeter_data
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
public class BussDeviceWatermeterData extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 设备ID */
	@Excel(name = "设备ID")
	private Long deviceId;

	/** 水表模块状态 0 正常通信 1 通信失败 2 设备异常 3 低电量 */
	@Excel(name = "水表模块状态 0 正常通信 1 通信失败 2 设备异常 3 低电量")
	private Integer waterMeterState;

	/** 用水量 立方米 / m³ */
	@Excel(name = "用水量 立方米 / m³")
	private BigDecimal waterConsumption;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setWaterMeterState(Integer waterMeterState) {
		this.waterMeterState = waterMeterState;
	}

	public Integer getWaterMeterState() {
		return waterMeterState;
	}

	public void setWaterConsumption(BigDecimal waterConsumption) {
		this.waterConsumption = waterConsumption;
	}

	public BigDecimal getWaterConsumption() {
		return waterConsumption;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("deviceId", getDeviceId()).append("waterMeterState", getWaterMeterState())
				.append("waterConsumption", getWaterConsumption()).toString();
	}
}
