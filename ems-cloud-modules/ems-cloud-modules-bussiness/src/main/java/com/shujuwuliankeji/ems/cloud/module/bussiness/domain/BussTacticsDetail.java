package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 策略详细对象 buss_tactics_detail
 * 
 * @author fanzhongjie
 * @date 2023-03-08
 */
public class BussTacticsDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 企业id
 */
    @Excel(name = "企业id ")
    private Long companyId;

    /** 项目id */
    @Excel(name = "项目id")
    private Long projectId;

    /** 策略id */
    @Excel(name = "策略id")
    private Long tacticsId;


    /** 执行操作（开 关） */
    @Excel(name = "执行操作", readConverterExp = "开=,关=")
    private String operate;

    /** 亮度 */
    @Excel(name = "亮度")
    private String light;

    /** 具体执行时间（时分秒） */
    @Excel(name = "具体执行时间", readConverterExp = "时=分秒")
    private String operateTime;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCompanyId(Long companyId) 
    {
        this.companyId = companyId;
    }

    public Long getCompanyId() 
    {
        return companyId;
    }
    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setTacticsId(Long tacticsId) 
    {
        this.tacticsId = tacticsId;
    }

    public Long getTacticsId() 
    {
        return tacticsId;
    }

    public void setOperate(String operate) 
    {
        this.operate = operate;
    }

    public String getOperate() 
    {
        return operate;
    }
    public void setLight(String light) 
    {
        this.light = light;
    }

    public String getLight() 
    {
        return light;
    }
    public void setOperateTime(String operateTime) 
    {
        this.operateTime = operateTime;
    }

    public String getOperateTime() 
    {
        return operateTime;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("companyId", getCompanyId())
            .append("projectId", getProjectId())
            .append("tacticsId", getTacticsId())
            .append("operate", getOperate())
            .append("light", getLight())
            .append("operateTime", getOperateTime())
            .toString();
    }
}
