package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.constant.SecurityConstants;
import com.shujuwuliankeji.ems.cloud.common.core.constant.UserConstants;
import com.shujuwuliankeji.ems.cloud.common.core.domain.R;
import com.shujuwuliankeji.ems.cloud.common.core.utils.StringUtils;
import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyDept;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyUser;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyDeptService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyUserService;
import com.shujuwuliankeji.ems.cloud.system.api.RemoteRoleService;
import com.shujuwuliankeji.ems.cloud.system.api.domain.SysRole;

/**
 * 用户信息Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-07
 */
@RestController
@RequestMapping("/company/user")
public class BussCompanyUserController extends BaseController {
	@Autowired
	private IBussCompanyUserService bussCompanyUserService;

	@Autowired
	private IBussCompanyDeptService bussCompanyDeptService;

	@Autowired
	private RemoteRoleService remoteRoleService;

	/**
	 * 查询用户信息列表
	 */
	@RequiresPermissions("bussiness:company:user:list")
	@GetMapping("/page")
	@SaaS
	public TableDataInfo list(BussCompanyUser bussCompanyUser) {
		startPage();
		List<BussCompanyUser> list = bussCompanyUserService.selectBussCompanyUserList(bussCompanyUser);
		return getDataTable(list);
	}

	/**
	 * 导出用户信息列表
	 */
	@RequiresPermissions("bussiness:company:user:export")
	@Log(title = "用户信息", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussCompanyUser bussCompanyUser) {
		List<BussCompanyUser> list = bussCompanyUserService.selectBussCompanyUserList(bussCompanyUser);
		ExcelUtil<BussCompanyUser> util = new ExcelUtil<BussCompanyUser>(BussCompanyUser.class);
		util.exportExcel(response, list, "用户信息数据");
	}

	/**
	 * 获取用户信息详细信息
	 */
	@RequiresPermissions("bussiness:company:user:query")
	@GetMapping(value = "/{userId}")
	public AjaxResult getInfo(@PathVariable("userId") Long userId) {
		BussCompanyUser user = new BussCompanyUser();
		user.setUserId(userId);
		return success(bussCompanyUserService.selectBussCompanyUserOne(user));
	}

	/**
	 * 新增用户信息
	 */
	@RequiresPermissions("bussiness:company:user:add")
	@Log(title = "用户信息", businessType = BusinessType.INSERT)
	@PostMapping
	@SaaS
	public AjaxResult add(@RequestBody BussCompanyUser bussCompanyUser) {
		if (UserConstants.NOT_UNIQUE.equals(bussCompanyUserService.checkUserNameUnique(bussCompanyUser))) {
			return error("新增用户'" + bussCompanyUser.getUserName() + "'失败，登录账号已被占用");
		} else if (StringUtils.isNotEmpty(bussCompanyUser.getPhonenumber())
				&& UserConstants.NOT_UNIQUE.equals(bussCompanyUserService.checkPhoneUnique(bussCompanyUser))) {
			return error("新增用户'" + bussCompanyUser.getUserName() + "'失败，手机号码已被占用");
		} else if (StringUtils.isNotEmpty(bussCompanyUser.getEmail())
				&& UserConstants.NOT_UNIQUE.equals(bussCompanyUserService.checkEmailUnique(bussCompanyUser))) {
			return error("新增用户'" + bussCompanyUser.getUserName() + "'失败，邮箱账号已被占用");
		}
		bussCompanyUser.setCreateBy(SecurityUtils.getUsername());
		bussCompanyUser.setPassword(SecurityUtils.encryptPassword(bussCompanyUser.getPassword()));
		return toAjax(bussCompanyUserService.insertBussCompanyUser(bussCompanyUser));
	}

	/**
	 * 修改用户信息
	 */
	@RequiresPermissions("bussiness:company:user:edit")
	@Log(title = "用户信息", businessType = BusinessType.UPDATE)
	@PutMapping
	@SaaS
	public AjaxResult edit(@RequestBody BussCompanyUser bussCompanyUser) {
		return toAjax(bussCompanyUserService.updateBussCompanyUser(bussCompanyUser));
	}

	/**
	 * 删除用户信息
	 */
	@RequiresPermissions("bussiness:company:user:remove")
	@Log(title = "用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
	public AjaxResult remove(@PathVariable Long[] userIds) {
		return toAjax(bussCompanyUserService.deleteBussCompanyUserByUserIds(userIds));
	}

	/**
	 * 状态修改
	 */
	@RequiresPermissions("bussiness:company:user:edit")
	@Log(title = "企业用户管理", businessType = BusinessType.UPDATE)
	@PutMapping("/changeStatus")
	@SaaS
	public AjaxResult changeStatus(@RequestBody BussCompanyUser user) {
		user.setUpdateBy(SecurityUtils.getUsername());
		return toAjax(bussCompanyUserService.updateUserStatus(user));
	}

	/**
	 * 重置密码
	 */
	@RequiresPermissions("bussiness:company:user:edit")
	@Log(title = "企业用户管理", businessType = BusinessType.UPDATE)
	@PutMapping("/resetPwd")
	public AjaxResult resetPwd(@RequestBody BussCompanyUser user) {
		user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
		user.setUpdateBy(SecurityUtils.getUsername());
		return toAjax(bussCompanyUserService.resetPwd(user));
	}

	/**
	 * 获取部门树列表
	 */
	@RequiresPermissions("bussiness:company:user:list")
	@GetMapping("/deptTree")
	@SaaS
	public AjaxResult deptTree(BussCompanyDept dept) {
		return success(bussCompanyDeptService.selectDeptTreeList(dept));
	}

	/**
	 * 获取SAASrole
	 */
	@RequiresPermissions("bussiness:company:user:edit")
	@GetMapping("/role")
	public R<List<SysRole>> saasRole() {
		return remoteRoleService.getSaaSRole(SecurityConstants.INNER);
	}

}
