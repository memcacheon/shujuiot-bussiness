package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussTacticsDetailMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussTacticsDetail;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussTacticsDetailService;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;

/**
 * 策略详细Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-03-08
 */
@Service
public class BussTacticsDetailServiceImpl implements IBussTacticsDetailService 
{
    @Autowired
    private BussTacticsDetailMapper bussTacticsDetailMapper;

	/**
     * 查询策略详细
     * 
     * @param id 策略详细主键
     * @return 策略详细
     */
    @Override
    @SaaS
    public BussTacticsDetail selectBussTacticsDetailById(BussTacticsDetail bussTacticsDetail)
    {
        return bussTacticsDetailMapper.selectBussTacticsDetailById(bussTacticsDetail);
    }

    /**
     * 查询策略详细列表
     * 
     * @param bussTacticsDetail 策略详细
     * @return 策略详细
     */
    @Override
    @SaaS
    public List<BussTacticsDetail> selectBussTacticsDetailList(BussTacticsDetail bussTacticsDetail)
    {
        return bussTacticsDetailMapper.selectBussTacticsDetailList(bussTacticsDetail);
    }

    /**
     * 新增策略详细
     * 
     * @param bussTacticsDetail 策略详细
     * @return 结果
     */
    @Override
    @SaaS
    public int insertBussTacticsDetail(BussTacticsDetail bussTacticsDetail)
    {
        bussTacticsDetail.setCreateTime(DateUtils.getNowDate());
        return bussTacticsDetailMapper.insertBussTacticsDetail(bussTacticsDetail);
    }

    /**
     * 修改策略详细
     * 
     * @param bussTacticsDetail 策略详细
     * @return 结果
     */
	@SaaS
    @Override
    public int updateBussTacticsDetail(BussTacticsDetail bussTacticsDetail)
    {
        bussTacticsDetail.setUpdateTime(DateUtils.getNowDate());
        return bussTacticsDetailMapper.updateBussTacticsDetail(bussTacticsDetail);
    }

    /**
     * 批量删除策略详细
     * 
     * @param ids 需要删除的策略详细主键
     * @return 结果
     */
	@SaaS
    @Override
    public int deleteBussTacticsDetailByIds(BussTacticsDetail bussTacticsDetail)
    {
        return bussTacticsDetailMapper.deleteBussTacticsDetailByIds(bussTacticsDetail);
    }

    /**
     * 删除策略详细信息
     * 
     * @param id 策略详细主键
     * @return 结果
     */
    @Override
    @SaaS
    public int deleteBussTacticsDetailById(BussTacticsDetail bussTacticsDetail)
    {
        return bussTacticsDetailMapper.deleteBussTacticsDetailById(bussTacticsDetail);
    }


    
}
