package com.shujuwuliankeji.ems.cloud.module.bussiness.iot.extend;

import com.github.cm.heclouds.onenet.studio.api.entity.application.project.QueryDeviceListRequest;

/**
 * 扩展参数grou_id
 * 
 * @author Fanzhongjie
 * @date 2023年2月1日 下午5:22:39
 * @tips TODO
 */
public class QueryProjectDeviceListRequest extends QueryDeviceListRequest {

	public void setGroupId(String groupId) {
		queryParam("group_id", groupId);
	}

}
