package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterWarning;

/**
 * 报警信息Service接口
 * 
 * @author fanzhongjie
 * @date 2023-02-10
 */
public interface IBussDeviceWatermeterWarningService {

	/**
	 * 查询报警信息
	 * 
	 * @param id 报警信息主键
	 * @return 报警信息
	 */
	public BussDeviceWatermeterWarning selectBussDeviceWatermeterWarningById(
			BussDeviceWatermeterWarning bussDeviceWatermeterWarning);

	/**
	 * 查询报警信息列表
	 * 
	 * @param bussDeviceWatermeterWarning 报警信息
	 * @return 报警信息集合
	 */
	public List<BussDeviceWatermeterWarning> selectBussDeviceWatermeterWarningList(
			BussDeviceWatermeterWarning bussDeviceWatermeterWarning);

	/**
	 * 新增报警信息
	 * 
	 * @param bussDeviceWatermeterWarning 报警信息
	 * @return 结果
	 */
	public int insertBussDeviceWatermeterWarning(BussDeviceWatermeterWarning bussDeviceWatermeterWarning);

	/**
	 * 修改报警信息
	 * 
	 * @param bussDeviceWatermeterWarning 报警信息
	 * @return 结果
	 */
	public int updateBussDeviceWatermeterWarning(BussDeviceWatermeterWarning bussDeviceWatermeterWarning);

	/**
	 * 批量删除报警信息
	 * 
	 * @param ids 需要删除的报警信息主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterWarningByIds(BussDeviceWatermeterWarning bussDeviceWatermeterWarning);

	/**
	 * 删除报警信息信息
	 * 
	 * @param id 报警信息主键
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterWarningById(BussDeviceWatermeterWarning bussDeviceWatermeterWarning);

}
