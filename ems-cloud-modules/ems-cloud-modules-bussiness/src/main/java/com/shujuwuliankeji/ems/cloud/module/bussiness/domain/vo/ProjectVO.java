package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月12日 下午1:21:50
 * @tips TODO
 */
public class ProjectVO {

	private Long id;

	private String projectName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

}
