package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.OneReceiveMsgMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.OneReceiveMsg;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IOneReceiveMsgService;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;

/**
 * 中台设备数据Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
@Service
public class OneReceiveMsgServiceImpl implements IOneReceiveMsgService 
{
    @Autowired
    private OneReceiveMsgMapper oneReceiveMsgMapper;

	/**
     * 查询中台设备数据
     * 
     * @param id 中台设备数据主键
     * @return 中台设备数据
     */
    @Override
    //@SaaS
    public OneReceiveMsg selectOneReceiveMsgById(OneReceiveMsg oneReceiveMsg)
    {
        return oneReceiveMsgMapper.selectOneReceiveMsgById(oneReceiveMsg);
    }

    /**
     * 查询中台设备数据列表
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 中台设备数据
     */
    @Override
    @SaaS
    public List<OneReceiveMsg> selectOneReceiveMsgList(OneReceiveMsg oneReceiveMsg)
    {
        return oneReceiveMsgMapper.selectOneReceiveMsgList(oneReceiveMsg);
    }

    /**
     * 新增中台设备数据
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 结果
     */
    @Override
    @SaaS
    public int insertOneReceiveMsg(OneReceiveMsg oneReceiveMsg)
    {
        oneReceiveMsg.setCreateTime(DateUtils.getNowDate());
        return oneReceiveMsgMapper.insertOneReceiveMsg(oneReceiveMsg);
    }

    /**
     * 修改中台设备数据
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 结果
     */
	@SaaS
    @Override
    public int updateOneReceiveMsg(OneReceiveMsg oneReceiveMsg)
    {
        oneReceiveMsg.setUpdateTime(DateUtils.getNowDate());
        return oneReceiveMsgMapper.updateOneReceiveMsg(oneReceiveMsg);
    }

    /**
     * 批量删除中台设备数据
     * 
     * @param ids 需要删除的中台设备数据主键
     * @return 结果
     */
	@SaaS
    @Override
    public int deleteOneReceiveMsgByIds(OneReceiveMsg oneReceiveMsg)
    {
        return oneReceiveMsgMapper.deleteOneReceiveMsgByIds(oneReceiveMsg);
    }

    /**
     * 删除中台设备数据信息
     * 
     * @param id 中台设备数据主键
     * @return 结果
     */
    @Override
    @SaaS
    public int deleteOneReceiveMsgById(OneReceiveMsg oneReceiveMsg)
    {
        return oneReceiveMsgMapper.deleteOneReceiveMsgById(oneReceiveMsg);
    }


    
}
