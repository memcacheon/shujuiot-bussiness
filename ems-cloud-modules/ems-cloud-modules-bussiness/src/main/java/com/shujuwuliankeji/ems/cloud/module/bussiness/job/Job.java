package com.shujuwuliankeji.ems.cloud.module.bussiness.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月30日 上午9:43:27
 * @tips 定时任务同步设备信息
 */
@Component
public class Job {

	/**
	 * 每5分钟执行一次
	 */
	@Scheduled(cron = "* 0/5 * * * ?")
	private void postMagneticState() {
	}

}
