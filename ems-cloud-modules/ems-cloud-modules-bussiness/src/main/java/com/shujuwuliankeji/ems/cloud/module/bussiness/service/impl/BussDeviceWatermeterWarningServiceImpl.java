package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterWarning;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceWatermeterWarningMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceWatermeterWarningService;

/**
 * 报警信息Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-02-10
 */
@Service
public class BussDeviceWatermeterWarningServiceImpl implements IBussDeviceWatermeterWarningService {
	@Autowired
	private BussDeviceWatermeterWarningMapper bussDeviceWatermeterWarningMapper;

	/**
	 * 查询报警信息
	 * 
	 * @param id 报警信息主键
	 * @return 报警信息
	 */
	@Override
	@SaaS
	public BussDeviceWatermeterWarning selectBussDeviceWatermeterWarningById(
			BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		return bussDeviceWatermeterWarningMapper.selectBussDeviceWatermeterWarningById(bussDeviceWatermeterWarning);
	}

	/**
	 * 查询报警信息列表
	 * 
	 * @param bussDeviceWatermeterWarning 报警信息
	 * @return 报警信息
	 */
	@Override
	@SaaS
	public List<BussDeviceWatermeterWarning> selectBussDeviceWatermeterWarningList(
			BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		return bussDeviceWatermeterWarningMapper.selectBussDeviceWatermeterWarningList(bussDeviceWatermeterWarning);
	}

	/**
	 * 新增报警信息
	 * 
	 * @param bussDeviceWatermeterWarning 报警信息
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussDeviceWatermeterWarning(BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		bussDeviceWatermeterWarning.setCreateTime(DateUtils.getNowDate());
		return bussDeviceWatermeterWarningMapper.insertBussDeviceWatermeterWarning(bussDeviceWatermeterWarning);
	}

	/**
	 * 修改报警信息
	 * 
	 * @param bussDeviceWatermeterWarning 报警信息
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussDeviceWatermeterWarning(BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		bussDeviceWatermeterWarning.setUpdateTime(DateUtils.getNowDate());
		return bussDeviceWatermeterWarningMapper.updateBussDeviceWatermeterWarning(bussDeviceWatermeterWarning);
	}

	/**
	 * 批量删除报警信息
	 * 
	 * @param ids 需要删除的报警信息主键
	 * @return 结果
	 */
	@SaaS
	@Override
	public int deleteBussDeviceWatermeterWarningByIds(BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		return bussDeviceWatermeterWarningMapper.deleteBussDeviceWatermeterWarningByIds(bussDeviceWatermeterWarning);
	}

	/**
	 * 删除报警信息信息
	 * 
	 * @param id 报警信息主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussDeviceWatermeterWarningById(BussDeviceWatermeterWarning bussDeviceWatermeterWarning) {
		return bussDeviceWatermeterWarningMapper.deleteBussDeviceWatermeterWarningById(bussDeviceWatermeterWarning);
	}

}
