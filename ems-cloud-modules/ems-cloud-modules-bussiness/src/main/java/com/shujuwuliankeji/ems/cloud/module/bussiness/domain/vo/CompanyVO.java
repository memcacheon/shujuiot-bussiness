package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月6日 上午10:34:30
 * @tips TODO
 */
public class CompanyVO {

	private Long id;

	private String companyName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

}
