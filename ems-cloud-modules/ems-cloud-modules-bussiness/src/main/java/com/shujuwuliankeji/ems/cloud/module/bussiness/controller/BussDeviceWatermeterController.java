package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceWatermeterService;

/**
 * 水表管理Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-30
 */
@RestController
@RequestMapping("/device/watermeter")
public class BussDeviceWatermeterController extends BaseController {
	@Autowired
	private IBussDeviceWatermeterService bussDeviceWatermeterService;

	/**
	 * 查询水表管理列表
	 */
	@RequiresPermissions("bussiness:deviceWatermeter:list")
	@GetMapping("/list")
	public TableDataInfo list(BussDeviceWatermeter bussDeviceWatermeter) {
		startPage();
		List<BussDeviceWatermeter> list = bussDeviceWatermeterService
				.selectBussDeviceWatermeterList(bussDeviceWatermeter);
		return getDataTable(list);
	}

	/**
	 * 导出水表管理列表
	 */
	@RequiresPermissions("bussiness:deviceWatermeter:export")
	@Log(title = "水表管理", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussDeviceWatermeter bussDeviceWatermeter) {
		List<BussDeviceWatermeter> list = bussDeviceWatermeterService
				.selectBussDeviceWatermeterList(bussDeviceWatermeter);
		ExcelUtil<BussDeviceWatermeter> util = new ExcelUtil<BussDeviceWatermeter>(BussDeviceWatermeter.class);
		util.exportExcel(response, list, "水表管理数据");
	}

	/**
	 * 获取水表管理详细信息
	 */
	@RequiresPermissions("bussiness:deviceWatermeter:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussDeviceWatermeter bussDeviceWatermeter = new BussDeviceWatermeter();
		bussDeviceWatermeter.setId(id);
		return success(bussDeviceWatermeterService.selectBussDeviceWatermeterById(bussDeviceWatermeter));
	}

	/**
	 * 新增水表管理
	 */
	@RequiresPermissions("bussiness:deviceWatermeter:add")
	@Log(title = "水表管理", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussDeviceWatermeter bussDeviceWatermeter) {
		return toAjax(bussDeviceWatermeterService.insertBussDeviceWatermeter(bussDeviceWatermeter));
	}

	/**
	 * 修改水表管理
	 */
	@RequiresPermissions("bussiness:deviceWatermeter:edit")
	@Log(title = "水表管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussDeviceWatermeter bussDeviceWatermeter) {
		return toAjax(bussDeviceWatermeterService.updateBussDeviceWatermeter(bussDeviceWatermeter));
	}

	/**
	 * 删除水表管理
	 */
	@RequiresPermissions("bussiness:deviceWatermeter:remove")
	@Log(title = "水表管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussDeviceWatermeter bussDeviceWatermeter = new BussDeviceWatermeter();
		bussDeviceWatermeter.setIds(ids);
		return toAjax(bussDeviceWatermeterService.deleteBussDeviceWatermeterByIds(bussDeviceWatermeter));
	}

	@RequiresPermissions("bussiness:deviceWatermeter:poll")
	@Log(title = "水表管理", businessType = BusinessType.OTHER)
	@GetMapping("poll")
	public AjaxResult pollData() {
		BussDevice bussDevice = new BussDevice();
		bussDeviceWatermeterService.pollData(bussDevice);
		return success();
	}

}
