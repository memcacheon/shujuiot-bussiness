package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.OneNetData;

/**
 * onenet http推送消息参数
 */
public class HttpParams {

    private String msg;
    private String signature;
    private Long time;
    private String id;
    private String nonce;

    public HttpParams() {
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    @Override
    public String toString() {
        return "HttpParams{" +
                "msg='" + msg + '\'' +
                ", signature='" + signature + '\'' +
                ", time=" + time +
                ", id='" + id + '\'' +
                ", nonce='" + nonce + '\'' +
                '}';
    }
}
