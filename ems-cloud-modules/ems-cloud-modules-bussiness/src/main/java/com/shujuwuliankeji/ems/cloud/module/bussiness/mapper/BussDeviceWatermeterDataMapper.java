package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterData;

/**
 * 水属性数据Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-02-04
 */
public interface BussDeviceWatermeterDataMapper {

	/**
	 * 查询水属性数据
	 * 
	 * @param id 水属性数据主键
	 * @return 水属性数据
	 */
	public BussDeviceWatermeterData selectBussDeviceAmmeterDataById(Long id);

	/**
	 * 查询水属性数据列表
	 * 
	 * @param bussDeviceAmmeterData 水属性数据
	 * @return 水属性数据集合
	 */
	public List<BussDeviceWatermeterData> selectBussDeviceWatermeterDataList(
			BussDeviceWatermeterData bussDeviceAmmeterData);

	/**
	 * 根据日期查询设备数据
	 * 
	 * @param companyId
	 * @param projectId
	 * @param format
	 * @return
	 */
	public Double selectDataByDate(@Param("companyId") Long companyId, @Param("projectId") Long projectId,
			@Param("dateStr") String format);

	/**
	 * 查询设备数据
	 * 
	 * @param bussDevice
	 * @return
	 */
	public BussDeviceWatermeterData lastDevieData(BussDeviceWatermeter bussDevice);

	/**
	 * 查询一天的设备数据
	 * 
	 * @param bussDevice
	 * @return
	 */
	public List<BussDeviceWatermeterData> dataStatistic(BussDeviceWatermeter bussDevice);

}
