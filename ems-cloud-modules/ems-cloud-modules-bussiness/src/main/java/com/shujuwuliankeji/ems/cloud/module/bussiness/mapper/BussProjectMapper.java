package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;

/**
 * 项目Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
public interface BussProjectMapper {
	/**
	 * 查询项目
	 * 
	 * @param id 项目主键
	 * @return 项目
	 */
	public BussProject selectBussProjectById(BussProject bussProject);

	/**
	 * 查询项目列表
	 * 
	 * @param bussProject 项目
	 * @return 项目集合
	 */
	public List<BussProject> selectBussProjectList(BussProject bussProject);

	/**
	 * 新增项目
	 * 
	 * @param bussProject 项目
	 * @return 结果
	 */
	public int insertBussProject(BussProject bussProject);

	/**
	 * 修改项目
	 * 
	 * @param bussProject 项目
	 * @return 结果
	 */
	public int updateBussProject(BussProject bussProject);

	/**
	 * 删除项目
	 * 
	 * @param id 项目主键
	 * @return 结果
	 */
	public int deleteBussProjectById(BussProject bussProject);

	/**
	 * 批量删除项目
	 * 
	 * @param ids 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussProjectByIds(@Param("entity") BussProject bussProject);

	/**
	 * 监测企业下项目名称是否重复
	 * 
	 * @param bussProject
	 * @return
	 */
	public int checkProjectNameUnique(BussProject bussProject);

	/**
	 * 根据名称查询project
	 * 
	 * @param bussProject
	 * @return
	 */
	public BussProject selectBussProjectByName(BussProject bussProject);

	/**
	 * 根据企业ID查询项目
	 * 
	 * @param companyId
	 * @return
	 */
	public List<BussProject> selectBussProjectByCompanyId(@Param("companyId") Long companyId);

	/**
	 * 查询所有字段
	 * 
	 * @param companyId
	 * @return
	 */
	public List<BussProject> selectBussProjectAllByCompanyId(@Param("companyId") Long companyId);
}
