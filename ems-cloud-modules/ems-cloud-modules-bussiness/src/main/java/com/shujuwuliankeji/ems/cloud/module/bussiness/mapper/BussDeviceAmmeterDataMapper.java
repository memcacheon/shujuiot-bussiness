package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterData;

/**
 * 电属性数据Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-02-04
 */
public interface BussDeviceAmmeterDataMapper {

	/**
	 * 查询电属性数据
	 * 
	 * @param id 电属性数据主键
	 * @return 电属性数据
	 */
	public BussDeviceAmmeterData selectBussDeviceAmmeterDataById(Long id);

	/**
	 * 查询电属性数据列表
	 * 
	 * @param bussDeviceAmmeterData 电属性数据
	 * @return 电属性数据集合
	 */
	public List<BussDeviceAmmeterData> selectBussDeviceAmmeterDataList(BussDeviceAmmeterData bussDeviceAmmeterData);

	/**
	 * 根据日期查询设备数据
	 * 
	 * @param companyId
	 * @param projectId
	 * @param format
	 * @return
	 */
	public Double selectDataByDate(@Param("companyId") Long companyId, @Param("projectId") Long projectId,
			@Param("dateStr") String format);

	/**
	 * 查询设备数据
	 * 
	 * @param bussDevice
	 * @return
	 */
	public BussDeviceAmmeterData lastDevieData(BussDeviceAmmeter bussDevice);

	/**
	 * 查询一天的设备数据
	 * 
	 * @param bussDevice
	 * @return
	 */
	public List<BussDeviceAmmeterData> dataStatistic(BussDeviceAmmeter bussDevice);

	/**
	 * 新增电属性数据
	 *
	 * @param bussDeviceAmmeterData 电属性数据
	 * @return 结果
	 */
	public int insertBussDeviceAmmeterData(BussDeviceAmmeterData bussDeviceAmmeterData);

}
