package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterWarning;

/**
 * 电报警Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
public interface BussDeviceAmmeterWarningMapper {

	/**
	 * 查询电报警
	 * 
	 * @param id 电报警主键
	 * @return 电报警
	 */
	public BussDeviceAmmeterWarning selectBussDeviceAmmeterWarningById(
			BussDeviceAmmeterWarning bussDeviceAmmeterWarning);

	/**
	 * 查询电报警列表
	 * 
	 * @param bussDeviceAmmeterWarning 电报警
	 * @return 电报警集合
	 */
	public List<BussDeviceAmmeterWarning> selectBussDeviceAmmeterWarningList(
			BussDeviceAmmeterWarning bussDeviceAmmeterWarning);

	/**
	 * 新增电报警
	 * 
	 * @param bussDeviceAmmeterWarning 电报警
	 * @return 结果
	 */
	public int insertBussDeviceAmmeterWarning(BussDeviceAmmeterWarning bussDeviceAmmeterWarning);

	/**
	 * 修改电报警
	 * 
	 * @param bussDeviceAmmeterWarning 电报警
	 * @return 结果
	 */
	public int updateBussDeviceAmmeterWarning(BussDeviceAmmeterWarning bussDeviceAmmeterWarning);

	/**
	 * 删除电报警
	 * 
	 * @param id 电报警主键
	 * @return 结果
	 */
	public int deleteBussDeviceAmmeterWarningById(BussDeviceAmmeterWarning bussDeviceAmmeterWarning);

	/**
	 * 批量删除电报警
	 * 
	 * @param ids 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceAmmeterWarningByIds(@Param("entity") BussDeviceAmmeterWarning bussDeviceAmmeterWarning);
}
