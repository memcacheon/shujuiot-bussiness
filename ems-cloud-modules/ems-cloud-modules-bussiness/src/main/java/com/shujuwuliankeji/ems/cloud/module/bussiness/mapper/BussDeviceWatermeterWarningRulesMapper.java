package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterWarningRules;

/**
 * 水报警规则Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-02-10
 */
public interface BussDeviceWatermeterWarningRulesMapper {

	/**
	 * 查询水报警规则
	 * 
	 * @param id 水报警规则主键
	 * @return 水报警规则
	 */
	public BussDeviceWatermeterWarningRules selectBussDeviceWatermeterWarningRulesById(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 查询水报警规则列表
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 水报警规则集合
	 */
	public List<BussDeviceWatermeterWarningRules> selectBussDeviceWatermeterWarningRulesList(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 新增水报警规则
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 结果
	 */
	public int insertBussDeviceWatermeterWarningRules(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 修改水报警规则
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 结果
	 */
	public int updateBussDeviceWatermeterWarningRules(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 删除水报警规则
	 * 
	 * @param id 水报警规则主键
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterWarningRulesById(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);

	/**
	 * 批量删除水报警规则
	 * 
	 * @param ids 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceWatermeterWarningRulesByIds(
			@Param("entity") BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules);
}
