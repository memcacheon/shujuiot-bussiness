package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyInfo;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyInfoService;

/**
 * 企业信息Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
@RestController
@RequestMapping("/company/info")
public class BussCompanyInfoController extends BaseController {
	@Autowired
	private IBussCompanyInfoService bussCompanyInfoService;

	/**
	 * 获取企业信息详细信息
	 */
	@RequiresPermissions("bussiness:companyInfo:query")
	@GetMapping
	public AjaxResult getInfo() {
		BussCompanyInfo bussCompanyInfo = new BussCompanyInfo();
		return success(bussCompanyInfoService.selectBussCompanyInfoById(bussCompanyInfo));
	}

	/**
	 * 修改企业信息
	 */
	@RequiresPermissions("bussiness:companyInfo:edit")
	@Log(title = "企业信息", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussCompanyInfo bussCompanyInfo) {
		return toAjax(bussCompanyInfoService.updateBussCompanyInfo(bussCompanyInfo));
	}

}
