package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterWarningRules;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceWatermeterWarningRulesMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceWatermeterWarningRulesService;

/**
 * 水报警规则Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-02-10
 */
@Service
public class BussDeviceWatermeterWarningRulesServiceImpl implements IBussDeviceWatermeterWarningRulesService {
	@Autowired
	private BussDeviceWatermeterWarningRulesMapper bussDeviceWatermeterWarningRulesMapper;

	/**
	 * 查询水报警规则
	 * 
	 * @param id 水报警规则主键
	 * @return 水报警规则
	 */
	@Override
	@SaaS
	public BussDeviceWatermeterWarningRules selectBussDeviceWatermeterWarningRulesById(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		return bussDeviceWatermeterWarningRulesMapper
				.selectBussDeviceWatermeterWarningRulesById(bussDeviceWatermeterWarningRules);
	}

	/**
	 * 查询水报警规则列表
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 水报警规则
	 */
	@Override
	@SaaS
	public List<BussDeviceWatermeterWarningRules> selectBussDeviceWatermeterWarningRulesList(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		return bussDeviceWatermeterWarningRulesMapper
				.selectBussDeviceWatermeterWarningRulesList(bussDeviceWatermeterWarningRules);
	}

	/**
	 * 新增水报警规则
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussDeviceWatermeterWarningRules(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		bussDeviceWatermeterWarningRules.setCreateTime(DateUtils.getNowDate());
		return bussDeviceWatermeterWarningRulesMapper
				.insertBussDeviceWatermeterWarningRules(bussDeviceWatermeterWarningRules);
	}

	/**
	 * 修改水报警规则
	 * 
	 * @param bussDeviceWatermeterWarningRules 水报警规则
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussDeviceWatermeterWarningRules(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		bussDeviceWatermeterWarningRules.setUpdateTime(DateUtils.getNowDate());
		return bussDeviceWatermeterWarningRulesMapper
				.updateBussDeviceWatermeterWarningRules(bussDeviceWatermeterWarningRules);
	}

	/**
	 * 批量删除水报警规则
	 * 
	 * @param ids 需要删除的水报警规则主键
	 * @return 结果
	 */
	@SaaS
	@Override
	public int deleteBussDeviceWatermeterWarningRulesByIds(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		return bussDeviceWatermeterWarningRulesMapper
				.deleteBussDeviceWatermeterWarningRulesByIds(bussDeviceWatermeterWarningRules);
	}

	/**
	 * 删除水报警规则信息
	 * 
	 * @param id 水报警规则主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussDeviceWatermeterWarningRulesById(
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		return bussDeviceWatermeterWarningRulesMapper
				.deleteBussDeviceWatermeterWarningRulesById(bussDeviceWatermeterWarningRules);
	}

}
