package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月5日 下午2:26:32
 * @tips TODO
 */
public class SysUserVO {

	private Long userId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	private String userName;
}
