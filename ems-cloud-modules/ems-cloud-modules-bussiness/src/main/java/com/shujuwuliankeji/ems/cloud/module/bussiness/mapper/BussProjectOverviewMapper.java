package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectOverview;

/**
 * 项目Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-05
 */
public interface BussProjectOverviewMapper {
	/**
	 * 查询项目
	 * 
	 * @param id 项目主键
	 * @return 项目
	 */
	public BussProjectOverview selectBussProjectOverviewById(Long id);

	/**
	 * 查询项目列表
	 * 
	 * @param bussProject 项目
	 * @return 项目集合
	 */
	public List<BussProjectOverview> selectBussProjectOverviewList(BussProjectOverview bussProject);

}
