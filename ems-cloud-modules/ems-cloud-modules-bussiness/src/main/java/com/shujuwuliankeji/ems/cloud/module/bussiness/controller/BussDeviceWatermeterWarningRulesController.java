package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterWarningRules;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceWatermeterWarningRulesService;

/**
 * 水报警规则Controller
 * 
 * @author fanzhongjie
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/waterAlarmRule")
public class BussDeviceWatermeterWarningRulesController extends BaseController {
	@Autowired
	private IBussDeviceWatermeterWarningRulesService bussDeviceWatermeterWarningRulesService;

	/**
	 * 查询水报警规则列表
	 */
	@RequiresPermissions("bussiness:waterAlarmRule:list")
	@GetMapping("/list")
	public TableDataInfo list(BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		startPage();
		List<BussDeviceWatermeterWarningRules> list = bussDeviceWatermeterWarningRulesService
				.selectBussDeviceWatermeterWarningRulesList(bussDeviceWatermeterWarningRules);
		return getDataTable(list);
	}

	/**
	 * 导出水报警规则列表
	 */
	@RequiresPermissions("bussiness:waterAlarmRule:export")
	@Log(title = "水报警规则", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response,
			BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		List<BussDeviceWatermeterWarningRules> list = bussDeviceWatermeterWarningRulesService
				.selectBussDeviceWatermeterWarningRulesList(bussDeviceWatermeterWarningRules);
		ExcelUtil<BussDeviceWatermeterWarningRules> util = new ExcelUtil<BussDeviceWatermeterWarningRules>(
				BussDeviceWatermeterWarningRules.class);
		util.exportExcel(response, list, "水报警规则数据");
	}

	/**
	 * 获取水报警规则详细信息
	 */
	@RequiresPermissions("bussiness:waterAlarmRule:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules = new BussDeviceWatermeterWarningRules();
		bussDeviceWatermeterWarningRules.setId(id);
		return success(bussDeviceWatermeterWarningRulesService
				.selectBussDeviceWatermeterWarningRulesById(bussDeviceWatermeterWarningRules));
	}

	/**
	 * 新增水报警规则
	 */
	@RequiresPermissions("bussiness:waterAlarmRule:add")
	@Log(title = "水报警规则", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		return toAjax(bussDeviceWatermeterWarningRulesService
				.insertBussDeviceWatermeterWarningRules(bussDeviceWatermeterWarningRules));
	}

	/**
	 * 修改水报警规则
	 */
	@RequiresPermissions("bussiness:waterAlarmRule:edit")
	@Log(title = "水报警规则", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules) {
		return toAjax(bussDeviceWatermeterWarningRulesService
				.updateBussDeviceWatermeterWarningRules(bussDeviceWatermeterWarningRules));
	}

	/**
	 * 删除水报警规则
	 */
	@RequiresPermissions("bussiness:waterAlarmRule:remove")
	@Log(title = "水报警规则", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussDeviceWatermeterWarningRules bussDeviceWatermeterWarningRules = new BussDeviceWatermeterWarningRules();
		bussDeviceWatermeterWarningRules.setIds(ids);
		return toAjax(bussDeviceWatermeterWarningRulesService
				.deleteBussDeviceWatermeterWarningRulesByIds(bussDeviceWatermeterWarningRules));
	}

}
