package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 设备管理对象 buss_device
 * 
 * @author fanzhongjie
 * @date 2023-01-28
 */
public class BussDeviceAmmeter extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 企业ID */
	@Excel(name = "企业ID")
	private Long companyId;

	/** 项目ID */
	@Excel(name = "项目ID")
	private Long projectId;

	/** 中台项目ID */
	@Excel(name = "中台项目ID")
	private String iotProjectId;

	/** iot_设备名称 */
	@Excel(name = "iot_设备名称")
	private String deviceName;

	/** iot_产品ID */
	@Excel(name = "iot_产品ID")
	private String productId;

	/** iot_产品名称 */
	@Excel(name = "iot_产品名称")
	private String productName;

	/** iot_设备描述 */
	@Excel(name = "iot_设备描述")
	private String desc;

	/** iot_设备状态 1-未激活 2-在线 3-离线 */
	@Excel(name = "iot_设备状态 1-未激活 2-在线 3-离线")
	private Integer status;

	/** iot_节点类型 1-直连设备 */
	@Excel(name = "iot_节点类型 1-直连设备")
	private Integer nodeType;

	/** iot_协议类型 1-泛协议 2-MQTT 3-CoAP 4-LwM2M */
	@Excel(name = "iot_协议类型 1-泛协议 2-MQTT 3-CoAP 4-LwM2M")
	private Integer protocol;

	/** iot_NB设备imei，15个数字组成的电子串号 */
	@Excel(name = "iot_NB设备imei，15个数字组成的电子串号")
	private String imei;

	/** iot_NB设备imsi，不超过15个的数字 */
	@Excel(name = "iot_NB设备imsi，不超过15个的数字")
	private String imsi;

	/** iot_最后一次在线时间 */
	@Excel(name = "iot_最后一次在线时间")
	private String lastTime;

	/** iot_激活时间 */
	@Excel(name = "iot_激活时间")
	private String activeTime;

	/** project_node祖级列表 */
	@Excel(name = "project_node祖级列表")
	private String ancestors;

	/**
	 * 搜索使用字符串日期 yyyy-MM-dd
	 */
	private String date;

	/**
	 * 查询字段
	 */
	private String field;

	/**
	 * 节点ID
	 */
	private Long nodeId;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setIotProjectId(String iotProjectId) {
		this.iotProjectId = iotProjectId;
	}

	public String getIotProjectId() {
		return iotProjectId;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setProtocol(Integer protocol) {
		this.protocol = protocol;
	}

	public Integer getProtocol() {
		return protocol;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getImei() {
		return imei;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getImsi() {
		return imsi;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setActiveTime(String activeTime) {
		this.activeTime = activeTime;
	}

	public String getActiveTime() {
		return activeTime;
	}

	public void setAncestors(String ancestors) {
		this.ancestors = ancestors;
	}

	public String getAncestors() {
		return ancestors;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("companyId", getCompanyId()).append("projectId", getProjectId())
				.append("iotProjectId", getIotProjectId()).append("deviceName", getDeviceName())
				.append("productId", getProductId()).append("productName", getProductName()).append("desc", getDesc())
				.append("status", getStatus()).append("nodeType", getNodeType()).append("protocol", getProtocol())
				.append("imei", getImei()).append("imsi", getImsi()).append("lastTime", getLastTime())
				.append("activeTime", getActiveTime()).append("ancestors", getAncestors()).toString();
	}

	public Long getNodeId() {
		return nodeId;
	}

	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
}
