package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.OneReceiveMsg;

/**
 * 中台设备数据Service接口
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
public interface IOneReceiveMsgService 
{

	/**
     * 查询中台设备数据
     * 
     * @param id 中台设备数据主键
     * @return 中台设备数据
     */
    public OneReceiveMsg selectOneReceiveMsgById(OneReceiveMsg oneReceiveMsg);
    

    /**
     * 查询中台设备数据列表
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 中台设备数据集合
     */
    public List<OneReceiveMsg> selectOneReceiveMsgList(OneReceiveMsg oneReceiveMsg);

    /**
     * 新增中台设备数据
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 结果
     */
    public int insertOneReceiveMsg(OneReceiveMsg oneReceiveMsg);

    /**
     * 修改中台设备数据
     * 
     * @param oneReceiveMsg 中台设备数据
     * @return 结果
     */
    public int updateOneReceiveMsg(OneReceiveMsg oneReceiveMsg);

 	/**
     * 批量删除中台设备数据
     * 
     * @param ids 需要删除的中台设备数据主键集合
     * @return 结果
     */
    public int deleteOneReceiveMsgByIds(OneReceiveMsg oneReceiveMsg);

    /**
     * 删除中台设备数据信息
     * 
     * @param id 中台设备数据主键
     * @return 结果
     */
    public int deleteOneReceiveMsgById(OneReceiveMsg oneReceiveMsg);
   
}
