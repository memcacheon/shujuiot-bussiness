package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.constant.DeviceTypeEnum;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.ProjectDeviceSummaryVO;

/**
 * 设备管理Service接口
 * 
 * @author fanzhongjie
 * @date 2023-01-30
 */
public interface IBussDeviceService {

	/**
	 * 查询设备管理
	 * 
	 * @param id 设备管理主键
	 * @return 设备管理
	 */
	public BussDevice selectBussDeviceById(BussDevice bussDevice);

	/**
	 * 查询设备管理列表
	 * 
	 * @param bussDevice 设备管理
	 * @return 设备管理集合
	 */
	public List<BussDevice> selectBussDeviceList(BussDevice bussDevice);

	/**
	 * 新增设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int insertBussDevice(BussDevice bussDevice);

	/**
	 * 修改设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int updateBussDevice(BussDevice bussDevice);

	/**
	 * 批量删除设备管理
	 * 
	 * @param ids 需要删除的设备管理主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceByIds(BussDevice bussDevice);

	/**
	 * 删除设备管理信息
	 * 
	 * @param id 设备管理主键
	 * @return 结果
	 */
	public int deleteBussDeviceById(BussDevice bussDevice);

	/**
	 * 获取已分配拓扑的设备
	 * 
	 * @param bussDevice
	 * @return
	 */
	public List<BussDevice> selectBussTopologyDeviceList(BussDevice bussDevice);

	/**
	 * 获取暂未分配的设备
	 * 
	 * @param bussDevice
	 * @return
	 */
	public List<BussDevice> selectBussNonTopologyDeviceList(BussDevice bussDevice);

	/**
	 * 往设备拓扑中添加设备
	 * 
	 * @param bussDevice
	 * @return
	 */
	public int addDeviceToTopology(BussDevice bussDevice);

	/**
	 * 指定设备ID移除拓扑
	 * 
	 * @param device
	 * @return
	 */
	public int removeTopology(BussDevice device);

	/**
	 * 检测是否存在该设备
	 * 
	 * @param checkDevice
	 * @return true存在 false不存在
	 */
	public boolean checkDeviceExist(BussDevice checkDevice);

	/**
	 * 批量插入
	 * 
	 * @param list
	 */
	public int insertBussDeviceBatch(List<BussDevice> list);

	/**
	 * 统一拉取设备
	 * 
	 * @param companyId
	 * @param ammeter
	 */
	public void poll(Long companyId, DeviceTypeEnum ammeter);

	/**
	 * 查询项目概况
	 * 
	 * @param device
	 * @return
	 */
	public ProjectDeviceSummaryVO projectSummary(BussDevice device);

}
