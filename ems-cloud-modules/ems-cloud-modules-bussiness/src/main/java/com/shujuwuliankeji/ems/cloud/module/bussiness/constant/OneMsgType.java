package com.shujuwuliankeji.ems.cloud.module.bussiness.constant;

public class OneMsgType {
    //消息源数据类型

    // 物模型数据（设备属性事件上报）
    public static final String NOTIFY = "notify";
    // 设备生命周期（在线、离线）
    public static final String LIFE_CYCLE = "lifeCycle";
    //场景联动触发日志
    public static final String SCENELOG = "sceneLog";

    //规则触发条件
    //大于等于 greater than or equal
    public static final String GREATER_THAN_OR_EQUAL="&gt;=";
    //小于等于 less than or equal
    public static final String LESS_THAN_OR_EQUAL="&lt;=";
    //小于
    public static final String LESS_THAN="&lt;";
    //大于
    public static final String GREATER_THAN="&gt;";
    //等于
    public static final String EQUAL="=";





}
