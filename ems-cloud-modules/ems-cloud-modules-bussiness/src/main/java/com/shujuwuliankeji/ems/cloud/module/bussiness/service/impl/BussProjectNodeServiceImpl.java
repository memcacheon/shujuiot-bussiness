package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.common.core.utils.StringUtils;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectNode;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.NodeTreeSelect;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussProjectNodeMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectNodeService;

/**
 * 项目节点Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-12
 */
@Service
public class BussProjectNodeServiceImpl implements IBussProjectNodeService {
	@Autowired
	private BussProjectNodeMapper bussProjectNodeMapper;

	/**
	 * 查询项目节点
	 * 
	 * @param id 项目节点主键
	 * @return 项目节点
	 */
	@Override
	@SaaS
	public BussProjectNode selectBussProjectNodeById(BussProjectNode bussProjectNode) {
		return bussProjectNodeMapper.selectBussProjectNodeById(bussProjectNode);
	}

	/**
	 * 查询项目节点列表
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 项目节点
	 */
	@Override
	@SaaS
	public List<BussProjectNode> selectBussProjectNodeList(BussProjectNode bussProjectNode) {
		return bussProjectNodeMapper.selectBussProjectNodeList(bussProjectNode);
	}

	/**
	 * 新增项目节点
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussProjectNode(BussProjectNode bussProjectNode) {

		BussProjectNode temp = new BussProjectNode();
		temp.setId(bussProjectNode.getParentId());
		temp.setCompanyId(bussProjectNode.getCompanyId());
		BussProjectNode parent = bussProjectNodeMapper.selectBussProjectNodeById(temp);

		if (parent != null) {
			bussProjectNode.setAncestors(parent.getAncestors() + "," + bussProjectNode.getParentId());
		} else {
			bussProjectNode.setAncestors("0");
		}

		bussProjectNode.setCreateBy(SecurityUtils.getUsername());
		bussProjectNode.setCreateTime(DateUtils.getNowDate());

		return bussProjectNodeMapper.insertBussProjectNode(bussProjectNode);
	}

	/**
	 * 修改项目节点
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 结果
	 */
	@SaaS
	@Override
	public int updateBussProjectNode(BussProjectNode bussProjectNode) {

		bussProjectNode.setUpdateTime(DateUtils.getNowDate());
		bussProjectNode.setUpdateBy(SecurityUtils.getUsername());

		return bussProjectNodeMapper.updateBussProjectNode(bussProjectNode);
	}

	/**
	 * 批量删除项目节点
	 * 
	 * @param ids 需要删除的项目节点主键
	 * @return 结果
	 */
	@SaaS
	@Override
	public int deleteBussProjectNodeByIds(BussProjectNode bussProjectNode) {
		// FOR:TODO 检查是否可以删除：节点下有设备数据不可以删除。
		int res = bussProjectNodeMapper.deleteBussProjectNodeByIds(bussProjectNode);
		return res;
	}

	/**
	 * 删除项目节点信息
	 * 
	 * @param id 项目节点主键
	 * @return 结果
	 */
	@Override
	@SaaS
	public int deleteBussProjectNodeById(BussProjectNode bussProjectNode) {
		return bussProjectNodeMapper.deleteBussProjectNodeById(bussProjectNode);
	}

	@Override
	@SaaS
	public List<NodeTreeSelect> selectNodeTreeList(BussProjectNode bussProjectNode) {
		List<BussProjectNode> nodeList = bussProjectNodeMapper.selectBussProjectNodeList(bussProjectNode);
		return buildNodeTreeSelect(nodeList);
	}

	private List<NodeTreeSelect> buildNodeTreeSelect(List<BussProjectNode> nodeList) {
		List<BussProjectNode> nodeTrees = buildNodeTree(nodeList);
		return nodeTrees.stream().map(NodeTreeSelect::new).collect(Collectors.toList());
	}

	private List<BussProjectNode> buildNodeTree(List<BussProjectNode> nodeList) {
		List<BussProjectNode> returnList = new ArrayList<BussProjectNode>();
		List<Long> tempList = nodeList.stream().map(BussProjectNode::getId).collect(Collectors.toList());
		for (BussProjectNode node : nodeList) {
			// 如果是顶级节点, 遍历该父节点的所有子节点
			if (!tempList.contains(node.getParentId())) {
				recursionFn(nodeList, node);
				returnList.add(node);
			}
		}
		if (returnList.isEmpty()) {
			returnList = nodeList;
		}
		return returnList;
	}

	private void recursionFn(List<BussProjectNode> nodeList, BussProjectNode node) {
		// 得到子节点列表
		List<BussProjectNode> childList = getChildList(nodeList, node);
		node.setChildren(childList);
		for (BussProjectNode tChild : childList) {
			if (hasChild(nodeList, tChild)) {
				recursionFn(nodeList, tChild);
			}
		}

	}

	/**
	 * 得到子节点列表
	 */
	private List<BussProjectNode> getChildList(List<BussProjectNode> list, BussProjectNode t) {
		List<BussProjectNode> tlist = new ArrayList<BussProjectNode>();
		Iterator<BussProjectNode> it = list.iterator();
		while (it.hasNext()) {
			BussProjectNode n = (BussProjectNode) it.next();
			if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getId().longValue()) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	/**
	 * 判断是否有子节点
	 */
	private boolean hasChild(List<BussProjectNode> list, BussProjectNode t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}

}
