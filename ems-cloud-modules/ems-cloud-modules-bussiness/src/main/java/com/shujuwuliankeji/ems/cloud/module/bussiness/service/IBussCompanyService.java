package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompany;

/**
 * 项目Service接口
 * 
 * @author fanzhongjie
 * @date 2022-12-30
 */
public interface IBussCompanyService {
	/**
	 * 查询项目
	 * 
	 * @param id 项目主键
	 * @return 项目
	 */
	public BussCompany selectBussCompanyById(Long id);

	/**
	 * 查询项目列表
	 * 
	 * @param bussCompany 项目
	 * @return 项目集合
	 */
	public List<BussCompany> selectBussCompanyList(BussCompany bussCompany);

	/**
	 * 新增项目
	 * 
	 * @param bussCompany 项目
	 * @return 结果
	 */
	public int insertBussCompany(BussCompany bussCompany);

	/**
	 * 修改项目
	 * 
	 * @param bussCompany 项目
	 * @return 结果
	 */
	public int updateBussCompany(BussCompany bussCompany);

	/**
	 * 批量删除项目
	 * 
	 * @param ids 需要删除的项目主键集合
	 * @return 结果
	 */
	public int deleteBussCompanyByIds(Long[] ids);

	/**
	 * 删除项目信息
	 * 
	 * @param id 项目主键
	 * @return 结果
	 */
	public int deleteBussCompanyById(Long id);

	/**
	 * 查询所有企业
	 * 
	 * @return
	 */
	public List<BussCompany> listAllCompany();

	/**
	 * 根据用户id查询企业
	 * 
	 * @param userId
	 * @return
	 */
	public BussCompany getCompanyByUserId(Long userId);
}
