package com.shujuwuliankeji.ems.cloud.module.bussiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableCustomConfig;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableRyFeignClients;
import com.shujuwuliankeji.ems.cloud.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 业务模块
 * 
 * @author fanzhongjie
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@EnableScheduling
@EnableAsync
@SpringBootApplication(scanBasePackages = { "com.shujuwuliankeji.ems.cloud" })
public class EmsCloudBussinessApplication {
	public static void main(String[] args) {
		SpringApplication.run(EmsCloudBussinessApplication.class, args);
	}
}
