package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussTacticsDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 策略详细Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-03-08
 */
public interface BussTacticsDetailMapper 
{

 /**
     * 查询策略详细
     * 
     * @param id 策略详细主键
     * @return 策略详细
     */
    public BussTacticsDetail selectBussTacticsDetailById(BussTacticsDetail bussTacticsDetail);

    /**
     * 查询策略详细列表
     * 
     * @param bussTacticsDetail 策略详细
     * @return 策略详细集合
     */
    public List<BussTacticsDetail> selectBussTacticsDetailList(BussTacticsDetail bussTacticsDetail);

    /**
     * 新增策略详细
     * 
     * @param bussTacticsDetail 策略详细
     * @return 结果
     */
    public int insertBussTacticsDetail(BussTacticsDetail bussTacticsDetail);

    /**
     * 修改策略详细
     * 
     * @param bussTacticsDetail 策略详细
     * @return 结果
     */
    public int updateBussTacticsDetail(BussTacticsDetail bussTacticsDetail);

    /**
     * 删除策略详细
     * 
     * @param id 策略详细主键
     * @return 结果
     */
    public int deleteBussTacticsDetailById(BussTacticsDetail bussTacticsDetail);

    /**
     * 批量删除策略详细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBussTacticsDetailByIds(@Param("entity") BussTacticsDetail bussTacticsDetail);

   /**
    * 批量新增策略明细
    * @param detailList
    */
   void insertBussTacticsDetails(@Param("detailList") List<BussTacticsDetail> detailList);
}
