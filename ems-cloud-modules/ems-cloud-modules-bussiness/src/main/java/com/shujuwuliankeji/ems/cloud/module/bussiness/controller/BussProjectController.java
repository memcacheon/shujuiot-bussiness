package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectService;

/**
 * 项目Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
@RestController
@RequestMapping("/project")
public class BussProjectController extends BaseController {
	@Autowired
	private IBussProjectService bussProjectService;

	/**
	 * 查询项目列表
	 */
	@RequiresPermissions("bussiness:project:list")
	@GetMapping("/list")
	public TableDataInfo list(BussProject bussProject) {
		startPage();
		List<BussProject> list = bussProjectService.selectBussProjectList(bussProject);
		return getDataTable(list);
	}

	/**
	 * 导出项目列表
	 */
	@RequiresPermissions("bussiness:project:export")
	@Log(title = "项目", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussProject bussProject) {
		List<BussProject> list = bussProjectService.selectBussProjectList(bussProject);
		ExcelUtil<BussProject> util = new ExcelUtil<BussProject>(BussProject.class);
		util.exportExcel(response, list, "项目数据");
	}

	/**
	 * 获取项目详细信息
	 */
	@RequiresPermissions("bussiness:project:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussProject project = new BussProject();
		project.setId(id);
		return success(bussProjectService.selectBussProjectById(project));
	}

	/**
	 * 新增项目
	 */
	@RequiresPermissions("bussiness:project:add")
	@Log(title = "项目", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussProject bussProject) {
		return toAjax(bussProjectService.insertBussProject(bussProject));
	}

	/**
	 * 修改项目
	 */
	@RequiresPermissions("bussiness:project:edit")
	@Log(title = "项目", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussProject bussProject) {
		return toAjax(bussProjectService.updateBussProject(bussProject));
	}

	/**
	 * 删除项目
	 */
	@RequiresPermissions("bussiness:project:remove")
	@Log(title = "项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussProject project = new BussProject();
		project.setIds(ids);
		return toAjax(bussProjectService.deleteBussProjectByIds(project));
	}

	@RequiresPermissions("bussiness:project:edit")
	@GetMapping("list/user")
	public AjaxResult listUser() {
		return success(bussProjectService.listUser());
	}

}
