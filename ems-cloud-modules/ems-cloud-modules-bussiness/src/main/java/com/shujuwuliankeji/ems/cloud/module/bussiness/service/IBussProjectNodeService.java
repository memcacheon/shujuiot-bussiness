package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectNode;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.NodeTreeSelect;

/**
 * 项目节点Service接口
 * 
 * @author fanzhongjie
 * @date 2023-01-12
 */
public interface IBussProjectNodeService {

	/**
	 * 查询项目节点
	 * 
	 * @param id 项目节点主键
	 * @return 项目节点
	 */
	public BussProjectNode selectBussProjectNodeById(BussProjectNode bussProjectNode);

	/**
	 * 查询项目节点列表
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 项目节点集合
	 */
	public List<BussProjectNode> selectBussProjectNodeList(BussProjectNode bussProjectNode);

	/**
	 * 新增项目节点
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 结果
	 */
	public int insertBussProjectNode(BussProjectNode bussProjectNode);

	/**
	 * 修改项目节点
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 结果
	 */
	public int updateBussProjectNode(BussProjectNode bussProjectNode);

	/**
	 * 批量删除项目节点
	 * 
	 * @param ids 需要删除的项目节点主键集合
	 * @return 结果
	 */
	public int deleteBussProjectNodeByIds(BussProjectNode bussProjectNode);

	/**
	 * 删除项目节点信息
	 * 
	 * @param id 项目节点主键
	 * @return 结果
	 */
	public int deleteBussProjectNodeById(BussProjectNode bussProjectNode);

	/**
	 * 获取树形节点数据
	 * 
	 * @param bussProjectNode
	 * @return
	 */
	public List<NodeTreeSelect> selectNodeTreeList(BussProjectNode bussProjectNode);

}
