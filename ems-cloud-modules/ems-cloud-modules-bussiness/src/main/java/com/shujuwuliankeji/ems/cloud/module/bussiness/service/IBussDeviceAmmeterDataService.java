package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterData;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.DeviceAmmeterDetailStatisticVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.StatisticVO;

/**
 * 电属性数据Service接口
 * 
 * @author fanzhongjie
 * @date 2023-02-04
 */
public interface IBussDeviceAmmeterDataService {

	/**
	 * 查询电属性数据
	 * 
	 * @param id 电属性数据主键
	 * @return 电属性数据
	 */
	public BussDeviceAmmeterData selectBussDeviceAmmeterDataById(Long id);

	/**
	 * 查询电属性数据列表
	 * 
	 * @param bussDeviceAmmeterData 电属性数据
	 * @return 电属性数据集合
	 */
	public List<BussDeviceAmmeterData> selectBussDeviceAmmeterDataList(BussDeviceAmmeterData bussDeviceAmmeterData);

	/**
	 * 查询最新的一条数据信息
	 * 
	 * @param bussDevice
	 * @return
	 */
	public DeviceAmmeterDetailStatisticVO lastDevieData(BussDeviceAmmeter bussDevice);

	/**
	 * 设备详情请求数据 x y
	 * 
	 * @param bussDevice
	 * @return
	 */
	public StatisticVO dataStatistic(BussDeviceAmmeter bussDevice);

	/**
	 * 新增电属性数据
	 *
	 * @param bussDeviceAmmeterData 电属性数据
	 * @return 结果
	 */
	public int insertBussDeviceAmmeterData(BussDeviceAmmeterData bussDeviceAmmeterData);





}
