package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.OneReceive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IOneReceiveService;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;

/**
 * 中台推送数据Controller
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
@RestController
@RequestMapping("/receive")
public class OneReceiveController extends BaseController
{
    @Autowired
    private IOneReceiveService oneReceiveService;

    /**
     * 查询中台推送数据列表
     */
    @RequiresPermissions("bussiness:receive:list")
    @GetMapping("/list")
    public TableDataInfo list(OneReceive oneReceive)
    {
        startPage();
        List<OneReceive> list = oneReceiveService.selectOneReceiveList(oneReceive);
        return getDataTable(list);
    }

    /**
     * 导出中台推送数据列表
     */
    @RequiresPermissions("bussiness:receive:export")
    @Log(title = "中台推送数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OneReceive oneReceive)
    {
        List<OneReceive> list = oneReceiveService.selectOneReceiveList(oneReceive);
        ExcelUtil<OneReceive> util = new ExcelUtil<OneReceive>(OneReceive.class);
        util.exportExcel(response, list, "中台推送数据数据");
    }

  /**
     * 获取中台推送数据详细信息
     */
    @RequiresPermissions("bussiness:receive:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
    	OneReceive oneReceive = new OneReceive() ;
    	oneReceive.setId(id) ;
        return success(oneReceiveService.selectOneReceiveById(oneReceive));
    }
  

    /**
     * 新增中台推送数据
     */
    @RequiresPermissions("bussiness:receive:add")
    @Log(title = "中台推送数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OneReceive oneReceive)
    {
        return toAjax(oneReceiveService.insertOneReceive(oneReceive));
    }

    /**
     * 修改中台推送数据
     */
    @RequiresPermissions("bussiness:receive:edit")
    @Log(title = "中台推送数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OneReceive oneReceive)
    {
        return toAjax(oneReceiveService.updateOneReceive(oneReceive));
    }
 /**
     * 删除中台推送数据
     */
    @RequiresPermissions("bussiness:receive:remove")
    @Log(title = "中台推送数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
    	OneReceive oneReceive = new OneReceive() ;
    	oneReceive.setIds(ids) ;
        return toAjax(oneReceiveService.deleteOneReceiveByIds(oneReceive));
    }
   
}
