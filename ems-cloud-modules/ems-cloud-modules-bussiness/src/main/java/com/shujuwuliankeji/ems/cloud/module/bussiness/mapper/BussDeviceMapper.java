package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;

/**
 * 设备管理Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-30
 */
public interface BussDeviceMapper {

	/**
	 * 查询设备管理
	 * 
	 * @param id 设备管理主键
	 * @return 设备管理
	 */
	public BussDevice selectBussDeviceById(BussDevice bussDevice);

	/**
	 * 查询设备管理列表
	 * 
	 * @param bussDevice 设备管理
	 * @return 设备管理集合
	 */
	public List<BussDevice> selectBussDeviceList(BussDevice bussDevice);

	/**
	 * 新增设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int insertBussDevice(BussDevice bussDevice);

	/**
	 * 修改设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int updateBussDevice(BussDevice bussDevice);

	/**
	 * 删除设备管理
	 * 
	 * @param id 设备管理主键
	 * @return 结果
	 */
	public int deleteBussDeviceById(BussDevice bussDevice);

	/**
	 * 批量删除设备管理
	 * 
	 * @param ids 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceByIds(@Param("entity") BussDevice bussDevice);

	/**
	 * 查询已分配拓扑设备
	 * 
	 * @param bussDevice
	 * @return
	 */
	public List<BussDevice> selectBussTopologyDeviceList(BussDevice bussDevice);

	/**
	 * 获取暂未分配的设备
	 * 
	 * @param bussDevice
	 * @return
	 */
	public List<BussDevice> selectBussNonTopologyDeviceList(BussDevice bussDevice);

	/**
	 * 批量设置设备拓扑
	 * 
	 * @param bussDevice
	 * @return
	 */
	public int addDeviceToTopology(@Param("entity") BussDevice bussDevice);

	/**
	 * 移除设备
	 * 
	 * @param device
	 * @return
	 */
	public int removeTopology(@Param("entity") BussDevice device);

	/**
	 * 检测设备是否存在
	 * 
	 * @param checkDevice
	 * @return
	 */
	public int checkDeviceExist(BussDevice checkDevice);

	/**
	 * 批量插入设备
	 * 
	 * @param list
	 * @return
	 */
	public int insertBussDeviceBatch(List<BussDevice> list);

	/**
	 * 查询在线设备数
	 * 
	 * @param device
	 * @return
	 */
	public int selectOnlineCount(BussDevice device);

	/**
	 * 查询总设备数
	 * 
	 * @param device
	 * @return
	 */
	public int selectDeviceCount(BussDevice device);
}
