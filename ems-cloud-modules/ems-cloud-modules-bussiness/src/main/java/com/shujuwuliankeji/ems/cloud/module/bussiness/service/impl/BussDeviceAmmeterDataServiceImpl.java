package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterData;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.DeviceAmmeterDetailStatisticVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.StatisticVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceAmmeterDataMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceAmmeterDataService;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;

/**
 * 电属性数据Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-02-04
 */
@Service
public class BussDeviceAmmeterDataServiceImpl implements IBussDeviceAmmeterDataService {
	@Autowired
	private BussDeviceAmmeterDataMapper bussDeviceAmmeterDataMapper;

	/**
	 * 查询电属性数据
	 * 
	 * @param id 电属性数据主键
	 * @return 电属性数据
	 */
	@Override
	public BussDeviceAmmeterData selectBussDeviceAmmeterDataById(Long id) {
		return bussDeviceAmmeterDataMapper.selectBussDeviceAmmeterDataById(id);
	}

	/**
	 * 查询电属性数据列表
	 * 
	 * @param bussDeviceAmmeterData 电属性数据
	 * @return 电属性数据
	 */
	@Override
	public List<BussDeviceAmmeterData> selectBussDeviceAmmeterDataList(BussDeviceAmmeterData bussDeviceAmmeterData) {
		return bussDeviceAmmeterDataMapper.selectBussDeviceAmmeterDataList(bussDeviceAmmeterData);
	}

	/**
	 * 新增电属性数据
	 *
	 * @param bussDeviceAmmeterData 电属性数据
	 * @return 结果
	 */
	@Override
	@SaaS
	public int insertBussDeviceAmmeterData(BussDeviceAmmeterData bussDeviceAmmeterData)
	{
		bussDeviceAmmeterData.setCreateTime(DateUtils.getNowDate());
		return bussDeviceAmmeterDataMapper.insertBussDeviceAmmeterData(bussDeviceAmmeterData);
	}
	@Override
	@SaaS
	public DeviceAmmeterDetailStatisticVO lastDevieData(BussDeviceAmmeter bussDevice) {
		DeviceAmmeterDetailStatisticVO vo = new DeviceAmmeterDetailStatisticVO();

		BussDeviceAmmeterData data = bussDeviceAmmeterDataMapper.lastDevieData(bussDevice);
		if (data != null) {
			vo.setVoltage(data.getUa());
			vo.setElectricCurrent(data.getIa());
			vo.setTotalActivePower(data.getP());
			vo.setCreateDate(data.getCreateTime());
		}

		return vo;
	}

	@SaaS
	@Override
	public StatisticVO dataStatistic(BussDeviceAmmeter bussDevice) {
		StatisticVO vo = new StatisticVO();
		List<BussDeviceAmmeterData> data = bussDeviceAmmeterDataMapper.dataStatistic(bussDevice);
		if (CollUtil.isNotEmpty(data)) {
			List<String> xData = new ArrayList<>();
			List<Object> yData = new ArrayList<>();
			vo.setxData(xData);
			vo.setyData(yData);
			data.forEach(d -> {
				xData.add(DateUtil.format(d.getCreateTime(), "HH:mm"));
				yData.add(BeanUtil.getFieldValue(d, bussDevice.getField()));
			});
		}
		return vo;
	}

}
