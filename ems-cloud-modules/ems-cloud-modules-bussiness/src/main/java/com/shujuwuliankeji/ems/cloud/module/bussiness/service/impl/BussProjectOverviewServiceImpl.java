package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectOverview;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussProjectOverviewMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectOverviewService;

/**
 * 项目Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-05
 */
@Service
public class BussProjectOverviewServiceImpl implements IBussProjectOverviewService {
	@Autowired
	private BussProjectOverviewMapper bussProjectOverviewMapper;

	/**
	 * 查询项目
	 * 
	 * @param id 项目主键
	 * @return 项目
	 */
	@Override
	public BussProjectOverview selectBussProjectOverviewById(Long id) {
		return bussProjectOverviewMapper.selectBussProjectOverviewById(id);
	}

	/**
	 * 查询项目列表
	 * 
	 * @param bussProject 项目
	 * @return 项目
	 */
	@Override
	public List<BussProjectOverview> selectBussProjectOverviewList(BussProjectOverview bussProject) {
		return bussProjectOverviewMapper.selectBussProjectOverviewList(bussProject);
	}

}
