package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeterWarningRules;

/**
 * 电报警规则Service接口
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
public interface IBussDeviceAmmeterWarningRulesService {

	/**
	 * 查询电报警规则
	 * 
	 * @param id 电报警规则主键
	 * @return 电报警规则
	 */
	public BussDeviceAmmeterWarningRules selectBussDeviceAmmeterWarningRulesById(
			BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules);

	/**
	 * 查询电报警规则列表
	 * 
	 * @param bussDeviceAmmeterWarningRules 电报警规则
	 * @return 电报警规则集合
	 */
	public List<BussDeviceAmmeterWarningRules> selectBussDeviceAmmeterWarningRulesList(
			BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules);

	/**
	 * 新增电报警规则
	 * 
	 * @param bussDeviceAmmeterWarningRules 电报警规则
	 * @return 结果
	 */
	public int insertBussDeviceAmmeterWarningRules(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules);

	/**
	 * 修改电报警规则
	 * 
	 * @param bussDeviceAmmeterWarningRules 电报警规则
	 * @return 结果
	 */
	public int updateBussDeviceAmmeterWarningRules(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules);

	/**
	 * 批量删除电报警规则
	 * 
	 * @param ids 需要删除的电报警规则主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceAmmeterWarningRulesByIds(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules);

	/**
	 * 删除电报警规则信息
	 * 
	 * @param id 电报警规则主键
	 * @return 结果
	 */
	public int deleteBussDeviceAmmeterWarningRulesById(BussDeviceAmmeterWarningRules bussDeviceAmmeterWarningRules);

}
