package com.shujuwuliankeji.ems.cloud.module.bussiness.constant;

/**
 * 设备类型
 * 
 * @author Fanzhongjie
 * @date 2023年2月2日 上午11:15:40
 * @tips TODO
 */
public enum DeviceTypeEnum {

	AMMETER("电表", 1), WATERMETER("水表", 2);

	private Integer value;
	private String name;

	private DeviceTypeEnum(String name, Integer value) {
		this.value = value;
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
