package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.constant.UserConstants;
import com.shujuwuliankeji.ems.cloud.common.core.utils.StringUtils;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyDept;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyDeptService;

/**
 * 部门信息
 * 
 * @author fanzhongjie
 */
@RestController
@RequestMapping("company/dept")
public class BussCompanyDeptController extends BaseController {
	@Autowired
	private IBussCompanyDeptService deptService;

	/**
	 * 获取部门列表
	 */
	@RequiresPermissions("bussiness:company:dept:list")
	@GetMapping("/list")
	@SaaS
	public AjaxResult list(BussCompanyDept dept) {
		List<BussCompanyDept> depts = deptService.selectDeptList(dept);
		return success(depts);
	}

	/**
	 * 查询部门列表（排除节点）
	 */
	@RequiresPermissions("bussiness:company:dept:list")
	@GetMapping("/list/exclude/{deptId}")
	public AjaxResult excludeChild(@PathVariable(value = "deptId", required = false) Long deptId) {
		List<BussCompanyDept> depts = deptService.selectDeptList(new BussCompanyDept());
		depts.removeIf(d -> d.getDeptId().intValue() == deptId
				|| ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + ""));
		return success(depts);
	}

	/**
	 * 根据部门编号获取详细信息
	 */
	@RequiresPermissions("bussiness:company:dept:query")
	@GetMapping(value = "/{deptId}")
	public AjaxResult getInfo(@PathVariable Long deptId) {
		BussCompanyDept dept = new BussCompanyDept();
		dept.setDeptId(deptId);
		return success(deptService.selectDeptById(dept));
	}

	/**
	 * 新增部门
	 */
	@RequiresPermissions("bussiness:company:dept:add")
	@Log(title = "部门管理", businessType = BusinessType.INSERT)
	@PostMapping
	@SaaS
	public AjaxResult add(@Validated @RequestBody BussCompanyDept dept) {
		if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept))) {
			return error("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
		}
		dept.setCreateBy(SecurityUtils.getUsername());
		return toAjax(deptService.insertDept(dept));
	}

	/**
	 * 修改部门
	 */
	@RequiresPermissions("bussiness:company:dept:edit")
	@Log(title = "部门管理", businessType = BusinessType.UPDATE)
	@PutMapping
	@SaaS
	public AjaxResult edit(@Validated @RequestBody BussCompanyDept dept) {
		Long deptId = dept.getDeptId();
		if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept))) {
			return error("修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
		} else if (dept.getParentId().equals(deptId)) {
			return error("修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
		} else if (StringUtils.equals(UserConstants.DEPT_DISABLE, dept.getStatus())
				&& deptService.selectNormalChildrenDeptById(deptId) > 0) {
			return error("该部门包含未停用的子部门！");
		}
		dept.setUpdateBy(SecurityUtils.getUsername());
		return toAjax(deptService.updateDept(dept));
	}

	/**
	 * 删除部门
	 */
	@RequiresPermissions("bussiness:company:dept:remove")
	@Log(title = "部门管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{deptId}")
	public AjaxResult remove(@PathVariable Long deptId) {
		if (deptService.hasChildByDeptId(deptId)) {
			return warn("存在下级部门,不允许删除");
		}
		if (deptService.checkDeptExistUser(deptId)) {
			return warn("部门存在用户,不允许删除");
		}
		BussCompanyDept dept = new BussCompanyDept();
		dept.setDeptId(deptId);
		return toAjax(deptService.deleteDeptById(dept));
	}
}
