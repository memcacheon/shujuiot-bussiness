package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectUser;

/**
 * 项目用户关联Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-06
 */
public interface BussProjectUserMapper {
	/**
	 * 查询项目用户关联
	 * 
	 * @param userId 项目用户关联主键
	 * @return 项目用户关联
	 */
	public BussProjectUser selectBussProjectUserByUserId(Long userId);

	/**
	 * 查询项目用户关联列表
	 * 
	 * @param bussProjectUser 项目用户关联
	 * @return 项目用户关联集合
	 */
	public List<BussProjectUser> selectBussProjectUserList(BussProjectUser bussProjectUser);

	/**
	 * 新增项目用户关联
	 * 
	 * @param bussProjectUser 项目用户关联
	 * @return 结果
	 */
	public int insertBussProjectUser(BussProjectUser bussProjectUser);

	/**
	 * 修改项目用户关联
	 * 
	 * @param bussProjectUser 项目用户关联
	 * @return 结果
	 */
	public int updateBussProjectUser(BussProjectUser bussProjectUser);

	/**
	 * 删除项目用户关联
	 * 
	 * @param userId 项目用户关联主键
	 * @return 结果
	 */
	public int deleteBussProjectUserByUserId(Long userId);

	/**
	 * 批量删除项目用户关联
	 * 
	 * @param userIds 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussProjectUserByUserIds(Long[] userIds);

	/**
	 * 
	 * @return
	 */
	public int batchBussProjectUser(List<BussProjectUser> projectUsers);
}
