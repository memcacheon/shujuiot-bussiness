package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectNode;

/**
 * 项目节点Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-12
 */
public interface BussProjectNodeMapper {

	/**
	 * 查询项目节点
	 * 
	 * @param id 项目节点主键
	 * @return 项目节点
	 */
	public BussProjectNode selectBussProjectNodeById(BussProjectNode bussProjectNode);

	/**
	 * 查询项目节点列表
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 项目节点集合
	 */
	public List<BussProjectNode> selectBussProjectNodeList(BussProjectNode bussProjectNode);

	/**
	 * 新增项目节点
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 结果
	 */
	public int insertBussProjectNode(BussProjectNode bussProjectNode);

	/**
	 * 修改项目节点
	 * 
	 * @param bussProjectNode 项目节点
	 * @return 结果
	 */
	public int updateBussProjectNode(BussProjectNode bussProjectNode);

	/**
	 * 删除项目节点
	 * 
	 * @param id 项目节点主键
	 * @return 结果
	 */
	public int deleteBussProjectNodeById(BussProjectNode bussProjectNode);

	/**
	 * 批量删除项目节点
	 * 
	 * @param ids 需要删除的数据主键集合
	 * @return 结果
	 */
	public int deleteBussProjectNodeByIds(@Param("entity") BussProjectNode bussProjectNode);
}
