package com.shujuwuliankeji.ems.cloud.module.bussiness.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.BaseEntity;

/**
 * 电报警规则对象 buss_device_ammeter_warning_rules
 * 
 * @author fanzhongjie
 * @date 2023-02-09
 */
public class BussDeviceAmmeterWarningRules extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** ID */
	private Long id;

	/** 企业ID */
	private Long companyId;

	/** 项目ID */
	@Excel(name = "项目ID")
	private Long projectId;

	/** 报警事件类型 */
	@Excel(name = "报警事件类型")
	private Integer eventType;

	/** 报警级别 */
	@Excel(name = "报警级别")
	private Integer eventLevel;

	/** 触发属性 */
	@Excel(name = "触发属性")
	private String conditionProperty;

	/** 触发条件 */
	@Excel(name = "触发条件")
	private String condition;

	/** 触发值 */
	@Excel(name = "触发值")
	private BigDecimal conditionValue;

	/** 是否启用 */
	@Excel(name = "是否启用")
	private String status;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}

	public Integer getEventType() {
		return eventType;
	}

	public void setEventLevel(Integer eventLevel) {
		this.eventLevel = eventLevel;
	}

	public Integer getEventLevel() {
		return eventLevel;
	}

	public void setConditionProperty(String conditionProperty) {
		this.conditionProperty = conditionProperty;
	}

	public String getConditionProperty() {
		return conditionProperty;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getCondition() {
		return condition;
	}

	public void setConditionValue(BigDecimal conditionValue) {
		this.conditionValue = conditionValue;
	}

	public BigDecimal getConditionValue() {
		return conditionValue;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("createBy", getCreateBy()).append("createTime", getCreateTime())
				.append("updateBy", getUpdateBy()).append("updateTime", getUpdateTime())
				.append("companyId", getCompanyId()).append("projectId", getProjectId())
				.append("eventType", getEventType()).append("eventLevel", getEventLevel())
				.append("conditionProperty", getConditionProperty()).append("condition", getCondition())
				.append("conditionValue", getConditionValue()).append("status", getStatus()).toString();
	}
}
