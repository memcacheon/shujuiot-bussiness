package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.shujuwuliankeji.ems.cloud.common.core.utils.DateUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussTacticsDetail;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussTacticsDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussTacticsMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussTactics;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussTacticsService;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import org.springframework.transaction.annotation.Transactional;

/**
 * 策略Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-03-07
 */
@Service
public class BussTacticsServiceImpl implements IBussTacticsService 
{
    @Autowired
    private BussTacticsMapper bussTacticsMapper;

    @Autowired
    private BussTacticsDetailMapper bussTacticsDetailMapper;

	/**
     * 查询策略
     * 
     * @param id 策略主键
     * @return 策略
     */
    @Override
    @SaaS
    public BussTactics selectBussTacticsById(BussTactics bussTactics)
    {
        return bussTacticsMapper.selectBussTacticsById(bussTactics);
    }

    /**
     * 查询策略列表
     * 
     * @param bussTactics 策略
     * @return 策略
     */
    @Override
    @SaaS
    public List<BussTactics> selectBussTacticsList(BussTactics bussTactics)
    {
        return bussTacticsMapper.selectBussTacticsList(bussTactics);
    }

    /**
     * 新增策略
     * 
     * @param bussTactics 策略
     * @return 结果
     */
    @Override
    @SaaS
    @Transactional(rollbackFor = Exception.class)
    public int insertBussTactics(BussTactics bussTactics)
    {
        bussTactics.setCreateTime(DateUtils.getNowDate());
        bussTacticsMapper.insertBussTactics(bussTactics);
        //新增策略再新增策略明细
        List<BussTacticsDetail> detailList = bussTactics.getDetailList();
        detailList=detailList.stream().map(e ->{
            e.setTacticsId(bussTactics.getId());
            return e;
        }).collect(Collectors.toList());
        bussTacticsDetailMapper.insertBussTacticsDetails(detailList);


        return 1;
    }

    /**
     * 修改策略
     * 
     * @param bussTactics 策略
     * @return 结果
     */
	@SaaS
    @Override
    public int updateBussTactics(BussTactics bussTactics)
    {
        bussTactics.setUpdateTime(DateUtils.getNowDate());
        return bussTacticsMapper.updateBussTactics(bussTactics);
    }

    /**
     * 批量删除策略
     * 
     * @param ids 需要删除的策略主键
     * @return 结果
     */
	@SaaS
    @Override
    public int deleteBussTacticsByIds(BussTactics bussTactics)
    {
        return bussTacticsMapper.deleteBussTacticsByIds(bussTactics);
    }

    /**
     * 删除策略信息
     * 
     * @param id 策略主键
     * @return 结果
     */
    @Override
    @SaaS
    public int deleteBussTacticsById(BussTactics bussTactics)
    {
        return bussTacticsMapper.deleteBussTacticsById(bussTactics);
    }


    
}
