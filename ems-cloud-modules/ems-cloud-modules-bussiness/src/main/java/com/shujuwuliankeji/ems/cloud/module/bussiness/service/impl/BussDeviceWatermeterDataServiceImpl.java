package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.security.annotation.SaaS;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeter;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceWatermeterData;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.DeviceWatermeterDetailStatisticVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.StatisticVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussDeviceWatermeterDataMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceWatermeterDataService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;

/**
 * 电属性数据Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-02-04
 */
@Service
public class BussDeviceWatermeterDataServiceImpl implements IBussDeviceWatermeterDataService {

	@Autowired
	private BussDeviceWatermeterDataMapper bussDeviceWatermeterDataMapper;

	/**
	 * 查询电属性数据
	 * 
	 * @param id 电属性数据主键
	 * @return 电属性数据
	 */
	@Override
	public BussDeviceWatermeterData selectBussDeviceWatermeterDataById(Long id) {
		return bussDeviceWatermeterDataMapper.selectBussDeviceAmmeterDataById(id);
	}

	/**
	 * 查询电属性数据列表
	 * 
	 * @param bussDeviceAmmeterData 电属性数据
	 * @return 电属性数据
	 */
	@Override
	public List<BussDeviceWatermeterData> selectBussDeviceWatermeterDataList(
			BussDeviceWatermeterData bussDeviceAmmeterData) {
		return bussDeviceWatermeterDataMapper.selectBussDeviceWatermeterDataList(bussDeviceAmmeterData);
	}

	@Override
	@SaaS
	public DeviceWatermeterDetailStatisticVO lastDevieData(BussDeviceWatermeter bussDevice) {
		DeviceWatermeterDetailStatisticVO vo = new DeviceWatermeterDetailStatisticVO();

		BussDeviceWatermeterData data = bussDeviceWatermeterDataMapper.lastDevieData(bussDevice);
		if (data != null) {
			vo.setWaterConsumption(data.getWaterConsumption());
			vo.setWaterMeterState(data.getWaterMeterState());
			vo.setCreateDate(data.getCreateTime());
		}

		return vo;
	}

	@SaaS
	@Override
	public StatisticVO dataStatistic(BussDeviceWatermeter bussDevice) {
		StatisticVO vo = new StatisticVO();
		List<BussDeviceWatermeterData> data = bussDeviceWatermeterDataMapper.dataStatistic(bussDevice);
		if (CollUtil.isNotEmpty(data)) {
			List<String> xData = new ArrayList<>();
			List<Object> yData = new ArrayList<>();
			vo.setxData(xData);
			vo.setyData(yData);
			data.forEach(d -> {
				xData.add(DateUtil.format(d.getCreateTime(), "HH:mm"));
				yData.add(d.getWaterConsumption());
			});
		}
		return vo;
	}

}
