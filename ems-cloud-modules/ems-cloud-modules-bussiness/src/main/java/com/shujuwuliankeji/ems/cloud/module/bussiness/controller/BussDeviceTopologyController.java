package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectNode;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.NodeTreeSelect;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.ProjectVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussDeviceService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectNodeService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectService;

import cn.hutool.core.bean.BeanUtil;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年1月30日 下午1:47:52
 * @tips 设备拓扑
 */
@RestController
@RequestMapping("/device/topology")
public class BussDeviceTopologyController extends BaseController {

	@Autowired
	private IBussProjectService projectService;

	@Autowired
	private IBussProjectNodeService projectNodeService;

	@Autowired
	private IBussDeviceService deviceService;

	/**
	 * 获取企业下项目
	 * 
	 * @return
	 */
	@RequiresPermissions("bussiness:device:topology:list")
	@GetMapping("project/list")
	public AjaxResult projectList() {
		List<BussProject> projectList = projectService.selectBussProjectList(new BussProject());
		List<ProjectVO> list = BeanUtil.copyToList(projectList, ProjectVO.class);
		return success(list);
	}

	/**
	 * 获取项目节点tree
	 * 
	 * @param projectId
	 * @return
	 */
	@RequiresPermissions("bussiness:device:topology:list")
	@GetMapping("project/node/tree")
	public AjaxResult projectNodeTree(@RequestParam(required = false) Long projectId) {
		BussProjectNode bussProjectNode = new BussProjectNode();
		bussProjectNode.setProjectId(projectId);
		List<NodeTreeSelect> tree = projectNodeService.selectNodeTreeList(bussProjectNode);
		return success(tree);
	}

	@RequiresPermissions("bussiness:device:topology:list")
	@GetMapping("device/list")
	public TableDataInfo list(BussDevice bussDevice) {
		startPage();
		List<BussDevice> list = deviceService.selectBussTopologyDeviceList(bussDevice);
		return getDataTable(list);
	}

	@RequiresPermissions("bussiness:device:topology:add")
	@Log(title = "设备拓扑", businessType = BusinessType.INSERT)
	@PostMapping("device/add")
	public AjaxResult add(@RequestBody BussDevice bussDevice) {
		int count = deviceService.addDeviceToTopology(bussDevice);
		return toAjax(count);
	}

	@RequiresPermissions("bussiness:device:topology:remove")
	@Log(title = "设备拓扑", businessType = BusinessType.DELETE)
	@DeleteMapping("device/remove/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussDevice device = new BussDevice();
		device.setIds(ids);
		return toAjax(deviceService.removeTopology(device));
	}

	/**
	 * 获取暂未分配的设备数据
	 * 
	 * @param bussDevice
	 * @return
	 */
	@RequiresPermissions("bussiness:device:topology:add")
	@GetMapping("allocate/device/list")
	public TableDataInfo allocateDeviceList(BussDevice bussDevice) {
		startPage();
		List<BussDevice> list = deviceService.selectBussNonTopologyDeviceList(bussDevice);
		return getDataTable(list);
	}

}
