package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.domain.R;
import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.core.web.page.TableDataInfo;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompany;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectOverview;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.CompanyVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectOverviewService;

import cn.hutool.core.bean.BeanUtil;

/**
 * 项目Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-05
 */
@RestController
@RequestMapping("/project/overview")
public class BussProjectOverviewController extends BaseController {

	@Autowired
	private IBussProjectOverviewService bussProjectOverviewService;

	@Autowired
	private IBussCompanyService companyService;

	/**
	 * 查询项目列表
	 */
	@RequiresPermissions("bussiness:project:overview:list")
	@GetMapping("/list")
	public TableDataInfo list(BussProjectOverview bussProject) {
		startPage();
		List<BussProjectOverview> list = bussProjectOverviewService.selectBussProjectOverviewList(bussProject);
		return getDataTable(list);
	}

	/**
	 * 导出项目列表
	 */
	@RequiresPermissions("bussiness:project:overview:export")
	@Log(title = "项目", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussProjectOverview bussProject) {
		List<BussProjectOverview> list = bussProjectOverviewService.selectBussProjectOverviewList(bussProject);
		ExcelUtil<BussProjectOverview> util = new ExcelUtil<BussProjectOverview>(BussProjectOverview.class);
		util.exportExcel(response, list, "项目数据");
	}

	/**
	 * 获取项目详细信息
	 */
	@RequiresPermissions("bussiness:project:overview:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		return success(bussProjectOverviewService.selectBussProjectOverviewById(id));
	}

	@RequiresPermissions("bussiness:project:overview:list")
	@GetMapping("/company/list")
	public R<List<CompanyVO>> companyList() {
		List<BussCompany> list = companyService.listAllCompany();
		return R.ok(BeanUtil.copyToList(list, CompanyVO.class));
	}

}
