package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyInfo;

/**
 * 企业信息Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-11
 */
public interface BussCompanyInfoMapper {

	/**
	 * 查询企业信息
	 * 
	 * @param id 企业信息主键
	 * @return 企业信息
	 */
	public BussCompanyInfo selectBussCompanyInfoById(BussCompanyInfo bussCompanyInfo);

	/**
	 * 修改企业信息
	 * 
	 * @param bussCompanyInfo 企业信息
	 * @return 结果
	 */
	public int updateBussCompanyInfo(BussCompanyInfo bussCompanyInfo);
}
