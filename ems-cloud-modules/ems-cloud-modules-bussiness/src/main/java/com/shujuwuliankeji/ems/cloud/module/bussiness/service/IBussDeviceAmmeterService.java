package com.shujuwuliankeji.ems.cloud.module.bussiness.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDevice;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussDeviceAmmeter;

/**
 * 设备管理Service接口
 * 
 * @author fanzhongjie
 * @date 2023-01-28
 */
public interface IBussDeviceAmmeterService {

	/**
	 * 查询设备管理
	 * 
	 * @param id 设备管理主键
	 * @return 设备管理
	 */
	public BussDeviceAmmeter selectBussDeviceById(BussDeviceAmmeter bussDevice);

	/**
	 * 查询设备管理列表
	 * 
	 * @param bussDevice 设备管理
	 * @return 设备管理集合
	 */
	public List<BussDeviceAmmeter> selectBussDeviceList(BussDeviceAmmeter bussDevice);

	/**
	 * 新增设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int insertBussDevice(BussDeviceAmmeter bussDevice);

	/**
	 * 修改设备管理
	 * 
	 * @param bussDevice 设备管理
	 * @return 结果
	 */
	public int updateBussDevice(BussDeviceAmmeter bussDevice);

	/**
	 * 批量删除设备管理
	 * 
	 * @param ids 需要删除的设备管理主键集合
	 * @return 结果
	 */
	public int deleteBussDeviceByIds(BussDeviceAmmeter bussDevice);

	/**
	 * 删除设备管理信息
	 * 
	 * @param id 设备管理主键
	 * @return 结果
	 */
	public int deleteBussDeviceById(BussDeviceAmmeter bussDevice);

	/**
	 * 同步中台设备数据
	 * 
	 * @param bussDevice
	 */
	public void pollData(BussDevice bussDevice);

}
