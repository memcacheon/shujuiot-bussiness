package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompanyUser;

/**
 * 用户信息Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-01-07
 */
public interface BussCompanyUserMapper {

	/**
	 * 查询单个用户信息
	 * 
	 * @param user
	 * @return 用户信息
	 */
	public BussCompanyUser selectBussCompanyUserOne(BussCompanyUser user);

	/**
	 * 查询用户信息
	 * 
	 * @param userId 用户信息主键
	 * @return 用户信息
	 */
	public BussCompanyUser selectBussCompanyUserByUserId(Long userId);

	/**
	 * 查询用户信息列表
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 用户信息集合
	 */
	public List<BussCompanyUser> selectBussCompanyUserList(BussCompanyUser bussCompanyUser);

	/**
	 * 新增用户信息
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 结果
	 */
	public int insertBussCompanyUser(BussCompanyUser bussCompanyUser);

	/**
	 * 修改用户信息
	 * 
	 * @param bussCompanyUser 用户信息
	 * @return 结果
	 */
	public int updateBussCompanyUser(BussCompanyUser bussCompanyUser);

	/**
	 * 删除用户信息
	 * 
	 * @param userId 用户信息主键
	 * @return 结果
	 */
	public int deleteBussCompanyUserByUserId(Long userId);

	/**
	 * 批量删除用户信息
	 * 
	 * @param userIds   需要删除的数据主键集合
	 * @param companyId
	 * @return 结果
	 */
	public int deleteBussCompanyUserByUserIds(@Param("userIds") Long[] userIds, @Param("companyId") Long companyId);

	/**
	 * 校验用户名称是否唯一
	 * 
	 * @param userName 用户名称
	 * @return 结果
	 */
	public BussCompanyUser checkUserNameUnique(String userName);

	/**
	 * 校验手机号码是否唯一
	 *
	 * @param phonenumber 手机号码
	 * @return 结果
	 */
	public BussCompanyUser checkPhoneUnique(String phonenumber);

	/**
	 * 校验email是否唯一
	 *
	 * @param email 用户邮箱
	 * @return 结果
	 */
	public BussCompanyUser checkEmailUnique(String email);
}
