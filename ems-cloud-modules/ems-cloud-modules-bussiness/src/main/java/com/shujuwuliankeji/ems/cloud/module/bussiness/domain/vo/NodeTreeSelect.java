package com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectNode;

/**
 * Treeselect树结构实体类
 * 
 * @author ruoyi
 */
public class NodeTreeSelect implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 节点ID */
	private Long id;

	/** 节点名称 */
	private String label;

	/** 子节点 */
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<NodeTreeSelect> children;

	public NodeTreeSelect() {

	}

	public NodeTreeSelect(BussProjectNode node) {
		this.id = node.getId();
		this.label = node.getNodeName();
		this.children = node.getChildren().stream().map(NodeTreeSelect::new).collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<NodeTreeSelect> getChildren() {
		return children;
	}

	public void setChildren(List<NodeTreeSelect> children) {
		this.children = children;
	}
}
