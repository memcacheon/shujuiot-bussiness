package com.shujuwuliankeji.ems.cloud.module.bussiness.mapper;

import java.util.List;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.OneReceive;
import org.apache.ibatis.annotations.Param;

/**
 * 中台推送数据Mapper接口
 * 
 * @author fanzhongjie
 * @date 2023-06-20
 */
public interface OneReceiveMapper 
{

 /**
     * 查询中台推送数据
     * 
     * @param id 中台推送数据主键
     * @return 中台推送数据
     */
    public OneReceive selectOneReceiveById(OneReceive oneReceive);

    /**
     * 查询中台推送数据列表
     * 
     * @param oneReceive 中台推送数据
     * @return 中台推送数据集合
     */
    public List<OneReceive> selectOneReceiveList(OneReceive oneReceive);

    /**
     * 新增中台推送数据
     * 
     * @param oneReceive 中台推送数据
     * @return 结果
     */
    public int insertOneReceive(OneReceive oneReceive);

    /**
     * 修改中台推送数据
     * 
     * @param oneReceive 中台推送数据
     * @return 结果
     */
    public int updateOneReceive(OneReceive oneReceive);

    /**
     * 删除中台推送数据
     * 
     * @param id 中台推送数据主键
     * @return 结果
     */
    public int deleteOneReceiveById(OneReceive oneReceive);

    /**
     * 批量删除中台推送数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOneReceiveByIds(@Param("entity") OneReceive oneReceive);
}
