package com.shujuwuliankeji.ems.cloud.module.bussiness.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.common.security.utils.SecurityUtils;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectUser;
import com.shujuwuliankeji.ems.cloud.module.bussiness.mapper.BussProjectUserMapper;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectUserService;

/**
 * 项目用户关联Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-06
 */
@Service
public class BussProjectUserServiceImpl implements IBussProjectUserService {
	@Autowired
	private BussProjectUserMapper bussProjectUserMapper;

	/**
	 * 查询项目用户关联
	 * 
	 * @param userId 项目用户关联主键
	 * @return 项目用户关联
	 */
	@Override
	public BussProjectUser selectBussProjectUserByUserId(Long userId) {
		SecurityUtils.getLoginUser();
		return bussProjectUserMapper.selectBussProjectUserByUserId(userId);
	}

	/**
	 * 查询项目用户关联列表
	 * 
	 * @param bussProjectUser 项目用户关联
	 * @return 项目用户关联
	 */
	@Override
	public List<BussProjectUser> selectBussProjectUserList(BussProjectUser bussProjectUser) {
		return bussProjectUserMapper.selectBussProjectUserList(bussProjectUser);
	}

	/**
	 * 新增项目用户关联
	 * 
	 * @param bussProjectUser 项目用户关联
	 * @return 结果
	 */
	@Override
	public int insertBussProjectUser(BussProjectUser bussProjectUser) {
		return bussProjectUserMapper.insertBussProjectUser(bussProjectUser);
	}

	/**
	 * 修改项目用户关联
	 * 
	 * @param bussProjectUser 项目用户关联
	 * @return 结果
	 */
	@Override
	public int updateBussProjectUser(BussProjectUser bussProjectUser) {
		return bussProjectUserMapper.updateBussProjectUser(bussProjectUser);
	}

	/**
	 * 批量删除项目用户关联
	 * 
	 * @param userIds 需要删除的项目用户关联主键
	 * @return 结果
	 */
	@Override
	public int deleteBussProjectUserByUserIds(Long[] userIds) {
		return bussProjectUserMapper.deleteBussProjectUserByUserIds(userIds);
	}

	/**
	 * 删除项目用户关联信息
	 * 
	 * @param userId 项目用户关联主键
	 * @return 结果
	 */
	@Override
	public int deleteBussProjectUserByUserId(Long userId) {
		return bussProjectUserMapper.deleteBussProjectUserByUserId(userId);
	}
}
