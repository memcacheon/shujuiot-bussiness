package com.shujuwuliankeji.ems.cloud.module.bussiness.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProject;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussProjectNode;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.vo.ProjectVO;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectNodeService;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussProjectService;

import cn.hutool.core.bean.BeanUtil;

/**
 * 项目节点Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-12
 */
@RestController
@RequestMapping("/projectNode")
public class BussProjectNodeController extends BaseController {
	@Autowired
	private IBussProjectNodeService bussProjectNodeService;

	@Autowired
	private IBussProjectService projectService;

	/**
	 * 查询项目节点列表
	 */
	@RequiresPermissions("bussiness:projectNode:list")
	@GetMapping("/list")
	public AjaxResult list(BussProjectNode bussProjectNode) {
		List<BussProjectNode> list = bussProjectNodeService.selectBussProjectNodeList(bussProjectNode);
		return success(list);
	}

	/**
	 * 导出项目节点列表
	 */
	@RequiresPermissions("bussiness:projectNode:export")
	@Log(title = "项目节点", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, BussProjectNode bussProjectNode) {
		List<BussProjectNode> list = bussProjectNodeService.selectBussProjectNodeList(bussProjectNode);
		ExcelUtil<BussProjectNode> util = new ExcelUtil<BussProjectNode>(BussProjectNode.class);
		util.exportExcel(response, list, "项目节点数据");
	}

	/**
	 * 获取项目节点详细信息
	 */
	@RequiresPermissions("bussiness:projectNode:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		BussProjectNode bussProjectNode = new BussProjectNode();
		bussProjectNode.setId(id);
		return success(bussProjectNodeService.selectBussProjectNodeById(bussProjectNode));
	}

	/**
	 * 新增项目节点
	 */
	@RequiresPermissions("bussiness:projectNode:add")
	@Log(title = "项目节点", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody BussProjectNode bussProjectNode) {
		return toAjax(bussProjectNodeService.insertBussProjectNode(bussProjectNode));
	}

	/**
	 * 修改项目节点
	 */
	@RequiresPermissions("bussiness:projectNode:edit")
	@Log(title = "项目节点", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody BussProjectNode bussProjectNode) {
		return toAjax(bussProjectNodeService.updateBussProjectNode(bussProjectNode));
	}

	/**
	 * 删除项目节点
	 */
	@RequiresPermissions("bussiness:projectNode:remove")
	@Log(title = "项目节点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		BussProjectNode bussProjectNode = new BussProjectNode();
		bussProjectNode.setIds(ids);
		return toAjax(bussProjectNodeService.deleteBussProjectNodeByIds(bussProjectNode));
	}

	@RequiresPermissions("bussiness:projectNode:list")
	@GetMapping("project/list")
	public AjaxResult projectList() {
		BussProject temp = new BussProject();
		List<BussProject> projects = projectService.selectBussProjectList(temp);
		return success(BeanUtil.copyToList(projects, ProjectVO.class));
	}

}
