package com.shujuwuliankeji.ems.cloud.module.bussiness.iot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cm.heclouds.onenet.studio.api.IotClient;
import com.github.cm.heclouds.onenet.studio.api.IotProfile;
import com.shujuwuliankeji.ems.cloud.common.core.exception.GlobalException;
import com.shujuwuliankeji.ems.cloud.module.bussiness.domain.BussCompany;
import com.shujuwuliankeji.ems.cloud.module.bussiness.service.IBussCompanyService;

/**
 * 
 * @author Fanzhongjie
 * @date 2023年2月1日 下午4:46:08
 * @tips TODO
 */
@Component
public class IotClientUtils {

	@Autowired
	private IBussCompanyService companyService;

	/**
	 * 根据企业信息提供的接入参数返回客户端
	 * 
	 * @param companyId
	 * @return IotClient
	 */
	public IotClient getClient(Long companyId) {

		BussCompany company = companyService.selectBussCompanyById(companyId);
		if (company == null) {
			throw new GlobalException("创建IotClient客户端企业信息为空");
		}
		IotProfile profile = new IotProfile();
		profile.userId(company.getIotUserId()).accessKey(company.getIotAccessKey())
				.url("http://iot.shujuwuliankeji.com:51234");
		IotClient client = IotClient.create(profile);
		return client;
	}

}
