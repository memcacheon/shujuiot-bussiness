package com.shujuwuliankeji.ems.cloud.module.system.service;

import java.util.List;

import com.shujuwuliankeji.ems.cloud.module.system.domain.SysRegion;

/**
 * 省市区管理Service接口
 * 
 * @author fanzhongjie
 * @date 2023-01-09
 */
public interface ISysRegionService {
	/**
	 * 查询省市区管理
	 * 
	 * @param id 省市区管理主键
	 * @return 省市区管理
	 */
	public SysRegion selectSysRegionById(Long id);

	/**
	 * 查询省市区管理列表
	 * 
	 * @param sysRegion 省市区管理
	 * @return 省市区管理集合
	 */
	public List<SysRegion> selectSysRegionList(SysRegion sysRegion);

	/**
	 * 新增省市区管理
	 * 
	 * @param sysRegion 省市区管理
	 * @return 结果
	 */
	public int insertSysRegion(SysRegion sysRegion);

	/**
	 * 修改省市区管理
	 * 
	 * @param sysRegion 省市区管理
	 * @return 结果
	 */
	public int updateSysRegion(SysRegion sysRegion);

	/**
	 * 批量删除省市区管理
	 * 
	 * @param ids 需要删除的省市区管理主键集合
	 * @return 结果
	 */
	public int deleteSysRegionByIds(Long[] ids);

	/**
	 * 删除省市区管理信息
	 * 
	 * @param id 省市区管理主键
	 * @return 结果
	 */
	public int deleteSysRegionById(Long id);
}
