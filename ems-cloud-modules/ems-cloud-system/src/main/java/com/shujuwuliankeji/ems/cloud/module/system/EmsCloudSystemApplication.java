package com.shujuwuliankeji.ems.cloud.module.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableCustomConfig;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableRyFeignClients;
import com.shujuwuliankeji.ems.cloud.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 系统模块
 * 
 * @author ruoyi
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication(scanBasePackages = { "com.shujuwuliankeji.ems.cloud" })
public class EmsCloudSystemApplication {
	public static void main(String[] args) {
		SpringApplication.run(EmsCloudSystemApplication.class, args);
	}
}
