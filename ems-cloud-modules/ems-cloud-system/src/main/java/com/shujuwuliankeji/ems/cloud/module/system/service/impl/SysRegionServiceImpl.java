package com.shujuwuliankeji.ems.cloud.module.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shujuwuliankeji.ems.cloud.module.system.domain.SysRegion;
import com.shujuwuliankeji.ems.cloud.module.system.mapper.SysRegionMapper;
import com.shujuwuliankeji.ems.cloud.module.system.service.ISysRegionService;

/**
 * 省市区管理Service业务层处理
 * 
 * @author fanzhongjie
 * @date 2023-01-09
 */
@Service
public class SysRegionServiceImpl implements ISysRegionService {
	@Autowired
	private SysRegionMapper sysRegionMapper;

	/**
	 * 查询省市区管理
	 * 
	 * @param id 省市区管理主键
	 * @return 省市区管理
	 */
	@Override
	public SysRegion selectSysRegionById(Long id) {
		return sysRegionMapper.selectSysRegionById(id);
	}

	/**
	 * 查询省市区管理列表
	 * 
	 * @param sysRegion 省市区管理
	 * @return 省市区管理
	 */
	@Override
	public List<SysRegion> selectSysRegionList(SysRegion sysRegion) {
		return sysRegionMapper.selectSysRegionList(sysRegion);
	}

	/**
	 * 新增省市区管理
	 * 
	 * @param sysRegion 省市区管理
	 * @return 结果
	 */
	@Override
	public int insertSysRegion(SysRegion sysRegion) {
		return sysRegionMapper.insertSysRegion(sysRegion);
	}

	/**
	 * 修改省市区管理
	 * 
	 * @param sysRegion 省市区管理
	 * @return 结果
	 */
	@Override
	public int updateSysRegion(SysRegion sysRegion) {
		return sysRegionMapper.updateSysRegion(sysRegion);
	}

	/**
	 * 批量删除省市区管理
	 * 
	 * @param ids 需要删除的省市区管理主键
	 * @return 结果
	 */
	@Override
	public int deleteSysRegionByIds(Long[] ids) {
		return sysRegionMapper.deleteSysRegionByIds(ids);
	}

	/**
	 * 删除省市区管理信息
	 * 
	 * @param id 省市区管理主键
	 * @return 结果
	 */
	@Override
	public int deleteSysRegionById(Long id) {
		return sysRegionMapper.deleteSysRegionById(id);
	}
}
