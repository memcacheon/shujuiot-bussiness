package com.shujuwuliankeji.ems.cloud.module.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.shujuwuliankeji.ems.cloud.common.core.annotation.Excel;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.TreeEntity;

/**
 * 省市区管理对象 sys_region
 * 
 * @author fanzhongjie
 * @date 2023-01-09
 */
public class SysRegion extends TreeEntity {
	private static final long serialVersionUID = 1L;

	/** $column.columnComment */
	private Long id;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String levelType;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String name;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String shortName;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String parentPath;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String province;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String city;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String district;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String provinceShortName;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String cityShortName;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String districtShortName;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String provincePinyin;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String cityPinyin;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String districtPinyin;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String pinyin;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String jianpin;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String firstChar;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String cityCode;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String zipCode;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String lng;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String lat;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String remark1;

	/** $column.columnComment */
	@Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
	private String remark2;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	public String getLevelType() {
		return levelType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	public String getParentPath() {
		return parentPath;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getProvince() {
		return province;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getDistrict() {
		return district;
	}

	public void setProvinceShortName(String provinceShortName) {
		this.provinceShortName = provinceShortName;
	}

	public String getProvinceShortName() {
		return provinceShortName;
	}

	public void setCityShortName(String cityShortName) {
		this.cityShortName = cityShortName;
	}

	public String getCityShortName() {
		return cityShortName;
	}

	public void setDistrictShortName(String districtShortName) {
		this.districtShortName = districtShortName;
	}

	public String getDistrictShortName() {
		return districtShortName;
	}

	public void setProvincePinyin(String provincePinyin) {
		this.provincePinyin = provincePinyin;
	}

	public String getProvincePinyin() {
		return provincePinyin;
	}

	public void setCityPinyin(String cityPinyin) {
		this.cityPinyin = cityPinyin;
	}

	public String getCityPinyin() {
		return cityPinyin;
	}

	public void setDistrictPinyin(String districtPinyin) {
		this.districtPinyin = districtPinyin;
	}

	public String getDistrictPinyin() {
		return districtPinyin;
	}

	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}

	public String getPinyin() {
		return pinyin;
	}

	public void setJianpin(String jianpin) {
		this.jianpin = jianpin;
	}

	public String getJianpin() {
		return jianpin;
	}

	public void setFirstChar(String firstChar) {
		this.firstChar = firstChar;
	}

	public String getFirstChar() {
		return firstChar;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLng() {
		return lng;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLat() {
		return lat;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark2() {
		return remark2;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE).append("id", getId())
				.append("parentId", getParentId()).append("levelType", getLevelType()).append("name", getName())
				.append("shortName", getShortName()).append("parentPath", getParentPath())
				.append("province", getProvince()).append("city", getCity()).append("district", getDistrict())
				.append("provinceShortName", getProvinceShortName()).append("cityShortName", getCityShortName())
				.append("districtShortName", getDistrictShortName()).append("provincePinyin", getProvincePinyin())
				.append("cityPinyin", getCityPinyin()).append("districtPinyin", getDistrictPinyin())
				.append("pinyin", getPinyin()).append("jianpin", getJianpin()).append("firstChar", getFirstChar())
				.append("cityCode", getCityCode()).append("zipCode", getZipCode()).append("lng", getLng())
				.append("lat", getLat()).append("remark1", getRemark1()).append("remark2", getRemark2()).toString();
	}
}
