package com.shujuwuliankeji.ems.cloud.module.system.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shujuwuliankeji.ems.cloud.common.core.utils.poi.ExcelUtil;
import com.shujuwuliankeji.ems.cloud.common.core.web.controller.BaseController;
import com.shujuwuliankeji.ems.cloud.common.core.web.domain.AjaxResult;
import com.shujuwuliankeji.ems.cloud.common.log.annotation.Log;
import com.shujuwuliankeji.ems.cloud.common.log.enums.BusinessType;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.RequiresPermissions;
import com.shujuwuliankeji.ems.cloud.module.system.domain.SysRegion;
import com.shujuwuliankeji.ems.cloud.module.system.service.ISysRegionService;

/**
 * 省市区管理Controller
 * 
 * @author fanzhongjie
 * @date 2023-01-09
 */
@RestController
@RequestMapping("/region")
public class SysRegionController extends BaseController {
	@Autowired
	private ISysRegionService sysRegionService;

	/**
	 * 查询省市区管理列表
	 */
	@RequiresPermissions("system:region:list")
	@GetMapping("/list")
	public AjaxResult list(SysRegion sysRegion) {
		List<SysRegion> list = sysRegionService.selectSysRegionList(sysRegion);
		return success(list);
	}

	/**
	 * 导出省市区管理列表
	 */
	@RequiresPermissions("system:region:export")
	@Log(title = "省市区管理", businessType = BusinessType.EXPORT)
	@PostMapping("/export")
	public void export(HttpServletResponse response, SysRegion sysRegion) {
		List<SysRegion> list = sysRegionService.selectSysRegionList(sysRegion);
		ExcelUtil<SysRegion> util = new ExcelUtil<SysRegion>(SysRegion.class);
		util.exportExcel(response, list, "省市区管理数据");
	}

	/**
	 * 获取省市区管理详细信息
	 */
	@RequiresPermissions("system:region:query")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		return success(sysRegionService.selectSysRegionById(id));
	}

	/**
	 * 新增省市区管理
	 */
	@RequiresPermissions("system:region:add")
	@Log(title = "省市区管理", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody SysRegion sysRegion) {
		return toAjax(sysRegionService.insertSysRegion(sysRegion));
	}

	/**
	 * 修改省市区管理
	 */
	@RequiresPermissions("system:region:edit")
	@Log(title = "省市区管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody SysRegion sysRegion) {
		return toAjax(sysRegionService.updateSysRegion(sysRegion));
	}

	/**
	 * 删除省市区管理
	 */
	@RequiresPermissions("system:region:remove")
	@Log(title = "省市区管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		return toAjax(sysRegionService.deleteSysRegionByIds(ids));
	}
}
