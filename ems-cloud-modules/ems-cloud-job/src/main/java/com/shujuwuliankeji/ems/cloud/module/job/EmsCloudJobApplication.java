package com.shujuwuliankeji.ems.cloud.module.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableCustomConfig;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableRyFeignClients;
import com.shujuwuliankeji.ems.cloud.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 定时任务
 * 
 * @author ruoyi
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication(scanBasePackages = { "com.shujuwuliankeji.ems.cloud" })
public class EmsCloudJobApplication {
	public static void main(String[] args) {
		SpringApplication.run(EmsCloudJobApplication.class, args);
	}
}
