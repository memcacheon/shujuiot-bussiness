package com.shujuwuliankeji.ems.cloud.module.gen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableCustomConfig;
import com.shujuwuliankeji.ems.cloud.common.security.annotation.EnableRyFeignClients;
import com.shujuwuliankeji.ems.cloud.common.swagger.annotation.EnableCustomSwagger2;

/**
 * 代码生成
 * 
 * @author ruoyi
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication(scanBasePackages = { "com.shujuwuliankeji.ems.cloud" })
public class EmsCloudGenApplication {
	public static void main(String[] args) {
		SpringApplication.run(EmsCloudGenApplication.class, args);
	}
}
